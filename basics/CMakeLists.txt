cmake_minimum_required(VERSION 2.8)

project(CatBasicsPlugin CXX)

set(CPP_FILES source/CatMessage.cpp source/CatMessagePrint.cpp source/CatWatchdog.cpp source/CatBasicsPlugin.cpp source/CatMemPage.cpp source/CatMemory.cpp source/CatElementSpy.cpp source/CatMultiple.cpp source/CatMessageFormatter.cpp source/CatEvent.cpp source/CatEventMessage.cpp source/CatEventFile.cpp)

include_directories(${PROJECT_SOURCE_DIR} ${CatShellCore_INCLUDE_DIRS})

add_library(${PROJECT_NAME} SHARED ${CPP_FILES})
target_link_libraries(CatBasicsPlugin CatShellCore)

set(${PROJECT_NAME}_INCLUDE_DIRS ${PROJECT_SOURCE_DIR} CACHE INTERNAL "${PROJECT_NAME}: Include Directories" FORCE)

install (TARGETS CatBasicsPlugin DESTINATION lib)
install (DIRECTORY ${PROJECT_SOURCE_DIR}/CatShell DESTINATION include)
