#ifndef INCLUDE_CATMESSAGEPRINT
#define INCLUDE_CATMESSAGEPRINT

#include "CatShell/core/Algorithm.h"

namespace CatShell {

class CatMessagePrint : public Algorithm
{
        CAT_OBJECT_DECLARE(CatMessagePrint, Algorithm, CAT_NAME_MESSAGE_PRINT, CAT_TYPE_MESSAGE_PRINT);
public:

        CatMessagePrint();
                /// Constructor: default.

        virtual ~CatMessagePrint();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

        Algorithm::DataPort    _in_port_message;
};

inline StreamSize CatMessagePrint::cat_stream_get_size() const
{
        return Algorithm::cat_stream_get_size();
}

inline void CatMessagePrint::cat_stream_out(std::ostream& output)
{
        return Algorithm::cat_stream_out(output);
}

inline void CatMessagePrint::cat_stream_in(std::istream& input)
{
        return Algorithm::cat_stream_in(input);
}

inline void CatMessagePrint::cat_print(std::ostream& output, const std::string& padding)
{
        Algorithm::cat_print(output, padding);
}


} // namespace CatShell

#endif

