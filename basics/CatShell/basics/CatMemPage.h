#ifndef INCLUDE_CATMEMPAGE
#define INCLUDE_CATMEMPAGE

#include "CatShell/core/defines.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/basics/defines.h"

#include <iostream>

namespace CatShell {

class CatMemPage : public CatObject
        ///
        /// Virtual class, as a base for all memory pages.
        ///
{
        CAT_OBJECT_DECLARE(CatMemPage, CatObject, CAT_NAME_CATMEMPAGE, CAT_TYPE_CATMEMPAGE);

public:

        enum class PrintSize { byte1, byte2, byte4, byte8 };

        CatMemPage();
                /// Constructor: default.

        virtual ~CatMemPage();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        void set_PrintSize(PrintSize size);
                /// Sets the format of printing.

        std::uint8_t* get_memory();
                /// Return the memory address.

        std::uint32_t get_memory_length() const;
                /// Returns the length of the valid data.

        std::uint32_t get_memory_capacity() const;
                /// Returns the capacity of the memory page.

        void set_memory_length(std::uint32_t length);
                /// Sets the length of the valid data. If greater than,
                /// capacity of the page, the length is set to capacity.

        std::uint8_t get_raw_8bit(std::uint32_t index) const;
                /// Returns 32bit word at desired index.

        std::uint16_t get_raw_16bit(std::uint32_t index) const;
                /// Returns 32bit word at desired index.

        std::uint32_t get_raw_32bit(std::uint32_t index) const;
                /// Returns 32bit word at desired index.

protected:

        void set_memory(std::uint8_t* addr, std::uint32_t capacity, std::uint32_t length = 0);
                /// This page will represent memory at the 'addr' for continous 'capacity' bytes,
                /// out of which first 'length' are valid data

private: 

        std::uint32_t   _data_capacity;
        std::uint32_t   _data_length;
        std::uint8_t*   _data;
        PrintSize       _p_format;
};

 

inline std::uint8_t CatMemPage::get_raw_8bit(std::uint32_t index) const {return *(_data+index);}
inline std::uint16_t CatMemPage::get_raw_16bit(std::uint32_t index) const {return *((std::uint16_t*)(_data+index));}
inline std::uint32_t CatMemPage::get_raw_32bit(std::uint32_t index) const {return *((std::uint32_t*)(_data+index));}

inline void CatMemPage::set_PrintSize(PrintSize size)
{
	_p_format = size;
}

inline void CatMemPage::set_memory(std::uint8_t* addr, std::uint32_t capacity, std::uint32_t length)
{
        _data_capacity = capacity;
        _data_length = length;
        _data = addr;
}

inline std::uint8_t* CatMemPage::get_memory()
{
        return _data;
}

inline std::uint32_t CatMemPage::get_memory_length() const
{
        return _data_length;
}

inline std::uint32_t CatMemPage::get_memory_capacity() const
{
        return _data_capacity;
}

inline void CatMemPage::set_memory_length(std::uint32_t length)
{
        if(length > _data_capacity)
                _data_length = _data_capacity;
        else
                _data_length = length;
}


inline StreamSize CatMemPage::cat_stream_get_size() const
{
        return _data_length;
}

inline void CatMemPage::cat_stream_out(std::ostream& output)
{
        output.write((char*)_data, _data_length);
}

inline void CatMemPage::cat_stream_in(std::istream& input)
{
        input.read((char*)_data, _data_length);
}

inline void CatMemPage::cat_free() {}

} // namespace CatShell


#endif
