#ifndef INCLUDE_CATEVENTMESSAGE
#define INCLUDE_CATEVENTMESSAGE

#include "CatShell/basics/CatEvent.h"

namespace CatShell {

class CatEventMessage : public CatShell::CatEvent
{
        CAT_OBJECT_DECLARE(CatEventMessage, CatShell::CatEvent, CAT_NAME_CATEVENTMESSAGE, CAT_TYPE_CATEVENTMESSAGE);
public:

        CatEventMessage();
                /// Constructor: default.

        virtual ~CatEventMessage();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatShell::CatAddress& addr);
                /// Returns the pointer to the desired part.
//----------------------------------------------------------------------

        void set_message(const std::string& msg);
          /// Set's the message.

protected:

private:

        std::string _message;
};

inline void CatEventMessage::cat_free() {}
inline StreamSize CatEventMessage::cat_stream_get_size() const { return CatEvent::cat_stream_get_size() + _message.length() + sizeof(std::uint32_t); }
inline void CatEventMessage::set_message(const std::string& msg) { _message = msg; }

} // namespace CatShell

#endif
