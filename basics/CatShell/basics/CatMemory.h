#ifndef INCLUDE_CATMEMORY
#define INCLUDE_CATMEMORY

#include "CatShell/basics/defines.h"
#include "CatShell/basics/CatMemPage.h"

namespace CatShell {

class CatMemory : public CatMemPage
        ///
        /// Memory page based on dynamic memory allocation.
        ///
{
        CAT_OBJECT_DECLARE(CatMemory, CatMemPage, CAT_NAME_CATMEMORY, CAT_TYPE_CATMEMORY);

public:

        CatMemory();
                /// Constructor: default.

        virtual ~CatMemory();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------

        void set_memory_capacity(std::uint32_t capacity);
                /// Sets the capacity of the data. Length is set to 0.
                /// NOTE: the content of the memory is lost !!!

protected:

private: 

};

inline StreamSize CatMemory::cat_stream_get_size() const
{
        return CatShell::CatMemPage::cat_stream_get_size() + sizeof(std::uint32_t);
}

inline void CatMemory::cat_print(std::ostream& output, const std::string& padding)
{
        CatMemPage::cat_print(output, padding);
}

} // namespace CatShell


#endif
