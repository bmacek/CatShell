#ifndef INCLUDE_CATWATCHDOG
#define INCLUDE_CATWATCHDOG

#include "CatShell/core/Process.h"
#include "CatShell/core/Algorithm.h"

namespace CatShell {

class CatWatchdog : public Process
{
        CAT_OBJECT_DECLARE(CatWatchdog, Process, CAT_NAME_WATCHDOG, CAT_TYPE_WATCHDOG);
public:

        CatWatchdog();
                /// Constructor: default.

        virtual ~CatWatchdog();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatPointer<WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------
protected:

private:

        Algorithm::DataPort    _out_port_message;
};

inline StreamSize CatWatchdog::cat_stream_get_size() const
{
        return Process::cat_stream_get_size();
}

inline void CatWatchdog::cat_stream_out(std::ostream& output)
{
        Process::cat_stream_out(output);
}

inline void CatWatchdog::cat_stream_in(std::istream& input)
{
        Process::cat_stream_in(input);
}

inline void CatWatchdog::cat_print(std::ostream& output, const std::string& padding)
{
        Process::cat_print(output, padding);
}


} // namespace CatShell

#endif
