#ifndef INCLUDE_CATMESSAGE
#define INCLUDE_CATMESSAGE

#include "CatShell/core/defines.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/basics/defines.h"

namespace CatShell {

class CatMessage : public CatObject
        ///
        /// Class holding a code and text message.
        ///
{
        CAT_OBJECT_DECLARE(CatMessage, CatObject, CAT_NAME_MESSAGE, CAT_TYPE_MESSAGE);

public:

        CatMessage();
                /// Constructor: default.

        CatMessage(CatMessageType type, const std::string& msg);
                /// Constructor: constructs the message.

        virtual ~CatMessage();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        void print(std::ostream& output);
                /// Streams the message.

        void set(CatMessageType type, const std::string& msg);
                /// Set the message.

        const std::string& message() const;
                /// Returns the message text.

protected:

private: 

        CatMessageType  _type;
        std::string     _message;
};

inline void CatMessage::cat_free() {}
inline void CatMessage::set(CatMessageType type, const std::string& msg) { _type=type; _message=msg; }
inline const std::string& CatMessage::message() const { return _message; }


} // namespace CatShell


#endif
