#ifndef INCLUDE_CATELEMENTSPY
#define INCLUDE_CATELEMENTSPY

#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatElement.h"
#include "CatShell/core/Algorithm.h"
#include "CatShell/core/MemoryPool.h"

#include "CatShell/basics/CatMessage.h"

namespace CatShell {

class CatElementSpy : public Algorithm
{
        CAT_OBJECT_DECLARE(CatElementSpy, Algorithm, CAT_NAME_CATELEMENTSPY, CAT_TYPE_CATELEMENTSPY);
public:

        CatElementSpy();
                /// Constructor: default.

        virtual ~CatElementSpy();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

        CatShell::MemoryPool<CatShell::CatMessage>* _pool;

        Algorithm::DataPort    _in_port_object;
        Algorithm::DataPort    _out_port_object;
        Algorithm::DataPort    _out_port_notify;

        CatVirtualElement* _element;
        void*              _variable_I;
        void*              _variable_II;
        bool               _fill_I;
};

inline StreamSize CatElementSpy::cat_stream_get_size() const { return Algorithm::cat_stream_get_size(); }
inline void CatElementSpy::cat_stream_out(std::ostream& output) { return Algorithm::cat_stream_out(output); }
inline void CatElementSpy::cat_stream_in(std::istream& input) { return Algorithm::cat_stream_in(input); }
inline void CatElementSpy::cat_print(std::ostream& output, const std::string& padding) { Algorithm::cat_print(output, padding); }

} // namespace CatShell

#endif

