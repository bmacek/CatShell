#ifndef INCLUDE_CATEVENT
#define INCLUDE_CATEVENT

#include "CatShell/core/TimePoint.h"
#include "CatShell/core/defines.h"
#include "CatShell/core/CatObject.h"

namespace CatShell {

class CatEvent : public CatShell::CatObject
{
        CAT_OBJECT_DECLARE(CatEvent, CatShell::CatObject, CAT_NAME_CATEVENT, CAT_TYPE_CATEVENT);
public:

        CatEvent();
                /// Constructor: default.

        virtual ~CatEvent();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatShell::CatAddress& addr);
                /// Returns the pointer to the desired part.
//----------------------------------------------------------------------

        void time_stamp_now();
          /// Updates the timestamp to current time.

protected:

private:

        CatShell::TimePoint     _time;
};

inline void CatEvent::cat_free() {}
inline StreamSize CatEvent::cat_stream_get_size() const { return _time.get_stream_length(); }
inline void CatEvent::time_stamp_now() { _time.get_now(); }

} // namespace CatShell

#endif
