#ifndef INCLUDE_CATEVENTFILE
#define INCLUDE_CATEVENTFILE

#include "CatShell/core/TimePoint.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/Process.h"
#include "CatShell/basics/CatEventMessage.h"
#include "CatShell/core/defines.h"

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

namespace CatShell {

class CatEventFile : public CatShell::Process
{
        CAT_OBJECT_DECLARE(CatEventFile, CatShell::Process, CAT_NAME_CATEVENTFILE, CAT_TYPE_CATEVENTFILE);
public:

        CatEventFile();
                /// Constructor: default.

        virtual ~CatEventFile();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------

protected:

private:

  std::ifstream*  _file;
  std::string     _file_path;

  CatShell::MemoryPool<CatEventMessage>* _pool;

  Algorithm::DataPort    _out_port_message;
};

inline StreamSize CatEventFile::cat_stream_get_size() const { return 0; }
inline void CatEventFile::cat_stream_out(std::ostream& output) {}
inline void CatEventFile::cat_stream_in(std::istream& input) {}
inline void CatEventFile::process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flash, CatShell::Logger& logger) {}
inline void CatEventFile::cat_print(std::ostream& output, const std::string& padding) { CatShell::Process::cat_print(output, padding); }

} // namespace CatShell

#endif
