#include <cstdint>

//
// Message types
//

typedef	std::uint32_t	CatMessageType;

#define CAT_MESSAGE_TYPE_UNKNOWN	(0)
#define CAT_MESSAGE_TYPE_HEARTBEAT	(1)
#define CAT_MESSAGE_TYPE_FINISH		(2)
#define CAT_MESSAGE_TYPE_TEXT		(3)

//
// Cat Types
//

#define CAT_TYPE_CATMULTIPLE    (0x0015)
#define CAT_NAME_CATMULTIPLE    "CatMultiple"
