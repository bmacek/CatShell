#ifndef INCLUDE_CATMESSAGEFORMATTER
#define INCLUDE_CATMESSAGEFORMATTER

#include "CatShell/core/Algorithm.h"
#include "CatShell/basics/CatMessage.h"
#include "CatShell/basics/defines.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/CatElement.h"

namespace CatShell {

class CatMessageFormatter : public Algorithm
{
        CAT_OBJECT_DECLARE(CatMessageFormatter, Algorithm, CAT_NAME_CATMESSAGEFORMATTER, CAT_TYPE_CATMESSAGEFORMATTER);
public:

        CatMessageFormatter();
                /// Constructor: default.

        virtual ~CatMessageFormatter();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

        Algorithm::DataPort    _in_port_object;
        Algorithm::DataPort    _out_port_message;

        std::string _padding_before;
        std::string _padding_after;

        MemoryPool<CatMessage>* _pool;

        CatVirtualElement* _element;
        void*              _variable;
};

inline StreamSize CatMessageFormatter::cat_stream_get_size() const
{
        return Algorithm::cat_stream_get_size();
}

inline void CatMessageFormatter::cat_stream_out(std::ostream& output)
{
        return Algorithm::cat_stream_out(output);
}

inline void CatMessageFormatter::cat_stream_in(std::istream& input)
{
        return Algorithm::cat_stream_in(input);
}

inline void CatMessageFormatter::cat_print(std::ostream& output, const std::string& padding)
{
        Algorithm::cat_print(output, padding);
}


} // namespace CatShell

#endif
