#include "CatShell/basics/CatMessageFormatter.h"
#include "CatShell/basics/CatMessage.h"
#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatMessageFormatter, CAT_NAME_CATMESSAGEFORMATTER, CAT_TYPE_CATMESSAGEFORMATTER);


CatMessageFormatter::CatMessageFormatter()
{

}

CatMessageFormatter::~CatMessageFormatter()
{
  if(_element)
  {
    if(_variable)
    {
      _element->delete_variable(_variable);
      _variable = nullptr;
    }

    delete _element;
    _element = nullptr;
  }
}

void CatMessageFormatter::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_object)
        {

          _element->fill(_variable, object);

          CatPointer<CatMessage>  msg = _pool->get_element();
          msg->set(CAT_MESSAGE_TYPE_TEXT, _padding_before+_element->get_string(_variable)+_padding_after);

          // publish data
          publish_data(_out_port_message, msg, logger);
        }
}

void CatMessageFormatter::algorithm_init()
{
        Algorithm::algorithm_init();

        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);
        _out_port_message = declare_output_port("out_message", CAT_NAME_MESSAGE);

        _padding_before = algo_settings().get_setting<std::string>("padding_before", "");
        _padding_after = algo_settings().get_setting<std::string>("padding_after", "");

        _element = CatVirtualElement::create_element(algo_settings()["data"]);
        if(_element)
          _variable = _element->create_variable();
        else
          _variable = nullptr;

        _pool = PluginDeamon::get_pool<CatMessage>();
}

void CatMessageFormatter::algorithm_deinit()
{
    if(_element)
    {
      if(_variable)
      {
        _element->delete_variable(_variable);
        _variable = nullptr;
      }

      delete _element;
      _element = nullptr;
    }

    Algorithm::algorithm_deinit();
}

} // namespace CatShell
