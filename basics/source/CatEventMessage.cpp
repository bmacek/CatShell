#include "CatShell/basics/CatEventMessage.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatEventMessage, CAT_NAME_CATEVENTMESSAGE, CAT_TYPE_CATEVENTMESSAGE);

CatEventMessage::CatEventMessage()
{
}

CatEventMessage::~CatEventMessage()
{
}

void CatEventMessage::cat_stream_out(std::ostream& output)
{
	CatEvent::cat_stream_out(output);
	// write the string
	std::uint32_t n = (std::uint32_t)_message.length();
	output.write((char*)&n, sizeof(std::uint32_t));
	output.write(_message.c_str(), n);
}

void CatEventMessage::cat_stream_in(std::istream& input)
{
	CatEvent::cat_stream_in(input);
	// read the string
	std::uint32_t n;
	input.read((char*)&n, sizeof(std::uint32_t));

	_message.resize(n);
	char tmp;
	for(std::uint32_t i=0; i<n; ++i)
	{
					input.read(&tmp, sizeof(char));
					_message[i] = tmp;
	}
}

void CatEventMessage::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	CatEvent::cat_print(output, padding);
	output << endl << "Message: " << _message;
}

void* CatEventMessage::cat_get_part(CatShell::CatAddress& addr)
{
	if(addr == "message")
	{
		return &_message;
	}
	else
		return CatEvent::cat_get_part(addr);
}

} // namespace DbmShell
