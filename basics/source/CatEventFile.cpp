#include "CatShell/basics/CatEventFile.h"

#include "CatShell/core/PluginDeamon.h"

#include <string>
#include <signal.h>
#include <poll.h>
#include <fcntl.h>

#include <stdio.h>

#include <sys/poll.h>

#include <sys/time.h>

#include <unistd.h>

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

using namespace std;
using namespace CatShell;

CAT_OBJECT_IMPLEMENT(CatEventFile, CAT_NAME_CATEVENTFILE, CAT_TYPE_CATEVENTFILE);

CatEventFile::CatEventFile()
{
}

CatEventFile::~CatEventFile()
{
}

void CatEventFile::algorithm_init()
{
	Process::algorithm_init();

	// define the output port
	_out_port_message = declare_output_port("out_message", CAT_NAME_CATEVENTMESSAGE);

	_file_path = algo_settings().get_setting<std::string>("path", "");

	_pool = PluginDeamon::get_pool<CatEventMessage>();
}

void CatEventFile::algorithm_deinit()
{
	Process::algorithm_deinit();
}
void CatEventFile::work_main()
{
	std::string	message;

	if(_file_path.size() > 0)
	{
		// open the file
		_file = new ifstream(_file_path.c_str(), std::ifstream::in);
		if(_file->is_open())
		{
			while(true)
			{
				getline(*_file, message);
				if(_file->good())
				{
					if(message == "quit")
					{
						break;
					}

					CatPointer<CatEventMessage> msg = _pool->get_element();
					msg->time_stamp_now();
					msg->set_message(message);
					//cout << "Message: " << message << endl;
					// publish data
					publish_data(_out_port_message, msg, logger());

				}
				else
				{
					if(!should_continue(false))
						break;
					_file->close();
					_file->open(_file_path.c_str(), std::ifstream::in);
				}
			}

			// close the file
			_file->close();
			delete _file;
			_file = nullptr;
			cout << "Messaging done." << endl;
		}
		else
		{
			ERROR(logger(), "EventFile: Could not open file: " << _file_path);
		}
	}
}

void CatEventFile::work_init()
{

}

void CatEventFile::work_deinit()
{

}

void CatEventFile::work_subthread_finished(CatPointer<WorkThread> child)
{
	// no sub-treads so nothing to do
}
