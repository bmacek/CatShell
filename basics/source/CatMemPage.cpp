#include "CatShell/basics/CatMemPage.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatMemPage, CAT_NAME_CATMEMPAGE, CAT_TYPE_CATMEMPAGE);

CatMemPage::CatMemPage()
: _data_capacity(0), _data_length(0), _data(nullptr), _p_format(PrintSize::byte1)
{
}

CatMemPage::~CatMemPage()
{
}

void CatMemPage::cat_print(std::ostream& output, const std::string& padding)
{
        CatObject::cat_print(output, padding);
        output << " Length in bytes: " << _data_length << "(" << _data_capacity << ")" << endl;

        std::uint32_t cnt=0;
        for(std::uint32_t j=0; true; j+=10)
        {
                // line
                printf("%s   %04u: ", padding.c_str(), j);
                //
                for(std::uint32_t i=0; i<10; ++i)
                {
                        if(_p_format == PrintSize::byte1)
                        {
                                printf("%02x ", _data[cnt]);
                                cnt += 1;
                        }
                        else if(_p_format == PrintSize::byte2)
                        {
                                printf("%04x ", *((uint16_t*)(_data+cnt)));
                                cnt += 2;                                
                        }
                        else if(_p_format == PrintSize::byte4)
                        {
                                printf("%08x ", *((uint32_t*)(_data+cnt)));
                                cnt += 4;                                
                        }
                        else if(_p_format == PrintSize::byte8)
                        {
                                printf("%016llx ", *((uint64_t*)(_data+cnt)));
                                cnt += 8;                                
                        }
                        if(cnt >= _data_length)
                                break;
                }
                if(cnt >= _data_length)
                        break;
                else
                        printf("\n");
        }
}

} // namespace CatShell
