#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/PluginDeamon.h"
#include "CatShell/basics/CatWatchdog.h"
#include "CatShell/basics/CatMessage.h"

#include <iostream>
#include <chrono>       
#include <thread>

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatWatchdog, CAT_NAME_WATCHDOG, CAT_TYPE_WATCHDOG);

CatWatchdog::CatWatchdog()
{

}

CatWatchdog::~CatWatchdog()
{

}

void CatWatchdog::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
    // will not be processing any data
}

void CatWatchdog::algorithm_init()
{
	Process::algorithm_init();

    // define the output port
    _out_port_message = declare_output_port("out_message", CAT_NAME_MESSAGE);
}

void CatWatchdog::algorithm_deinit()
{
	Process::algorithm_deinit();
}

void CatWatchdog::work_main()
{
        std::uint32_t counter = 0;

        MemoryPool<CatMessage>* pool = PluginDeamon::get_pool<CatMessage>();

        std::uint32_t step = algo_settings().get_setting<std::uint32_t>("step", 1);

        while(should_continue(false))
        {
                // wait
                std::this_thread::sleep_for(std::chrono::seconds(2));

                // log
                LOG(logger(), "Heartbeat " << counter)

                // create message
                char txt[100];
                sprintf(txt, "Message no. %d.", counter);

                CatPointer<CatMessage> msg = pool->get_element();
                msg->set(CAT_MESSAGE_TYPE_HEARTBEAT, txt);

                // publish data
                publish_data(_out_port_message, msg, logger());

                // increase the counter
                counter += step;
        }

        flush_data_clients(_out_port_message, logger());
}

void CatWatchdog::work_init()
{
        INFO(logger(), "CatWatchdog preparation...")
}

void CatWatchdog::work_deinit()
{
        INFO(logger(), "CatWatchdog clean up...")
}

void CatWatchdog::work_subthread_finished(CatPointer<WorkThread> child)
{
}

} // namespace CatShell
