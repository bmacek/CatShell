#include "CatShell/basics/CatMessage.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatMessage, CAT_NAME_MESSAGE, CAT_TYPE_MESSAGE);

CatMessage::CatMessage()
: _type(CAT_MESSAGE_TYPE_UNKNOWN), _message("")
{

}

CatMessage::CatMessage(CatMessageType type, const std::string& msg)
: _type(type), _message(msg)
{

}

CatMessage::~CatMessage()
{
}

StreamSize CatMessage::cat_stream_get_size() const
{
        return _message.length() + sizeof(std::uint32_t) + sizeof(CatMessageType);
}

void CatMessage::cat_stream_out(std::ostream& output)
{
        // write the type
        output.write((char*)&_type, sizeof(CatMessageType));
        // write the string
        std::uint32_t n = (std::uint32_t)_message.length();
        output.write((char*)&n, sizeof(std::uint32_t));
        output.write(_message.c_str(), n);
}

void CatMessage::cat_stream_in(std::istream& input)
{
        // read the type
        input.read((char*)&_type, sizeof(CatMessageType));
        // read the string
        std::uint32_t n;
        input.read((char*)&n, sizeof(std::uint32_t));

        _message.resize(n);
        char tmp;
        for(std::uint32_t i=0; i<n; ++i)
        {
                input.read(&tmp, sizeof(char));
                _message[i] = tmp;
        }
}

void CatMessage::cat_print(std::ostream& output, const std::string& padding)
{
        CatObject::cat_print(output, padding);
        output << "[" << _type << "] " << _message;
}

void CatMessage::print(std::ostream& output)
{
        output << "[" << _type << "] " << _message;
}

} // namespace CatShell
