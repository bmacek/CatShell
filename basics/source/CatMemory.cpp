#include "CatShell/basics/CatMemory.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatMemory, CAT_NAME_CATMEMORY, CAT_TYPE_CATMEMORY);

CatMemory::CatMemory()
{
}

CatMemory::~CatMemory()
{
        std::uint8_t* tmp = get_memory();
        if(tmp)
                delete tmp;
}

void CatMemory::set_memory_capacity(std::uint32_t capacity)
{
        if(get_memory_capacity() < capacity)
        {
                // need to extend
                std::uint8_t* tmp = get_memory();
                if(tmp)
                        delete tmp;

                tmp = new std::uint8_t[capacity];
                set_memory(tmp, capacity, 0);
        }
        else
        {
                set_memory(get_memory(), get_memory_capacity(), 0);
        }
}
        
inline void CatMemory::cat_stream_out(std::ostream& output)
{
        std::uint32_t length = get_memory_length();

        // write the length
        output.write((char*)&length, sizeof(std::uint32_t));

        if(length > 0)
        {        
                // write the data
                CatShell::CatMemPage::cat_stream_out(output);
        }
}

inline void CatMemory::cat_stream_in(std::istream& input)
{
        std::uint32_t length;

        // read the length
        input.read((char*)&length, sizeof(std::uint32_t));

        // create the space
        set_memory_capacity(length);

        if(length > 0)
        {
                // read data
                CatShell::CatMemPage::cat_stream_in(input);
        }
}



} // namespace CatShell
