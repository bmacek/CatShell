#include "CatShell/basics/CatMultiple.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatMultiple, CAT_NAME_CATMULTIPLE, CAT_TYPE_CATMULTIPLE);


CatMultiple::CatMultiple()
{
}

CatMultiple::~CatMultiple()
{
}

void CatMultiple::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_object && object.isSomething())
        {
                for(std::uint32_t i=0; i<_multiplication; ++i)
				{
                	publish_data(_out_port_object+(Algorithm::DataPort)i, object, logger);
				}
        }

        Algorithm::process_data(port, object, flush, logger);
}

void CatMultiple::algorithm_init()
{
        Algorithm::algorithm_init();

        _multiplication = algo_settings().get_setting<std::uint32_t>("multiplication", 1);
        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);
        _out_port_object = declare_output_port("out_object", CAT_NAME_OBJECT, _multiplication);
}

} // namespace CatShell
