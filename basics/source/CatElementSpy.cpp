#include "CatShell/basics/CatElementSpy.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatElementSpy, CAT_NAME_CATELEMENTSPY, CAT_TYPE_CATELEMENTSPY);


CatElementSpy::CatElementSpy()
: _element(nullptr), _variable_I(nullptr), _variable_II(nullptr)
{

}

CatElementSpy::~CatElementSpy()
{
        if(_element)
        {
                if(_variable_I)
                        _element->delete_variable(_variable_I);
                _variable_I = nullptr;
                if(_variable_II)
                        _element->delete_variable(_variable_II);
                _variable_II = nullptr;
                delete _element;
                _element = nullptr;
        }
}

void CatElementSpy::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_object && object.isSomething())
        {
                if(_fill_I)
                {
                        if(!_variable_I)
                                _variable_I = _element->create_variable();
                        _element->fill(_variable_I, object);        
                        _fill_I = false;            
                }
                else
                {
                        if(!_variable_II)
                                _variable_II = _element->create_variable();
                        _element->fill(_variable_II, object);                    
                        _fill_I = true;
                }

                if(_variable_I && _variable_II)
                {
                        // we have both so compare
                        if(!_element->are_variables_equal(_variable_I, _variable_II))
                        {
                                // get the message
                                CatPointer<CatMessage> msg = _pool->get_element();
                                msg->set(CAT_MESSAGE_TYPE_UNKNOWN, _element->get_string(!_fill_I ? _variable_I : _variable_II));
                                //
                                flush_data_clients(_out_port_object, logger);
                                // publish message
                                publish_data(_out_port_notify, msg, logger);
                         }
                }

                publish_data(_out_port_object, object, logger);
        }

        Algorithm::process_data(port, object, flush, logger);
}

void CatElementSpy::algorithm_init()
{
        Algorithm::algorithm_init();

        // prepare the pool
        _pool = PluginDeamon::get_pool<CatMessage>();

        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);
        _out_port_object = declare_output_port("out_object", CAT_NAME_OBJECT);
        _out_port_notify = declare_output_port("out_notify", CAT_NAME_MESSAGE);

        _element = CatVirtualElement::create_element(algo_settings()["element"]);
        if(!_element)
                return;

        _fill_I = true;
}

inline void CatElementSpy::algorithm_deinit()
{
        if(_element)
        {
                if(_variable_I)
                        _element->delete_variable(_variable_I);
                _variable_I = nullptr;
                if(_variable_II)
                        _element->delete_variable(_variable_II);
                _variable_II = nullptr;
                delete _element;
                _element = nullptr;
        }
        Algorithm::algorithm_deinit();
}



} // namespace CatShell
