#include "CatShell/core/Plugin.h"
#include "CatShell/basics/CatMessage.h"
#include "CatShell/basics/CatMessagePrint.h"
#include "CatShell/basics/CatWatchdog.h"
#include "CatShell/basics/CatMemPage.h"
#include "CatShell/basics/CatMemory.h"
#include "CatShell/basics/CatElementSpy.h"
#include "CatShell/basics/CatMultiple.h"
#include "CatShell/basics/CatMessageFormatter.h"
#include "CatShell/basics/CatEvent.h"
#include "CatShell/basics/CatEventMessage.h"
#include "CatShell/basics/CatEventFile.h"

using namespace std;

namespace CatShell {

EXPORT_PLUGIN(Plugin, CatBasicPlugin)
START_EXPORT_CLASSES(CatBasicPlugin)
EXPORT_CLASS(CatMessage, CAT_NAME_MESSAGE, CatBasicPlugin)
EXPORT_CLASS(CatMessagePrint, CAT_NAME_MESSAGE_PRINT, CatBasicPlugin)
EXPORT_CLASS(CatWatchdog, CAT_NAME_WATCHDOG, CatBasicPlugin)
EXPORT_CLASS(CatMemPage, CAT_NAME_CATMEMPAGE, CatBasicPlugin)
EXPORT_CLASS(CatMemory, CAT_NAME_CATMEMORY, CatBasicPlugin)
EXPORT_CLASS(CatElementSpy, CAT_NAME_CATELEMENTSPY, CatBasicPlugin)
EXPORT_CLASS(CatMultiple, CAT_NAME_CATMULTIPLE, CatBasicPlugin)
EXPORT_CLASS(CatMessageFormatter, CAT_NAME_CATMESSAGEFORMATTER, CatBasicPlugin)
EXPORT_CLASS(CatEvent, CAT_NAME_CATEVENT, CatBasicPlugin)
EXPORT_CLASS(CatEventMessage, CAT_NAME_CATEVENTMESSAGE, CatBasicPlugin)
EXPORT_CLASS(CatEventFile, CAT_NAME_CATEVENTFILE, CatBasicPlugin)
END_EXPORT_CLASSES(CatBasicPlugin)

} // namespace CatShell
