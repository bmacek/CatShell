#include "CatShell/basics/CatEvent.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatEvent, CAT_NAME_CATEVENT, CAT_TYPE_CATEVENT);

CatEvent::CatEvent()
{
}

CatEvent::~CatEvent()
{
}

void CatEvent::cat_stream_out(std::ostream& output)
{
	_time.write_to_stream(output);
}

void CatEvent::cat_stream_in(std::istream& input)
{
	_time.read_from_stream(input);
}

void CatEvent::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	CatObject::cat_print(output, padding);
	_time.convert_to_UTC_asci(text);
	output << "CatEvent at: " << text;
}

void* CatEvent::cat_get_part(CatShell::CatAddress& addr)
{
		return nullptr;
}

} // namespace CatShell
