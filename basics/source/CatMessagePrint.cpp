#include "CatShell/basics/CatMessagePrint.h"
#include "CatShell/basics/CatMessage.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatMessagePrint, CAT_NAME_MESSAGE_PRINT, CAT_TYPE_MESSAGE_PRINT);


CatMessagePrint::CatMessagePrint()
{

}

CatMessagePrint::~CatMessagePrint()
{

}

void CatMessagePrint::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_message)
        {
                CatPointer<CatMessage>  msg = object.cast<CatMessage>();

                std::cout << algo_settings().get_setting<std::string>("padding", "");
                msg->print(std::cout);
                cout << endl;
        }
}

void CatMessagePrint::algorithm_init()
{
        Algorithm::algorithm_init();

        _in_port_message = declare_input_port("in_message", CAT_NAME_MESSAGE);
}

void CatMessagePrint::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
}

} // namespace CatShell
