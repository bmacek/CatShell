#ifndef INCLUDE_ALGORITHM
#define INCLUDE_ALGORITHM

#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"
#include "CatShell/core/Setting.h"
#include "CatShell/core/Logger.h"

namespace CatShell {

class Algorithm : public virtual CatObject
{
        friend class PluginDeamon;
public:

        typedef std::uint32_t   DataPort;

        Algorithm();
                /// Constructor: default.

        virtual ~Algorithm();
                /// Destructor.

        void publish_data(DataPort port, CatPointer<CatObject> object, CatShell::Logger& logger);
                /// Publish data.

        CatPointer<CatObject> request_data(DataPort port, bool& flush, CatShell::Logger& logger);
                /// Requests data from the desired port.

        Setting& algo_settings();
                /// Returns settings.

        void flush_data_clients(DataPort port, CatShell::Logger& logger);
                /// Flushes all the clients if specefied port. If port is 0, 
                /// than all clients of all ports are flushed.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

        virtual CatPointer<CatObject> produce_data(DataPort port, bool& flush, CatShell::Logger& logger);
                /// Produces data on request. Secont parameter that is returned if a flush flag.
                /// Object is taken form the first one that producess a result.
//----------------------------------------------------------------------

protected:

        void set_settings(Setting* settings);
                /// Set the algorithm settings.

        DataPort get_input_port(const std::string& name) const;
                /// Returns the port. 0 if not found.

        DataPort get_output_port(const std::string& name) const;
                /// Returns the port. 0 if not found.

        std::string get_input_port_type(const DataPort port) const;
                /// Return the type for desired port.

        std::string get_output_port_type(const DataPort port) const;
                /// Return the type for desired port.

        bool register_data_client(DataPort output_port, CatPointer<Algorithm>& client, DataPort client_port);
                /// Register the user of data.

        DataPort declare_output_port(const std::string& name, const std::string& type, std::uint8_t number_of_ports = 1);
                /// Declares the output port. Returns the port number. If multiple ports are defined than 
                /// the name is extended with 000, 001, 002, ... and only the first port number is returned. Others are 
                /// guaranteed to be sequential.

        DataPort declare_input_port(const std::string& name, const std::string& type, std::uint8_t number_of_ports = 1);
                /// Declares the input port. Returns the port number. If multiple ports are defined than 
                /// the name is extended with 000, 001, 002, ... and only the first port number is returned. Others are 
                /// guaranteed to be sequential.

        void untie();
                /// Destroys all the links.

private:

        typedef struct 
        {
                CatPointer<Algorithm>   process;
                DataPort                link;
        } data_link_t;

        typedef struct
        {
                std::string                     name;
                std::string                     type;
                std::vector<data_link_t>        links;
                DataPort                        port;
        } output_port_t;

        typedef struct
        {
                std::string     name;
                std::string     type;
                std::vector<data_link_t>        links;
                DataPort        port;
        } input_port_t;

        std::vector<output_port_t>              _ports_output;
        std::vector<input_port_t>               _ports_input;
        std::map<std::string, input_port_t>     _map_ports_input;   

        Setting*        _settings;

        static const DataPort   AllPorts;
};

inline void Algorithm::set_settings(Setting* settings) { _settings = settings; }

inline Setting& Algorithm::algo_settings() { return *_settings; }

inline StreamSize Algorithm::cat_stream_get_size() const { return 0; }

inline void Algorithm::cat_stream_out(std::ostream& output) {}

inline void Algorithm::cat_stream_in(std::istream& input) {}

inline void Algorithm::cat_print(std::ostream& output, const std::string& padding) { CatObject::cat_print(output, padding); }

inline void Algorithm::cat_free() {}

inline void Algorithm::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger) { if(flush) flush_data_clients(0, logger); }

inline CatPointer<CatObject> Algorithm::produce_data(DataPort port, bool& flush, CatShell::Logger& logger) { flush = false; return CatPointer<CatObject>(nullptr); }

} // namespace CatShell

#endif

