#ifndef INCLUDE_CATCOLLECTION
#define INCLUDE_CATCOLLECTION

#include "CatShell/core/defines.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include <iostream>
#include <vector>

namespace CatShell {

class CatCollection : public CatObject, public std::vector<CatPointer<CatObject>>
        ///
        /// Collection of CatObjects
        ///
{
        CAT_OBJECT_DECLARE(CatCollection, CatObject, CAT_NAME_CATCOLLECTION, CAT_TYPE_CATCOLLECTION);

public:
        CatCollection();
                /// Constructor: default.

        virtual ~CatCollection();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatAddress& addr);
                /// Returns the pointer to the desired part.

//----------------------------------------------------------------------

protected:

private: 
        std::uint32_t   _size;
};

} // namespace CatShell

#endif
