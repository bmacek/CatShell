#ifndef INCLUDE_CATFILEOUT
#define INCLUDE_CATFILEOUT

#include "CatShell/core/CatObject.h"
#include "CatShell/core/Algorithm.h"

#include <fstream>
#include <iostream>
#include <mutex>

namespace CatShell {

class CatFileOut : public Algorithm
{
        CAT_OBJECT_DECLARE(CatFileOut, Algorithm, CAT_NAME_CATFILEOUT, CAT_TYPE_CATFILEOUT);
public:

        CatFileOut();
                /// Constructor: default.

        virtual ~CatFileOut();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

        Algorithm::DataPort    _in_port_object;

        std::fstream*          _file;

	bool                   _limit;
	StreamSize             _max_size;
	std::string            _name;
	std::string            _ext;
	std::uint32_t          _count;
	StreamSize             _pile;

  std::mutex              _data_lock;
};

inline StreamSize CatFileOut::cat_stream_get_size() const
{
        return Algorithm::cat_stream_get_size();
}

inline void CatFileOut::cat_stream_out(std::ostream& output)
{
        return Algorithm::cat_stream_out(output);
}

inline void CatFileOut::cat_stream_in(std::istream& input)
{
        return Algorithm::cat_stream_in(input);
}

inline void CatFileOut::cat_print(std::ostream& output, const std::string& padding)
{
        Algorithm::cat_print(output, padding);
}


} // namespace CatShell

#endif
