#ifndef INCLUDE_CATPOINTER
#define INCLUDE_CATPOINTER

#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatException.h"

namespace CatShell {

template <class C>
class CatPointer
        ///
        /// CatPointer is a pointer encapsulating data garbage collection capabilities.
        /// As a template argument any object derived from 'BaseObject' can be used.
        ///
{
        friend class MemoryPoolBase;
        template <class T> friend class MemoryPool;
        template <class T> friend class CatPointer;

public:
        CatPointer(): _ptr(nullptr)
        {
        }

        CatPointer(C* ptr): _ptr(ptr)
        {
                if (_ptr) _ptr->reference_plus();
        }

        CatPointer(const CatPointer& ptr): _ptr(ptr._ptr)
        {
                if (_ptr) _ptr->reference_plus();
        }

        template <class Other>
        CatPointer(const CatPointer<Other>& ptr): _ptr(const_cast<Other*>(ptr.get()))
        {
                if (_ptr) _ptr->reference_plus();
        }

        ~CatPointer()
        {
                if (_ptr)
                        _ptr->reference_minus();
        }
       
        CatPointer& operator = (const CatPointer& ptr)
        {
                return assign(ptr);
        }
       
        template <class Other>
        CatPointer& operator = (const CatPointer<Other>& ptr)
        {
                return assign<Other>(ptr);
        }

        C* operator -> ()
        {
                if (_ptr)
                        return _ptr;
                else
                        throw NullCatException(__FILE__, __LINE__, "");
        }

        const C* operator -> () const
        {
                if (_ptr)
                        return _ptr;
                else
                        throw NullCatException(__FILE__, __LINE__, "");
        }

        C& operator * ()
        {
                if (_ptr)
                        return *_ptr;
                else
                        throw NullCatException(__FILE__, __LINE__, "");
        }

        const C& operator * () const
        {
                if (_ptr)
                        return *_ptr;
                else
                        throw NullCatException(__FILE__, __LINE__, "");
        }

        operator C* ()
        {
                return _ptr;
        }
       
        operator const C* () const
        {
                return _ptr;
        }
       
        bool operator ! () const
        {
                return _ptr == 0;
        }

        bool isNull() const
        {
                return _ptr == 0;
        }
       
        bool isSomething() const
        {
                return _ptr != 0;
        }       

        bool operator == (const CatPointer& ptr) const
        {
                return _ptr == ptr._ptr;
        }

        bool operator == (const C* ptr) const
        {
                return _ptr == ptr;
        }

        bool operator == (C* ptr) const
        {
                return _ptr == ptr;
        }

        bool operator != (const CatPointer& ptr) const
        {
                return _ptr != ptr._ptr;
        }

        bool operator != (const C* ptr) const
        {
                return _ptr != ptr;
        }

        bool operator != (C* ptr) const
        {
                return _ptr != ptr;
        }

        bool operator < (const CatPointer& ptr) const
        {
                return _ptr < ptr._ptr;
        }

        bool operator < (const C* ptr) const
        {
                return _ptr < ptr;
        }

        bool operator < (C* ptr) const
        {
                return _ptr < ptr;
        }

        bool operator <= (const CatPointer& ptr) const
        {
                return _ptr <= ptr._ptr;
        }

        bool operator <= (const C* ptr) const
        {
                return _ptr <= ptr;
        }

        bool operator <= (C* ptr) const
        {
                return _ptr <= ptr;
        }

        bool operator > (const CatPointer& ptr) const
        {
                return _ptr > ptr._ptr;
        }

        bool operator > (const C* ptr) const
        {
                return _ptr > ptr;
        }

        bool operator > (C* ptr) const
        {
                return _ptr > ptr;
        }

        bool operator >= (const CatPointer& ptr) const
        {
                return _ptr >= ptr._ptr;
        }

        bool operator >= (const C* ptr) const
        {
                return _ptr >= ptr;
        }

        bool operator >= (C* ptr) const
        {
                return _ptr >= ptr;
        }

       
        template <class Other>
        CatPointer<Other> cast() const
                /// Casts the CatPointer via a dynamic cast to the given type.
                /// Returns an CatPointer containing NULL if the cast fails.
                /// Example: (assume class Sub: public Super)
                ///    CatPointer<Super> super(new Sub());
                ///    CatPointer<Sub> sub = super.cast<Sub>();
        {
                Other* pOther = dynamic_cast<Other*>(_ptr);
                return CatPointer<Other>(pOther, true);
        }

        template <class Other>
        CatPointer<Other> unsafeCast() const
                /// Casts the CatPointer via a static cast to the given type.
                /// Example: (assume class Sub: public Super)
                ///    CatPointer<Super> super(new Sub());
                ///    CatPointer<Sub> sub = super.unsafeCast<Sub>();
        {
                Other* pOther = static_cast<Other*>(_ptr);
                return CatPointer<Other>(pOther, true);
        }

        void make_nullptr()
                /// Defines the pointer as nullptr.
        {
                if (_ptr)
                        _ptr->reference_minus();
                _ptr = nullptr;
        }
private:

        CatPointer(C* ptr, bool shared): _ptr(ptr)
        {
                if (shared && _ptr) _ptr->reference_plus();
        }

        void assign_blind(C* ptr)
        {
                ptr = ptr;
        }
       
        CatPointer& assign(C* ptr)
        {
                if (_ptr != ptr)
                {
                        if (_ptr)
                                _ptr->reference_minus();
                        _ptr = ptr;
                }
                return *this;
        }

        CatPointer& assign(C* ptr, bool shared)
        {
                if (_ptr != ptr)
                {
                        if (_ptr)
                                _ptr->reference_minus();
                        _ptr = ptr;
                        if (shared && _ptr) _ptr->reference_plus();
                }
                return *this;
        }
       
        CatPointer& assign(const CatPointer& ptr)
        {
                if (&ptr != this)
                {
                        if (_ptr) 
                                _ptr->reference_minus();
                        _ptr = ptr._ptr;
                        if (_ptr) _ptr->reference_plus();
                }
                return *this;
        }
       
        template <class Other>
        CatPointer& assign(const CatPointer<Other>& ptr)
        {
                if (ptr.get() != _ptr)
                {
                        if (_ptr)
                                _ptr->reference_minus();
                        _ptr = static_cast<C*>(const_cast<Other*>(ptr.get()));
                        if (_ptr) _ptr->reference_plus();
                }
                return *this;
        }

        CatPointer& operator = (C* ptr)
        {
                return assign(ptr);
        }

        void swap(CatPointer& ptr)
        {
                std::swap(_ptr, ptr._ptr);
        }


        C* get()
        {
                return _ptr;
        }

        const C* get() const
        {
                return _ptr;
        }

        C* duplicate()
        {
                if (_ptr) _ptr->reference_plus();
                return _ptr;
        }

private:
        C* _ptr;
};


template <class C>
inline void swap(CatPointer<C>& p1, CatPointer<C>& p2)
{
        p1.swap(p2);
}

} // namespace CatShell

#endif
