#ifndef INCLUDE_CATCONDITION
#define INCLUDE_CATCONDITION

#include "CatShell/core/CatObject.h"
#include "CatShell/core/Algorithm.h"

#include <string>

namespace CatShell {

class CatCondition : public Algorithm
{
        CAT_OBJECT_DECLARE(CatCondition, Algorithm, CAT_NAME_CATCONDITION, CAT_TYPE_CATCONDITION);
public:

        CatCondition();
                /// Constructor: default.

        virtual ~CatCondition();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

  typedef struct
  {
          CatAddress      address;
          std::string     def;
          std::string     type;
  } tVariable;

  typedef struct
  {
      std::string test;
      std::string type;
      bool a_type;
//      union
//      {
        void*     a_value;
        tVariable a_element;
//      };
      bool b_type;
//      union
//      {
        void*     b_value;
        tVariable b_element;
//      };
  } tTest;

  void read_condition(tTest* condition, Setting& setting);
    /// Reads the condition from the settings.

  void delete_condition(tTest* condition);
    /// Memory cleanup of the condition that is not needed any more.

  bool test(CatPointer<CatObject>& object, tTest& condition, CatShell::Logger& logger);
    /// Performs the test on the object.

  template<class T>
  bool test_cc(CatPointer<CatObject>& object, std::string& test, tVariable& a, tVariable& b);

  template<class T>
  bool test_vc(CatPointer<CatObject>& object, std::string& test, void* a, tVariable& b);

  template<class T>
  bool test_cv(CatPointer<CatObject>& object, std::string& test, tVariable& a, void* b);

  template<class T>
  bool test_vv(std::string& test, T* a, T* b);

private:

  Algorithm::DataPort    _in_port_object;
  Algorithm::DataPort    _out_port_yes;
  Algorithm::DataPort    _out_port_no;

  tTest  _test;
};

inline StreamSize CatCondition::cat_stream_get_size() const
{
        return Algorithm::cat_stream_get_size();
}

inline void CatCondition::cat_stream_out(std::ostream& output)
{
        return Algorithm::cat_stream_out(output);
}

inline void CatCondition::cat_stream_in(std::istream& input)
{
        return Algorithm::cat_stream_in(input);
}

inline void CatCondition::cat_print(std::ostream& output, const std::string& padding)
{
        Algorithm::cat_print(output, padding);
}

template<class T>
bool CatCondition::test_cc(CatPointer<CatObject>& object, std::string& test, tVariable& a, tVariable& b)
{
  void* tmp_a;
  void* tmp_b;
  tmp_a = (object->cat_get_part(a.address));
  tmp_b = (object->cat_get_part(b.address));
  if(!tmp_a || !tmp_b)
    return false;
  else
    return test_vv<T>(test, (T*)tmp_a, (T*)tmp_b);
}

template<class T>
bool CatCondition::test_vc(CatPointer<CatObject>& object, std::string& test, void* a, tVariable& b)
{
  void* tmp;
  tmp = (object->cat_get_part(b.address));
  if(!tmp)
    return false;
  else
    return test_vv<T>(test, (T*)a, (T*)tmp);
}

template<class T>
bool CatCondition::test_cv(CatPointer<CatObject>& object, std::string& test, tVariable& a, void* b)
{
  void* tmp;
  tmp = (object->cat_get_part(a.address));
  if(!tmp)
    return false;
  else
    return test_vv<T>(test, (T*)tmp, (T*)b);
}

template <>
inline bool CatCondition::test_vv<std::uint16_t>(std::string& test, std::uint16_t* a, std::uint16_t* b)
{
  if(test == "==")
  {
    return (*a) == (*b);
  }
  else if(test == ">")
  {
    return (*a) > (*b);
  }
  else if(test == "<")
  {
    return (*a) < (*b);
  }
  else if(test == "%")
  {
    return ((*a) % (*b) == 0);
  }
  else
    return false;
}

template <>
inline bool CatCondition::test_vv<std::uint32_t>(std::string& test, std::uint32_t* a, std::uint32_t* b)
{
  if(test == "==")
  {
    return (*a) == (*b);
  }
  else if(test == ">")
  {
    return (*a) > (*b);
  }
  else if(test == "<")
  {
    return (*a) < (*b);
  }
  else if(test == "%")
  {
    return ((*a) % (*b) == 0);
  }
  else
    return false;
}

template <>
inline bool CatCondition::test_vv<std::string>(std::string& test, std::string* a, std::string* b)
{
  if(test == "==")
  {
    return (*a) == (*b);
  }
  else if(test == "contains")
  {
    return (a->find(*b) != std::string::npos);
  }
  else
    return false;
}

template <class T>
inline bool CatCondition::test_vv(std::string& test, T* a, T* b)
{
        throw CatException(__FILE__, __LINE__, "Condition type not implemented.");
}


} // namespace CatShell

#endif
