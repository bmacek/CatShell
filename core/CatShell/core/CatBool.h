#ifndef INCLUDE_CATBOOL
#define INCLUDE_CATBOOL

#include "CatShell/core/defines.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include <iostream>
#include <vector>

namespace CatShell {

class CatBool : public CatObject
        ///
        /// Double floating number for CatShell.
        ///
{
        CAT_OBJECT_DECLARE(CatBool, CatObject, CAT_NAME_CATBOOL, CAT_TYPE_CATBOOL);

public:
        CatBool();
                /// Constructor: default.

        virtual ~CatBool();
                /// Destructor.

        void operator=(const bool rhs);
                /// Assigment.

        bool value() const;
                /// Returns the value.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatAddress& addr);
                /// Returns the pointer to the desired part.

//----------------------------------------------------------------------

protected:

private: 

        bool _data;
};

inline CatBool::CatBool() { }
inline CatBool::~CatBool() { }
inline StreamSize CatBool::cat_stream_get_size() const { return sizeof(std::uint8_t); }
inline void CatBool::cat_print(std::ostream& output, const std::string& padding) { CatObject::cat_print(output, padding); output << (_data?"true":"false"); }
inline void CatBool::cat_free() { }
inline void CatBool::operator=(const bool rhs) { _data = rhs; }
inline bool CatBool::value() const { return _data; }

} // namespace CatShell

#endif
