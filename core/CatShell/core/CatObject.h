#ifndef INCLUDE_CATOBJECT
#define INCLUDE_CATOBJECT

#include "CatShell/core/defines.h"
#include "CatShell/core/CatAddress.h"

#include <atomic>
#include <iostream>

namespace CatShell {

class MemoryPoolBase;
template<class U> class CatPointer;

class CatObject
        ///
        /// Base class for all data classes.
        /// It's purpose is the memory management and garbage collection.
        ///
        ///
        /// Each derived class should use macros:
        ///   CAT_OBJECT_DECLARE(CLASS, NAME, TYPE) -> in the class definition
        ///   CAT_OBJECT_IMPLEMENT(CLASS, NAME, TYPE) -> in the implementation
        ///
{
        template<class U> friend class MemoryPool;
        template<class U> friend class CatPointer;
public:

        CatObject();
                /// Constructor: default.

        virtual ~CatObject();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const = 0;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output) = 0;
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input) = 0;
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free() = 0;
                /// Free all internal references.

        virtual void* cat_get_part(CatAddress& addr);
                /// Returns the pointer to the desired part.

//----------------------------------------------------------------------

        virtual CatType cat_type() const = 0;
                /// Return the type code of the object.

        virtual const std::string& cat_name() const = 0;
                /// Return the type name of the object.

        std::uint32_t cat_references() const;
                /// Returns how many references to this object exist.

        static bool s_is_derived(const CatType type);

protected:

        virtual MemoryPoolBase* cat_memory_pool() = 0;
                /// Return the memory pool to which the object should be returned.

private: 

        void reference_plus();
                /// Reference increase.

        void reference_minus();
                /// Reference decrease. 
                /// Returns 'true' if the object is no longer referenced.

private:

        mutable std::atomic_uint_least32_t      _ref_count;
};

inline CatObject::CatObject()
: _ref_count(0)
{
}

inline CatObject::~CatObject()
{
}

inline std::uint32_t CatObject::cat_references() const
{
        return (std::uint32_t)_ref_count;
}

inline void CatObject::reference_plus()
{
        ++_ref_count;
}

inline void CatObject::cat_print(std::ostream& output, const std::string& padding)
{
        output << padding << "[" << cat_name() << "] ";
}

inline void* CatObject::cat_get_part(CatAddress& addr)
{
        return nullptr;
}

inline bool CatObject::s_is_derived(const CatType type)
{
        return (type == CAT_TYPE_OBJECT);
}


#define CAT_OBJECT_DECLARE(CLASS, DERIVED, NAME, TYPE)                             \
public:                                                                            \
        virtual CatType cat_type() const {return TYPE;}                            \
        virtual const std::string& cat_name() const {return s_type_name;}          \
        virtual CatShell::MemoryPoolBase* cat_memory_pool() {return CLASS::s_mem;} \
        static CatType s_cat_type() {return TYPE;}                                 \
        static std::string& s_cat_name() {return s_type_name;}                     \
        static void s_init_memory_pool(CatShell::MemoryPoolBase* p) {s_mem = p;}   \
        static bool s_is_derived(const CatType type)                               \
        {                                                                          \
                if(type==TYPE)                                                     \
                        return true;                                               \
                else                                                               \
                        return DERIVED::s_is_derived(type);                        \
        }                                                                          \
private:                                                                           \
        static CatShell::MemoryPoolBase* s_mem;                                    \
        static std::string s_type_name;                                            \
public:                  


#define CAT_OBJECT_IMPLEMENT(CLASS, NAME, TYPE)            \
        CatShell::MemoryPoolBase* CLASS::s_mem = nullptr;  \
        std::string CLASS::s_type_name = NAME;


} // namespace CatShell

#endif
