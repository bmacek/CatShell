#ifndef INCLUDE_CATELEMENT_INCLUDED
#define INCLUDE_CATELEMENT_INCLUDED

#include <iostream>

#include "CatShell/core/CatException.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"
#include "CatShell/core/Setting.h"

namespace CatShell {

//template <class T> class CatElement;

class CatVirtualElement
///
/// Virtual base class for all elements.
///
{
public:

        CatVirtualElement() {}
                /// Constructor.

        virtual ~CatVirtualElement() {}
                /// Destructor.

        virtual void fill(void* var, CatPointer<CatObject> object) = 0;
                /// Fills the storage variable

        virtual const std::string& get_type() const = 0;
            /// Returns the name of the type.

        virtual bool has_dimension() const = 0;
            /// Returns true if index was present.

        virtual std::uint32_t get_dimension() const = 0;
            /// Returns the dimension of array.

        virtual void* create_variable() const = 0;
            /// Returns a pointer to newly vreated variable.

        virtual void delete_variable(void* var) const = 0;
            /// Removes the variable.

        virtual bool are_variables_equal(void* var_I, void* var_II) = 0;
            /// Returns true if data is the same in both variables.

        virtual std::string get_string(void* var) const = 0;
            /// Returns the value in string format.

        static CatVirtualElement* create_element(Setting& setting);
            /// Creates an element form the settings.
};

template <class T>
class CatElement : public CatVirtualElement
///
/// Template class for getting value from elements.
///
{
public:

        CatElement();
                /// Constructor: default.

        CatElement(Setting& setting);
                /// Constructor: create from the settings.

        void define(Setting& setting);
                /// Create from the settings.

        virtual ~CatElement();
                /// Destructor.

        T get_value(CatPointer<CatObject> object);
                /// Returns appropriate value from the object.

        T* get_pointer(CatPointer<CatObject> object);
                /// Returns appropriate value from the object.

        virtual void fill(void* var, CatPointer<CatObject> object);
                /// Fills the storage variable

        bool is_type(const std::string& type) const;
            /// Returns true if eement is of desired type.

        const std::string& get_type() const;
            /// Returns the name of the type.

        bool has_dimension() const;
            /// Returns true if index was present.

        std::uint32_t get_dimension() const;
            /// Returns the dimension of array.

        void* create_variable() const;
            /// Returns a pointer to newly vreated variable.

        void delete_variable(void* var) const;
            /// Removes the variable.

        bool are_variables_equal(void* var_I, void* var_II);
            /// Returns true if data is the same in both variables.

        std::string get_string(void* var) const;
            /// Returns the value in string format.

protected:
private:
        template <class C>
        T translate_value(const C& value);
                /// Return translated type.

        void decode_type_index();
            /// Decode if type has index and extract it.
private:

        CatAddress      _name;
        std::string     _type;
        std::uint32_t   _index;
        T               _default;
};

template <class T>
CatElement<T>::CatElement()
: _type("")
{
}

template <class T>
CatElement<T>::CatElement(Setting& setting)
{
        _name = setting.get_setting<std::string>("name", "");
        _type = setting.get_setting<std::string>("type", "");
        _default = translate_value<std::string>(setting.get_setting<std::string>("default", ""));
        decode_type_index();
}

template <class T>
std::string CatElement<T>::get_string(void* var) const
{
    std::stringstream ss;
    if(_index)
    {
        for(std::uint32_t i=0; i<_index; ++i)
            ss << (*((T*)var)+i) << "_";
    }
    else
    {
        ss << (*(T*)var);
    }
    return ss.str();
}

template <class T>
bool CatElement<T>::are_variables_equal(void* var_I, void* var_II)
{
    if(_index)
    {
        for(std::uint32_t i=0; i<_index; ++i)
            if(*(((T*)var_I)+i) != *(((T*)var_II)+i))
                return false;
        return true;
    }
    else
        return *((T*)var_I) == *((T*)var_II);
}

template <class T>
void* CatElement<T>::create_variable() const
{
    if(_index)
        return new T[_index];
    else
        return new T;
}

template <class T>
void CatElement<T>::delete_variable(void* var) const
{
    if(_index)
        delete [] (T*)var;
    else
        delete (T*)var;
}

template <class T>
CatElement<T>::~CatElement()
{
}

template <class T>
void CatElement<T>::define(Setting& setting)
{
        _name = setting.get_setting<std::string>("name", "");
        _type = setting.get_setting<std::string>("type", "");
        _default = translate_value<std::string>(setting.get_setting<std::string>("default", ""));
        decode_type_index();
}

template <class T> inline bool CatElement<T>::is_type(const std::string& type) const { return _type == type; }
template <class T> inline bool CatElement<T>::has_dimension() const { return _index != 0; }
template <class T> inline std::uint32_t CatElement<T>::get_dimension() const { return _index; }
template <class T> inline const std::string& CatElement<T>::get_type() const { return _type; }

template <class T>
void CatElement<T>::decode_type_index()
{
    // have the substring
    // check if index is present
    size_t p0 = _type.find("[");
    if(p0 == std::string::npos)
    {
        // no index is present
        _index = 0;
        return;
    }
    else
    {
        size_t p1 = _type.find("]", p0);
        if(p1 == std::string::npos)
        {
            // error in index parsing
            _index = 0;
            return;
        }
        else
        {
            // insert it with the index
            _index = (std::uint32_t)atoi(_type.substr(p0+1, p1-p0-1).c_str());
            _type = _type.substr(0,p0);
            return;
        }
    }
}

template <class T>
void CatElement<T>::fill(void* var, CatPointer<CatObject> object)
{
    if(_index)
    {
        T* src = get_pointer(object);
        for(std::uint32_t i=0; i<_index; ++i)
            *(((T*)var)+i) = *(src+i);
    }
    else
        *((T*)var) = get_value(object);
}

template <class T>
T CatElement<T>::get_value(CatPointer<CatObject> object)
{
        void* pointer;
        _name.begin();
        pointer = object->cat_get_part(_name);
        if(!pointer)
                return _default;

        if(_type == "double")
                return translate_value<T>(*(double*)pointer);
        else if(_type == "float")
                return translate_value<T>(*(float*)pointer);
        else if(_type == "std::uint64_t")
               return translate_value<T>(*(std::uint64_t*)pointer);
        else if(_type == "std::uint32_t")
               return translate_value<T>(*(std::uint32_t*)pointer);
        else if(_type == "std::uint16_t")
                return translate_value<T>(*(std::uint16_t*)pointer);
        else if(_type == "std::uint8_t")
                return translate_value<T>(*(std::uint8_t*)pointer);
        else if(_type == "bool")
                return translate_value<T>(*(bool*)pointer);
        else
                throw CatException(__FILE__, __LINE__, "Type unrecognised: " + _type);
}

template <class T>
T* CatElement<T>::get_pointer(CatPointer<CatObject> object)
{
        void* pointer;
        _name.begin();
        pointer = object->cat_get_part(_name);
        return static_cast<T*>(pointer);
}

// bool

template <>
template <>
inline bool CatElement<bool>::translate_value(const std::string& value)
{
    if(value == "true")
        return true;
    else
        return false;
}

template <>
template <>
inline bool CatElement<bool>::translate_value(const bool& value)
{
    return value;
}

// std::uint8_t

template <>
template <>
inline std::uint8_t CatElement<std::uint8_t>::translate_value(const std::string& value)
{
        return (std::uint8_t)atoi(value.c_str());
}

template <>
template <>
inline std::uint8_t CatElement<std::uint8_t>::translate_value(const std::uint8_t& value)
{
        return value;
}

// std::uint16_t

template <>
template <>
inline std::uint16_t CatElement<std::uint16_t>::translate_value(const std::string& value)
{
        return (std::uint16_t)atoi(value.c_str());
}

template <>
template <>
inline std::uint16_t CatElement<std::uint16_t>::translate_value(const std::uint16_t& value)
{
        return value;
}

template <>
template <>
inline std::uint16_t CatElement<std::uint16_t>::translate_value(const std::uint8_t& value)
{
        return value;
}

// std::uint32_t

template <>
template <>
inline std::uint32_t CatElement<std::uint32_t>::translate_value(const std::string& value)
{
        return (std::uint32_t)atoi(value.c_str());
}

template <>
template <>
inline std::uint32_t CatElement<std::uint32_t>::translate_value(const std::uint32_t& value)
{
        return value;
}

template <>
template <>
inline std::uint32_t CatElement<std::uint32_t>::translate_value(const std::uint16_t& value)
{
        return value;
}

template <>
template <>
inline std::uint32_t CatElement<std::uint32_t>::translate_value(const std::uint8_t& value)
{
        return value;
}

// std::uint64_t

template <>
template <>
inline std::uint64_t CatElement<std::uint64_t>::translate_value(const std::string& value)
{
        return (std::uint32_t)atoi(value.c_str());
}

template <>
template <>
inline std::uint64_t CatElement<std::uint64_t>::translate_value(const std::uint64_t& value)
{
        return value;
}

template <>
template <>
inline std::uint64_t CatElement<std::uint64_t>::translate_value(const std::uint32_t& value)
{
        return value;
}

template <>
template <>
inline std::uint64_t CatElement<std::uint64_t>::translate_value(const std::uint16_t& value)
{
        return value;
}

template <>
template <>
inline std::uint64_t CatElement<std::uint64_t>::translate_value(const std::uint8_t& value)
{
        return value;
}

// float

template <>
template <>
inline float CatElement<float>::translate_value(const std::string& value)
{
        return atof(value.c_str());
}

template <>
template <>
inline float CatElement<float>::translate_value(const std::uint64_t& value)
{
        return value;
}

template <>
template <>
inline float CatElement<float>::translate_value(const std::uint32_t& value)
{
        return value;
}

template <>
template <>
inline float CatElement<float>::translate_value(const std::uint16_t& value)
{
        return value;
}

template <>
template <>
inline float CatElement<float>::translate_value(const std::uint8_t& value)
{
        return value;
}

template <>
template <>
inline float CatElement<float>::translate_value(const float& value)
{
        return value;
}

// double

template <>
template <>
inline double CatElement<double>::translate_value(const std::string& value)
{
        return atof(value.c_str());
}

template <>
template <>
inline double CatElement<double>::translate_value(const std::uint64_t& value)
{
        return value;
}

template <>
template <>
inline double CatElement<double>::translate_value(const std::uint32_t& value)
{
        return value;
}

template <>
template <>
inline double CatElement<double>::translate_value(const std::uint16_t& value)
{
        return value;
}

template <>
template <>
inline double CatElement<double>::translate_value(const std::uint8_t& value)
{
        return value;
}

template <>
template <>
inline double CatElement<double>::translate_value(const float& value)
{
        return value;
}

template <>
template <>
inline double CatElement<double>::translate_value(const double& value)
{
        return value;
}

template <class T>
template <class C>
T CatElement<T>::translate_value(const C& value)
{
        throw CatException(__FILE__, __LINE__, "Conversion not implemented.");
}
/**/

} // namespace CatShell

#endif
