#ifndef INCLUDE_PLUGINDEAMON
#define INCLUDE_PLUGINDEAMON

#include "CatShell/core/Deamon.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/Algorithm.h"
#include "CatShell/core/Process.h"
#include "CatShell/core/ClassManager.h"

#include <set>

namespace CatShell {

class PluginDeamon : public Deamon
        /// Base class for all apps that run as deamon
        /// processes and have the plugin infrastructure. It has build in logging, settings,
        /// and shutdown procedures.
       
        /// The settings file can be specified with parameter -s <file>
        /// If not, it searches for the <programName>.xml. If nothing is found
        /// error occures.
        ///
        /// Setting file, shoud be a well formatted XML file.
        ///
        /// Minimal settings for a file should be the logging settings, since the logging
        /// infrastructure is automatically setup.
{
CAT_OBJECT_DECLARE(PluginDeamon, Deamon, "CatPluginDeamon", CAT_TYPE_PLUGINDEAMON)
public:

        PluginDeamon();
                /// Constructor: default.

        virtual ~PluginDeamon();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatPointer<WorkThread> child);
                /// Called when a subprocess finished.

//----------------------------------------------------------------------

        void load_plugin(const std::string& path, const std::string& name, Setting* setting);
                /// Loads the plugin if not loaded already.
                /// Throws PluginException on failure.

        void unload_plugin(const std::string& name);
                /// Starts the unloading process of the plugin.
                /// Throws PluginException on failure.

        static MemoryPoolBase* get_memory_pool(const std::string& type);
                /// Returns the memory pool or nullptr if not found.

        static MemoryPoolBase* get_memory_pool(CatType type);
                /// Returns the memory pool or nullptr if not found.

        template <class T>
        static MemoryPool<T>* get_pool();
                /// Returns the pointer to an object.

        static void stream_object_out(std::ostream& output, CatPointer<CatObject> object);
                /// Streams the object to the stream.

        static CatPointer<CatObject> stream_object_in(std::istream& input);
                /// Streams the object form the stream. Null pointer returned if nothing found.

        static StreamSize stream_object_size(CatPointer<CatObject> object);
                /// Returns the size needed for streaming this object.

        static void Stack();
                /// Prints stack info in the logfile under Error severity.

	bool is_derived(const std::string& base, const std::string& derived);
		/// Returns 'true' if 'derived' is derived from 'base'.

protected:


private:

        MemoryPoolBase* get_memory_pool_private(const std::string& type);
                /// Returns the memory pool or nullptr if not found.
                /// Does not lock the mutex!

        ClassManagerBase* get_class_private(const std::string& type);
                /// Returns the memory pool or nullptr if not found.
                /// Does not lock the mutex!

        std::uint32_t remove_free_plugins();
                /// Removes all plugins marked for removal that are also inactive.

        CatPointer<Algorithm> get_algorithm(const std::string& name);
                /// Return the process pointer.

        bool register_memory_pool(MemoryPoolBase* mp, ClassManagerBase* cm);
                /// Adds the pool to the list of available ones.

        void unregister_memory_pool(MemoryPoolBase* mp);
                /// Removes the pool from the list of available ones.

        void finish();
                /// Finish with deregistering algorithms, and plugins.

private:

        typedef struct
        {
                // address
                std::string     name;
                std::string     path;
                void*           handle;
                // process
                bool            waiting_shutdown;
                // functions
                bool (*f_config)(Setting*, Logger*, const std::string&);
                bool (*f_init)(void);
                bool (*f_deinit)(void);
                bool (*f_get_data_types)(std::set<std::string>&);
                MemoryPoolBase* (*f_get_memory_pool)(const std::string&);
                ClassManagerBase* (*f_get_class_manager)(const std::string&);
                bool (*f_enable)(void);
                bool (*f_disable)(void);
                // memory pools
                std::map<std::string, MemoryPoolBase*>          m_pools;
        } plugin_t;

        std::vector<plugin_t>   _plugins;

	typedef struct {
		MemoryPoolBase* pool;
		ClassManagerBase* manager;
	} Base;

	std::map<std::string, Base>   _pool_name;
        std::map<CatType, Base>       _pool_type;

//        std::map<std::string, MemoryPoolBase*>   _pool_name;
//        std::map<CatType, MemoryPoolBase*>       _pool_type;

        std::map<std::string, CatPointer<Algorithm>>   _algorithms;

        typedef std::map<std::string, CatPointer<Process>> process_list_t;
        std::vector<process_list_t*>                     _processes;
        process_list_t*                                  _current_batch;
        //std::map<std::string, CatPointer<Process>>     _processes;

        static PluginDeamon* s_instance;
};

template <class T>
inline MemoryPool<T>* PluginDeamon::get_pool()
{
        return (MemoryPool<T>*)s_instance->get_memory_pool(T::s_cat_type());
}

inline StreamSize PluginDeamon::stream_object_size(CatPointer<CatObject> object) { return sizeof(StreamSize) + sizeof(CatType) + object->cat_stream_get_size(); }

} // namespace CatShell

#endif
