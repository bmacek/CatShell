#ifndef INCLUDE_CATEXCEPTION
#define INCLUDE_CATEXCEPTION

#include <exception>
#include <string>

namespace CatShell {

class CatException : public std::exception
        ///
        /// Base class for all exceptions.
        ///
{
public:
        CatException();
        CatException(const std::string& file, unsigned int line, const std::string& what_arg);
        CatException(const std::string& file, unsigned int line, const char* what_arg);

        virtual ~CatException();
       
        virtual const char* what() const noexcept;
                /// Displays the name of the exception.
               
        const std::string& message() const;
                /// Displays the message of the exception.
               
        std::string explain() const;
                /// Displays the entire exception info.
       
private:
        std::string     _file;
        unsigned int    _line;
        std::string     _message;
       
};

//
// Macros for quickly declaring and implementing exception classes.
//

#define BASE_DECLARE_EXCEPTION(CLS, BASE)                                                       \
        class CLS: public BASE                                                                  \
        {                                                                                       \
        public:                                                                                 \
                CLS();                                                                          \
                CLS(const std::string& file, unsigned int line, const std::string& what_arg);   \
                CLS(const std::string& file, unsigned int line, const char* what_arg);          \
                virtual ~CLS() throw();                                                         \
                virtual const char* what() const noexcept;                                      \
        };



#define BASE_IMPLEMENT_EXCEPTION(CLS, BASE, NAME)                                               \
        CLS::CLS()                                                                              \
        : BASE()                                                                                \
        {                                                                                       \
        }                                                                                       \
        CLS::CLS(const std::string& file, unsigned int line, const std::string& what_arg)       \
        : BASE(file, line, what_arg)                                                            \
        {                                                                                       \
        }                                                                                       \
        CLS::CLS(const std::string& file, unsigned int line, const char* what_arg)              \
        : BASE(file, line, what_arg)                                                            \
        {                                                                                       \
        }                                                                                       \
        CLS::~CLS() throw()                                                                     \
        {                                                                                       \
        }                                                                                       \
        const char* CLS::what() const noexcept                                                  \
        {                                                                                       \
                return NAME;                                                                    \
        }
       

BASE_DECLARE_EXCEPTION(SettingCatException, CatException)
BASE_DECLARE_EXCEPTION(NullCatException, CatException)
BASE_DECLARE_EXCEPTION(OutOfMemoryCatException, CatException)
BASE_DECLARE_EXCEPTION(PluginCatException, CatException)

} // namespace CatShell

#endif
