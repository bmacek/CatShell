#ifndef INCLUDE_CATDOUBLE
#define INCLUDE_CATDOUBLE

#include "CatShell/core/defines.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include <iostream>
#include <vector>

namespace CatShell {

class CatDouble : public CatObject
        ///
        /// Double floating number for CatShell.
        ///
{
        CAT_OBJECT_DECLARE(CatDouble, CatObject, CAT_NAME_CATDOUBLE, CAT_TYPE_CATDOUBLE);

public:
        CatDouble();
                /// Constructor: default.

        virtual ~CatDouble();
                /// Destructor.

        void operator=(const double rhs);
                /// Assigment.

        double value() const;
                /// Returns the value.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatAddress& addr);
                /// Returns the pointer to the desired part.

//----------------------------------------------------------------------

protected:

private: 

        double _data;
};

inline CatDouble::CatDouble() { }
inline CatDouble::~CatDouble() { }
inline StreamSize CatDouble::cat_stream_get_size() const { return sizeof(double); }
inline void CatDouble::cat_stream_out(std::ostream& output) { output.write((char*)&_data, sizeof(double)); }
inline void CatDouble::cat_stream_in(std::istream& input) { input.read((char*)&_data, sizeof(double)); }
inline void CatDouble::cat_print(std::ostream& output, const std::string& padding) { CatObject::cat_print(output, padding); output << _data; }
inline void CatDouble::cat_free() { }
inline void CatDouble::operator=(const double rhs) { _data = rhs; }
inline double CatDouble::value() const { return _data; }

} // namespace CatShell

#endif
