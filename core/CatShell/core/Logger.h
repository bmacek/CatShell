#ifndef INCLUDE_LOGGER
#define INCLUDE_LOGGER

#include "CatShell/core/defines.h"
#include "CatShell/core/LimitedFile.h"

#include <mutex>
#include <iostream>

namespace CatShell {

//
// Logger
//

class Logger
{
public:

	Logger() = delete;
		/// Constructor: default.

	Logger(const std::string& name);
		/// Constructor: default.

	virtual ~Logger();
		/// Destructor.

	void startNoteDEBUG(bool lock=true);
		/// Starts the DEBUG entry to the log.
	void startNoteINFO(bool lock=true);
		/// Starts the INFO entry to the log.
	void startNoteLOG(bool lock=true);
		/// Starts the LOG entry to the log.
	void startNoteWARNING(bool lock=true);
		/// Starts the WARNING entry to the log.
	void startNoteERROR(bool lock=true);
		/// Starts the ERROR entry to the log.
	void startNoteFATAL(bool lock=true);
		/// Starts the FATAL entry to the log.

	void endNote();
		/// Ends all entries.

	void lock();
		/// Lock the mutex.

	std::string padding() const;
		/// Returns the padding for formatting the beggining of the lines.

	bool isGood();
		/// Is the logger stil active.

	void full_name(std::ostream& output) const;
		/// Puts the full logger name in the stream

	Logger* get_sublogger(const std::string& name);
		/// Get child logger. 

	virtual std::ostream& stream();
		/// Returns the stream, where one can input the message.

	virtual void flush();
		/// It empties the buffers.

protected:

	Logger(const std::string& name, Logger* parenth);
		/// Constructor: constructs a logger with  parenth.

	bool		_good;

private:
	std::mutex*		_lock;
	std::string		_name;
	std::string		_state;
	Logger*			_parenth;
};

inline void Logger::flush()
{
	if(_parenth)
		_parenth->flush();
}

inline bool Logger::isGood()
{
	return _good;
}

inline void Logger::lock()
{
	_lock->lock();
}


//
// FileLogger
//

class FileLogger : public Logger
        /// Takes care of logging in files.
{
public:
	FileLogger() = delete;
		/// Constructor: default.

	FileLogger(const std::string& log_name, const std::string& file_name, std::uint16_t max_files, std::uint32_t max_length);
		/// Constructor: opens a file sequence

	virtual ~FileLogger();
		/// Destructor.

	virtual std::ostream& stream();
		/// Returns the stream, where one can input the message.

	virtual void flush();
		/// It empties the buffers.

private:
       
	LimitedFile _file;
};

#ifdef PRINT_LOG_DEBUG
        #define DEBUG(LOG, MESSAGE) if(LOG.isGood()){LOG.startNoteDEBUG(true); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl; LOG.endNote();}
        #define START_LOG_DEBUG(LOG) if(LOG.isGood()){LOG.lock();
		#define LINE_LOG_DEBUG(LOG, MESSAGE) LOG.startNoteDEBUG(false); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl;
#else
        #define DEBUG(LOG, MESSAGE)
        #define START_LOG_DEBUG(LOG) if(false){
		#define LINE_LOG_DEBUG(LOG, MESSAGE)
#endif
#ifdef PRINT_LOG_INFO
        #define INFO(LOG, MESSAGE) if(LOG.isGood()){LOG.startNoteINFO(true); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl; LOG.endNote();}
        #define START_LOG_INFO(LOG) if(LOG.isGood()){LOG.lock();
		#define LINE_LOG_INFO(LOG, MESSAGE) LOG.startNoteINFO(false); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl;
#else
        #define INFO(LOG, MESSAGE)
        #define START_LOG_INFO(LOG) if(false){
		#define LINE_LOG_INFO(LOG, MESSAGE) 
#endif
#ifdef PRINT_LOG_LOG
        #define LOG(LOG, MESSAGE) if(LOG.isGood()){LOG.startNoteLOG(true); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl; LOG.endNote();}
        #define START_LOG_LOG(LOG) if(LOG.isGood()){LOG.lock();
		#define LINE_LOG_LOG(LOG, MESSAGE) LOG.startNoteLOG(false); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl;
#else
        #define LOG(LOG, MESSAGE)
        #define START_LOG_LOG(LOG) if(false){
		#define LINE_LOG_LOG(LOG, MESSAGE)
#endif
#ifdef PRINT_LOG_WARNING
        #define WARNING(LOG, MESSAGE) if(LOG.isGood()){LOG.startNoteWARNING(true); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl; LOG.endNote();}
        #define START_LOG_WARNING(LOG) if(LOG.isGood()){LOG.lock();
		#define LINE_LOG_WARNING(LOG, MESSAGE) LOG.startNoteWARNING(false); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl;
#else
        #define WARNING(LOG, MESSAGE)
        #define START_LOG_WARNING(LOG) if(false){
		#define LINE_LOG_WARNING(LOG, MESSAGE)
#endif
#ifdef PRINT_LOG_ERROR
        #define ERROR(LOG, MESSAGE) if(LOG.isGood()){LOG.startNoteERROR(true); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl; LOG.endNote();}
        #define START_LOG_ERROR(LOG) if(LOG.isGood()){LOG.lock();
		#define LINE_LOG_ERROR(LOG, MESSAGE) LOG.startNoteERROR(false); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl;
#else
        #define ERROR(LOG, MESSAGE)
        #define START_LOG_ERROR(LOG) if(false){
		#define LINE_LOG_ERROR(LOG, MESSAGE)
#endif
#ifdef PRINT_LOG_FATAL
        #define FATAL(LOG, MESSAGE) if(LOG.isGood()){LOG.startNoteFATAL(true); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl; LOG.endNote();}
        #define START_LOG_FATAL(LOG) if(LOG.isGood()){LOG.lock();
		#define LINE_LOG_FATAL(LOG, MESSAGE) LOG.startNoteFATAL(false); LOG.full_name(LOG.stream()); LOG.stream() << " " << MESSAGE << std::endl;
#else
        #define FATAL(LOG, MESSAGE)
        #define START_LOG_FATAL(LOG) if(false){
		#define LINE_LOG_FATAL(LOG, MESSAGE)
#endif

#define END_LOG(LOG) LOG.endNote();}

} // namespace CatShell

#endif

