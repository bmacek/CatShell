#ifndef INCLUDE_CATPRINT
#define INCLUDE_CATPRINT

#include "CatShell/core/CatObject.h"
#include "CatShell/core/Algorithm.h"

namespace CatShell {

class CatPrint : public Algorithm
{
        CAT_OBJECT_DECLARE(CatPrint, Algorithm, CAT_NAME_CATPRINT, CAT_TYPE_CATPRINT);
public:

        CatPrint();
                /// Constructor: default.

        virtual ~CatPrint();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

        Algorithm::DataPort    _in_port_object;
        std::uint32_t          _counter;
};

inline StreamSize CatPrint::cat_stream_get_size() const
{
        return Algorithm::cat_stream_get_size();
}

inline void CatPrint::cat_stream_out(std::ostream& output)
{
        return Algorithm::cat_stream_out(output);
}

inline void CatPrint::cat_stream_in(std::istream& input)
{
        return Algorithm::cat_stream_in(input);
}

inline void CatPrint::cat_print(std::ostream& output, const std::string& padding)
{
        Algorithm::cat_print(output, padding);
}


} // namespace CatShell

#endif

