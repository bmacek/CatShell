#ifndef INCLUDE_LIMITEDFILE
#define INCLUDE_LIMITEDFILE

#include <iostream>
#include <fstream>
#include <string>

#include "CatShell/core/defines.h"

namespace CatShell {

//
// LimitedFile
//

class LimitedFile
        /// Takes care of file managing. It is not multithread safe.
        /// On flush it checkes the file size and creates new file if
        /// current one exceeds the limit.
{
public:
	LimitedFile() = delete;
		/// Constructor: default.

	LimitedFile(const std::string& name, const std::string& extension, std::ios::openmode openMode, std::uint32_t sizeLimit, bool cyclic, std::uint32_t cyclic_top, bool time_stamp, std::uint32_t startFileNumber = 0);
		/// Constructor: opening a file.

	virtual ~LimitedFile();
		/// Destructor.

	std::ofstream& write( const char* s , std::streamsize n );
		/// Writes the data to the file.

	std::ostream& flush();
		/// Deals with the file switching if size limit is exceeded.

	std::ostream& get_stream();
		/// Returns the stream.

private:

	void setNewCurrentFileName(bool increaseFileNumber = true);
		/// It constructs the next file name.

private:

	std::uint32_t		_sizeLimit;
	std::uint32_t		_fileNumber;
	std::string			_name;
	std::string			_extension;
	std::string			_currentName;
	std::ios::openmode	_openMode;
	bool 				_cyclic;
	std::uint32_t		_cycle_top;

	std::ofstream*		_file;
};

inline std::ofstream& LimitedFile::write(const char* s, std::streamsize n)
{
	//write it to the file
	_file->write(s, n);    
	return *_file;
	}

inline std::ostream& LimitedFile::get_stream()
{
	return *_file;
}

} // namespace CatShell

#endif

