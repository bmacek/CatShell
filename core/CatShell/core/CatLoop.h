#ifndef INCLUDE_CATLOOP
#define INCLUDE_CATLOOP

#include "CatShell/core/CatObject.h"
#include "CatShell/core/Process.h"

#include <queue>
#include <fstream>
#include <iostream>

namespace CatShell {

class CatLoop : public Process
{
        CAT_OBJECT_DECLARE(CatLoop, Process, CAT_NAME_CATLOOP, CAT_TYPE_CATLOOP);
public:

        CatLoop();
                /// Constructor: default.

        virtual ~CatLoop();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

        virtual CatShell::CatPointer<CatShell::CatObject> produce_data(DataPort port, bool& flush, CatShell::Logger& logger);
                /// Produces data on request. Secont parameter that is returned if a flush flag.
                /// Object is taken form the first one that producess a result.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatPointer<WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------

protected:
private:
private:

        Algorithm::DataPort    _in_port_object;
        Algorithm::DataPort    _out_port_object;

        std::uint64_t _counter;
        std::uint64_t _filter_start;
        std::uint64_t _filter_end;
};

inline StreamSize CatLoop::cat_stream_get_size() const { return Process::cat_stream_get_size(); }
inline void CatLoop::cat_stream_out(std::ostream& output) { return Process::cat_stream_out(output); }
inline void CatLoop::cat_stream_in(std::istream& input) { return Process::cat_stream_in(input); }
inline void CatLoop::cat_print(std::ostream& output, const std::string& padding) { Process::cat_print(output, padding); }

inline void CatLoop::algorithm_deinit() { Process::algorithm_deinit(); }

inline void CatLoop::work_init() { Process::work_init(); }
inline void CatLoop::work_deinit() { Process::work_deinit(); }
inline void CatLoop::work_subthread_finished(CatPointer<WorkThread> child) { }


} // namespace CatShell

#endif

