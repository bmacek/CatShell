#include <cstdint>

//
// Constants
//

#define SW_BASE_VERSION				"1.3"

#define SW_SYS_VARIABLE				"CATSHELL"

#define SW_LOG_VARIABLE				"CATSHELL_LOGS"

#ifdef CATSHELL_MACOSX
	#define PLUGIN_EXTENTION	".dylib"
#else
	#define PLUGIN_EXTENTION	".so"
#endif

//
// Memory pool
//

#define DEFAULT_MEMORY_POOL_MAX_SIZE	(1000)

//
// Type definitions
//

typedef std::uint32_t	StreamSize;
typedef std::uint32_t	CatType;

//
// Logging
//

//#define PRINT_LOG_DEBUG
#define PRINT_LOG_INFO
#define PRINT_LOG_LOG
#define PRINT_LOG_WARNING
#define PRINT_LOG_ERROR
#define PRINT_LOG_FATAL

//
// Cat Types
//

#define CAT_TYPE_OBJECT          (0x00)
#define CAT_NAME_OBJECT          "CatObject"
#define CAT_TYPE_DEAMON          (0x01)
#define CAT_NAME_DEAMON          "CatDaemon"
#define CAT_TYPE_PLUGINDEAMON    (0x02)
#define CAT_NAME_PLUGINDEAMON    "CatPluginDaemon"
#define CAT_TYPE_MESSAGE         (0x03)
#define CAT_NAME_MESSAGE         "CatMessage"
#define CAT_TYPE_MESSAGE_PRINT   (0x04)
#define CAT_NAME_MESSAGE_PRINT   "CatMessagePrint"
#define CAT_TYPE_WATCHDOG        (0x05)
#define CAT_NAME_WATCHDOG        "CatWatchdog"
#define CAT_TYPE_CATMEMPAGE      (0x06)
#define CAT_NAME_CATMEMPAGE      "CatMemPage"
#define CAT_TYPE_CATPRINT        (0x07)
#define CAT_NAME_CATPRINT        "CatPrint"
#define CAT_TYPE_CATSTDOUT       (0x08)
#define CAT_NAME_CATSTDOUT       "CatStdOut"
#define CAT_TYPE_CATSTDIN        (0x09)
#define CAT_NAME_CATSTDIN        "CatStdIn"
#define CAT_TYPE_CATMEMORY       (0x0a)
#define CAT_NAME_CATMEMORY       "CatMemory"
#define CAT_TYPE_CATFILEIN       (0x0b)
#define CAT_NAME_CATFILEIN       "CatFileIn"
#define CAT_TYPE_CATCOLLECTION   (0x0c)
#define CAT_NAME_CATCOLLECTION   "CatCollection"
#define CAT_TYPE_CATHELLO        (0x0d)
#define CAT_NAME_CATHELLO        "CatHello"
#define CAT_TYPE_CATPARTPRINT    (0x0e)
#define CAT_NAME_CATPARTPRINT    "CatPartPrint"
#define CAT_TYPE_CATLOOP         (0x0f)
#define CAT_NAME_CATLOOP         "CatLoop"
#define CAT_TYPE_CATELEMENTSPY   (0x10)
#define CAT_NAME_CATELEMENTSPY   "CatElementSpy"
#define CAT_TYPE_CATDOUBLE       (0x11)
#define CAT_NAME_CATDOUBLE       "CatDouble"
#define CAT_TYPE_CATUINT32       (0x12)
#define CAT_NAME_CATUINT32       "CatUint32"
#define CAT_TYPE_CATGENERATOR    (0x13)
#define CAT_NAME_CATGENERATOR    "CatGenerator"
#define CAT_TYPE_CATBOOL         (0x14)
#define CAT_NAME_CATBOOL         "CatBool"
#define CAT_TYPE_CATFILEOUT      (0x16)
#define CAT_NAME_CATFILEOUT      "CatFileOut"
#define CAT_TYPE_CATMESSAGEFORMATTER (0x18)
#define CAT_NAME_CATMESSAGEFORMATTER "CatMessageFormatter"
#define CAT_TYPE_CATPOISSON      (0x1031)
#define CAT_NAME_CATPOISSON      "CatPoisson"
#define CAT_TYPE_CATTHRESHOLD    (0x1038)
#define CAT_NAME_CATTHRESHOLD    "CatThreshold"
#define CAT_TYPE_CATSORT         (0x00000017)
#define CAT_NAME_CATSORT         "CatSort"
#define CAT_TYPE_CATCONDITION    (0x103e)
#define CAT_NAME_CATCONDITION    "CatCondition"
#define CAT_TYPE_CATCOUNTER      (0x103f)
#define CAT_NAME_CATCOUNTER      "CatCounter"
#define CAT_TYPE_CATEVENTFILE			(0x00001040)
#define CAT_NAME_CATEVENTFILE			"CatEventFile"
#define CAT_TYPE_CATEVENT        (0x1041)
#define CAT_NAME_CATEVENT        "CatEvent"
#define CAT_TYPE_CATEVENTMESSAGE (0x1042)
#define CAT_NAME_CATEVENTMESSAGE "CatEventMessage"
