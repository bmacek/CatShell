#ifndef INCLUDE_CATSERVICE_INCLUDED
#define INCLUDE_CATSERVICE_INCLUDED

#include <iostream>

namespace CatShell {

class CatService
///
/// General purpose functions.
///
{
public:

        CatService() = delete;
                /// Constructor: default.

        virtual ~CatService() = delete;
                /// Destructor.

        static void write7bitEncoded(std::ostream& output, std::uint32_t value);
                /// Writes 32-bit integer as 7-bit encoded stream.

        static std::uint32_t read7bitEncoded(std::istream& input);
                /// Reads integer from 7-bit encoded stream.

        static std::uint32_t lengtOf7bitEncoded(std::uint32_t value);
                /// Returns the size of 7-bit encoded stream for given value.

        static std::uint32_t reverse_bits(std::uint32_t x);
                /// Reverses the order of bits within the variable.

protected:
private:
};

} // namespace CatShell

#endif

