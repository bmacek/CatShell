#ifndef INCLUDE_SETTING
#define INCLUDE_SETTING

#include <cstdint>
#include <functional>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <map>

#include "CatShell/core/defines.h"
#include "CatShell/core/tinyxml2.h"
#include "CatShell/core/CatException.h"

namespace CatShell {

template <class T> class ValueSetting;
class SettingParameters;

class Setting
	///
	/// Base class for all the settings.
	///
{
public:

	typedef std::function<bool (Setting*)> callback_funct_t;
	typedef struct 
	{
		std::string     name;
		Setting*		setting;
	} setting_entry_t;

public:

	Setting() = delete;
		/// Constructor: default.

	Setting(const std::string& name, Setting* parenth = nullptr);
		/// Constructor: named setting.

	virtual ~Setting();
		/// Destructor.

	const std::string& name() const;
		/// Returns the name of the setting.

	std::string name_full() const;
		/// Get full name.

	void register_call_back(std::function<bool (Setting*)> cb);
		/// Each update of the setting the function will be called.
		/// Function: receives pointer to the setting
		///	and return wether it should be called again if setting changes.

	Setting& operator[](const std::string& key);
		/// Returns a child setting.

	bool add_element(Setting* setting);
		/// Adds a sub element of a group.

	bool has_element(const std::string& key) const;
		/// Returns if sub-element with a given name exist.

	Setting& create_element(const std::string& name);
		/// Create an element or a tree of elements. Returns the created element.

	virtual void print(std::ostream& output, const std::string& padding);
		/// Prints the setting in human readable form. 

	template <class T>
	T get_setting(const std::string& name, const T& def);
		/// Return the value of the setting, or given default if setting is not accessible or it does not have a value.

	template <class T>
	T get_value();
		/// Return the value of this setting.

	Setting* get_setting_node(const std::string& name, Setting* def);
		/// Return the value of the setting, or given default if setting is not accessible or it does not have a value.

	Setting* get_setting_if(const std::string& name, std::function<bool (Setting*)> condition);
		/// Returns the first subgroup that satisfies the condition. nullptr if none is found.

	class iterator : public std::iterator<std::forward_iterator_tag, Setting*>
	{
	public:
		iterator() : _map(nullptr) {}
		iterator(const iterator& it) : _map(it._map), _it(it._it) {}
		iterator(Setting* map, bool start) : _map(map), _it(start ? map->_children.begin() : map->_children.end()) {}
		~iterator() {}

		iterator& operator=(const iterator& rhs)
		{
			_map = rhs._map;
			_it = rhs._it;
			return *this;
		}

		bool operator==(const iterator& rhs) const
		{
			return (_map==rhs._map) && (_it==rhs._it);
		}

		bool operator!=(const iterator& rhs) const
		{
			return (_map!=rhs._map) || (_it!=rhs._it);
		}

		iterator& operator++()
		{
			if(_it != _map->_children.end())
				++_it;
			return *this;
		}

		iterator operator++(int)
		{
			iterator tmp(*this);
			if(_it != _map->_children.end())
				++_it;
			return tmp;
		}

		Setting& operator*()
		{
			return *_it->setting;
		}

		Setting* operator->()
		{
			return _it->setting;
		}

	protected:


	private:
		Setting*								_map;
		std::vector<setting_entry_t>::iterator	_it;
	};

	iterator begin();
		/// Returns the iterator to the first element in the map.

	iterator end();
		/// Returns the iterator to the past-the-end element.

protected:

	void update();
		/// Notifies everyone who is interested.

private:

	std::string						_name;
	std::vector<callback_funct_t>	_callbacks;
	Setting*						_parent;
	std::vector<setting_entry_t>	_children;
};

inline const std::string& Setting::name() const
{
	return _name;
}

inline Setting::iterator Setting::begin()
{
	return iterator(this, true);
}

inline Setting::iterator Setting::end()
{
	return iterator(this, false);
}

template <class T>
T Setting::get_setting(const std::string& name, const T& def)
{
	// split the string into tokens
	std::istringstream iss(name);
	std::string s;    

	Setting* sett = this;
	Setting* sett_next = nullptr;

	while (getline(iss, s, '/'))
	{
		// find the sub-setting
		sett_next = nullptr;
		for(auto& x: sett->_children)
		{
			if(x.name == s)
			{
				sett_next = x.setting;
				break;
			}
		}

		if(sett_next == nullptr)
		{
			// sub-setting not found
			return def;
		}
		else
			sett = sett_next;
	}

	return ((ValueSetting<T>*)sett)->value();
}

template <class T>
T Setting::get_value()
{
	return ((ValueSetting<T>*)this)->value();
}



//
// ValueSetting
//

template <class T>
class ValueSetting : public Setting
	///
	/// Integer setting.
	///
{
public:
	ValueSetting() = delete;
		/// Constructor.

	ValueSetting(const std::string& name, const T& value, Setting* parent = nullptr);
		/// Constructor.

	virtual ~ValueSetting();
		/// Destructor.

	virtual void print(std::ostream& output, const std::string& padding);
		/// Prints the setting in human readable form. 
	
	// specifics

	const T& value() const;
		/// Returns the value.

	void operator=(const T& value);
		/// Asigment operator.

protected:
private:

	T	_value;
};

template <class T>
ValueSetting<T>::ValueSetting(const std::string& name, const T& value, Setting* parent)
: Setting(name, parent), _value(value)
{
}

template <class T>
ValueSetting<T>::~ValueSetting()
{
}

template <class T>
inline const T& ValueSetting<T>::value() const
{
	return _value;
}

template <class T>
inline void ValueSetting<T>::operator=(const T& value)
{
	_value = value;
	update();
}

template <class T>
inline void ValueSetting<T>::print(std::ostream& output, const std::string& padding)
{
	output << padding << name() << " = " << _value << std::endl;
}

template <>
inline void ValueSetting<std::uint8_t>::print(std::ostream& output, const std::string& padding)
{
	output << padding << name() << " = " << (int)_value << std::endl;
}

//
// SettingParser
//

class SettingParser
{
public:

	static Setting* settings_from_xml(const std::string& name, const std::string& extension, SettingParameters& parameters);
		/// Reads the XML file and converts it into settings.

protected:
private:

	static Setting* xml_2_settings(tinyxml2::XMLElement* element, SettingParameters& parameters);
		/// Recursion function.
};

class SettingParameters
	///
	/// Substitution list for settings.
	///
{
public:

	SettingParameters();
		/// Constructor: default.

	virtual ~SettingParameters();
		/// Destructor.

	void insert_substitution(std::uint32_t serial, std::string value);
		/// Inserts the substitution rule.

	std::string transform_setting(const std::string& original);
		/// Substitutes the parameters in the setting.

protected:
private:

	std::map<std::uint32_t, std::string>	_substitutions;
};

} // namespace CatShell

#endif

