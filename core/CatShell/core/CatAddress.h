#ifndef INCLUDE_CATADDRESS
#define INCLUDE_CATADDRESS

#include "CatShell/core/defines.h"

#include <vector>
#include <iostream>

namespace CatShell {

class CatAddress
        ///
        /// Defines the address of the data.
        ///
{
public:

        CatAddress();
                /// Constructor: default.

        virtual ~CatAddress();
                /// Destructor.

        void operator=(const std::string& text);
                /// Consttuct address from string.

        void print(std::ostream& output);
                /// Print the address.

        void begin();
                /// Go to the start of the address.

        CatAddress& operator++();
                /// Go to the next level.

        CatAddress& operator++(int i);
                /// Go to the next level.

        bool operator==(const std::string& name) const;
                /// Compare if this is the name of the current address level.

        bool has_index() const;
                /// Returns true if there is an index.

        std::int32_t get_index() const;
                /// Returns the index.
  
protected:

private: 

        typedef struct tAddr
        {
                tAddr(const std::string& s, std::int32_t i) {name=s; index=i;}
                std::string     name;
                std::int32_t   index;
        } tAddr;

        std::vector<tAddr>              _levels;
        std::vector<tAddr>::iterator    _it;
};

inline void CatAddress::begin() { _it = _levels.begin(); }
inline CatAddress& CatAddress::operator++() { ++_it; return *this; }
inline CatAddress& CatAddress::operator++(int i) { ++_it; return *this; }

} // namespace CatShell

#endif
