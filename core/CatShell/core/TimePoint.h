#ifndef INCLUDE_TIMEPOINT
#define INCLUDE_TIMEPOINT

#include <cstdint>
#include <functional>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <map>

#include "CatShell/core/defines.h"

namespace CatShell {

class TimePoint
        /// Represents timestamp.
{
public:
        TimePoint();
                /// Constructor: default.
               
        TimePoint(const TimePoint& other_time);
                /// Constructor: copy.
       
        ~TimePoint();
                /// Destructor.

        void get_now();
                /// Remembers current time.
     
     	operator bool() const;
     		/// Returns true if time != 0.   

        std::uint64_t get_seconds() const;
                /// Returns seconds.
       
        std::uint32_t get_micro_seconds() const;
                /// Returns nanoseconds.
               
        double get_time_double() const;
                /// Returns time in double precission.
               
        void set_time_double(double time);
                /// Sets the time.
               
        void set_value(std::uint64_t seconds, std::uint32_t microseconds);
                /// Sets a value ot the time.
               
        void set_value_as_UTC(std::uint32_t year, std::uint8_t month, std::uint8_t daym, std::uint8_t hour, std::uint8_t minute, std::uint8_t second, std::uint32_t microseconds);
                /// Sets a value ot the time.

        TimePoint& operator=(const TimePoint& other_time);
                /// Assigment operator.
               
        TimePoint operator-(const TimePoint& other_time) const;
                /// Time substraction.
               
        void operator-=(const TimePoint& other_time);
                /// Time substraction.
               
        void operator+=(const TimePoint& other_time);
                /// Time addition.
               
        bool operator==(const TimePoint& other_time) const;
                /// Comparison operator.
               
        bool operator>(const TimePoint& other_time) const;
                /// Comparison operator.

        bool operator>=(const TimePoint& other_time) const;
                /// Comparison operator.

        bool operator<(const TimePoint& other_time) const;
                /// Comparison operator.

        bool operator<=(const TimePoint& other_time) const;
                /// Comparison operator.
              
        void convert_to_UTC_asci(char* str) const;
                /// Fills the buffer with the asci representation of UTC timestamp it represents.
       
        void convert_to_local_asci(char* str) const;
                /// Fills the buffer with the asci representation of local timestamp it represents.    

        StreamSize get_stream_length() const;
        	/// Returns the length of data when streamed.

        void write_to_stream(std::ostream& output) const;
        	/// Writes to stream.

        void read_from_stream(std::istream& input);
        	/// Reads from stream.

private:
        std::uint64_t _seconds;
        std::uint32_t _micro_seconds;
};

inline std::uint64_t TimePoint::get_seconds() const { return _seconds; }
inline std::uint32_t TimePoint::get_micro_seconds() const { return _micro_seconds; }
inline void TimePoint::set_value(std::uint64_t seconds, std::uint32_t microseconds) { _seconds = seconds; _micro_seconds = microseconds; }
inline StreamSize TimePoint::get_stream_length() const { return sizeof(std::uint64_t) + sizeof(std::uint32_t); }
inline TimePoint::operator bool() const { return (_seconds != 0) || (_micro_seconds != 0); }

} // namespace CatShell

#endif

