#ifndef INCLUDE_CLASSMANAGER
#define INCLUDE_CLASSMANAGER

#include "CatShell/core/defines.h"
#include "CatShell/core/Setting.h"
#include "CatShell/core/CatPointer.h"

#include <mutex>
#include <stack>
#include <new>

namespace CatShell {

class ClassManagerBase
        /// Base class for all class managers.
{
        template <class T> friend class CatPointer;
        friend class CatObject;
        
public:
        ClassManagerBase() {};
                /// Constructor: default.

        virtual ~ClassManagerBase() {};
                /// Destructor.

        virtual bool is_derived(CatType type) const = 0;
                /// Returns 'true' if objects in this class manager are derived from class of type 'type'.

        virtual CatType memory_cat_type() const = 0;
                /// Return the type number which this manager serves.

        virtual const std::string& memory_cat_name() const = 0;
                /// Return the type name which this pool serves.
protected:
private:
};

template<class T>
class ClassManager : public ClassManagerBase
{
public:
        ClassManager();
                /// Constructor: default.

        virtual ~ClassManager();
                /// Destructor.

        virtual bool is_derived(CatType type) const;
                /// Returns 'true' if objects in this memory pool are derived from class of type 'type'.

        virtual CatType memory_cat_type() const;
                /// Return the type number which this pool serves.

        virtual const std::string& memory_cat_name() const;
                /// Return the type name which this pool serves.
};

template<class T>
bool ClassManager<T>::is_derived(CatType type) const
{
        return T::s_is_derived(type);
}

template<class T>
CatType ClassManager<T>::memory_cat_type() const
{
        return T::s_cat_type();
}

template<class T>
ClassManager<T>::ClassManager()
{
}

template<class T>
ClassManager<T>::~ClassManager()
{
}

template<class T>
const std::string& ClassManager<T>::memory_cat_name() const
{
        return T::s_cat_name();
}
} // namespace CatShell

#endif
