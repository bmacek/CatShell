#ifndef INCLUDE_CATPOISSON
#define INCLUDE_CATPOISSON

#include "CatShell/core/CatObject.h"
#include "CatShell/core/Algorithm.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/CatUint32.h"

namespace CatShell {

class CatPoisson : public Algorithm
{
        CAT_OBJECT_DECLARE(CatPoisson, Algorithm, CAT_NAME_CATPOISSON, CAT_TYPE_CATPOISSON);
public:

        CatPoisson();
                /// Constructor: default.

        virtual ~CatPoisson();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

        Algorithm::DataPort    _in_port_object;
	
        Algorithm::DataPort    _out_port_object;

	MemoryPool<CatUint32>* _pool;

	std::uint32_t _repetition;
};

inline StreamSize CatPoisson::cat_stream_get_size() const
{
        return Algorithm::cat_stream_get_size();
}

inline void CatPoisson::cat_stream_out(std::ostream& output)
{
        return Algorithm::cat_stream_out(output);
}

inline void CatPoisson::cat_stream_in(std::istream& input)
{
        return Algorithm::cat_stream_in(input);
}

inline void CatPoisson::cat_print(std::ostream& output, const std::string& padding)
{
        Algorithm::cat_print(output, padding);
}


} // namespace CatShell

#endif

