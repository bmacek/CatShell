#ifndef INCLUDE_CATSORT
#define INCLUDE_CATSORT

#include "CatShell/core/CatObject.h"
#include "CatShell/core/Algorithm.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/CatUint32.h"
#include "CatShell/core/CatBool.h"
#include "CatShell/core/CatDouble.h"
#include "CatShell/core/defines.h"

namespace CatShell {

class CatSort : public Algorithm
{
        CAT_OBJECT_DECLARE(CatSort, Algorithm, CAT_NAME_CATSORT, CAT_TYPE_CATSORT);
public:

        CatSort();
                /// Constructor: default.

        virtual ~CatSort();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

        Algorithm::DataPort    _in_port_object;

	Algorithm::DataPort    _out_port_default;

	CatType _type_in;
	uint32_t _counter;

	typedef struct {
		CatType _type;
		DataPort _out_port;
		bool _strict;
	} outPort;

	std::vector<outPort> _out_port_object; 
};

inline StreamSize CatSort::cat_stream_get_size() const
{
        return Algorithm::cat_stream_get_size();
}

inline void CatSort::cat_stream_out(std::ostream& output)
{
        return Algorithm::cat_stream_out(output);
}

inline void CatSort::cat_stream_in(std::istream& input)
{
        return Algorithm::cat_stream_in(input);
}

inline void CatSort::cat_print(std::ostream& output, const std::string& padding)
{
        Algorithm::cat_print(output, padding);
}

} // namespace CatShell

#endif

