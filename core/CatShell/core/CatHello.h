#ifndef INCLUDE_CATHELLO
#define INCLUDE_CATHELLO

#include "CatShell/core/CatObject.h"
#include "CatShell/core/Process.h"

namespace CatShell {

class CatHello : public Process
{
        CAT_OBJECT_DECLARE(CatHello, Process, CAT_NAME_CATHELLO, CAT_TYPE_CATHELLO);
public:

        CatHello();
                /// Constructor: default.

        virtual ~CatHello();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatPointer<WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------

protected:
private:
};

inline StreamSize CatHello::cat_stream_get_size() const
{
        return Process::cat_stream_get_size();
}

inline void CatHello::cat_stream_out(std::ostream& output)
{
        return Process::cat_stream_out(output);
}

inline void CatHello::cat_stream_in(std::istream& input)
{
        return Process::cat_stream_in(input);
}

inline void CatHello::cat_print(std::ostream& output, const std::string& padding)
{
        Process::cat_print(output, padding);
}

inline void CatHello::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
}

inline void CatHello::work_subthread_finished(CatPointer<WorkThread> child)
{
}


} // namespace CatShell

#endif

