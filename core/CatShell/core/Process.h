#ifndef INCLUDE_PROCESS
#define INCLUDE_PROCESS

#include "CatShell/core/WorkThread.h"
#include "CatShell/core/Algorithm.h"

namespace CatShell {

class Process : public WorkThread, public Algorithm
{
public:

        Process();
                /// Constructor: default.

        virtual ~Process();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger) = 0;
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatPointer<WorkThread> child) = 0;
                /// Called when a subprocess finished.

//----------------------------------------------------------------------

protected:

private:

};

inline StreamSize Process::cat_stream_get_size() const
{
        return 0;
}

inline void Process::cat_stream_out(std::ostream& output)
{
}

inline void Process::cat_stream_in(std::istream& input)
{
}

inline void Process::algorithm_init()
{
        Algorithm::algorithm_init();
}

inline void Process::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
}

inline void Process::cat_print(std::ostream& output, const std::string& padding)
{
        Algorithm::cat_print(output, padding);
}

inline void Process::cat_free() {Algorithm::cat_free();}

} // namespace CatShell

#endif
