#ifndef INCLUDE_PLUGIN
#define INCLUDE_PLUGIN

#include "CatShell/core/defines.h"
#include "CatShell/core/Setting.h"
#include "CatShell/core/Logger.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/ClassManager.h"

#include <map>
#include <set>

namespace CatShell {

class Plugin
{
public:

        Plugin();
                /// Constructor: default.

        virtual ~Plugin();
                /// Destructor.

        bool define_plugin(Setting* settings, Logger* logger, const std::string& name);
                /// Define basic plugin variables.

        void define_new_memory_pool(const std::string& name, MemoryPoolBase* pool, ClassManagerBase* manager);
                /// Registers the memory pool.
                /// NOTE: no duplication checking is done, since this should be checked 
                ///       manually by the programmer when using macros.

        MemoryPoolBase* get_memory_pool(const std::string& name);
                /// Returns the memory pool, or nullptr if such pool does not exist.

	ClassManagerBase* get_class_manager(const std::string& name);
		/// Returns the class manager, or nullptr if such manager does not exist

        bool fill_available_types(std::set<std::string>& types);
                /// Inserts names of all available types.

        Setting& settings();
                /// Returns the settings.

        Logger& logger();
                /// Returns the logger.

        bool enable();
                /// Enable plugin.

        bool disable();
                /// Disable plugin.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual bool init();
                /// Prepares everything before plugin can be used. 

        virtual bool deinit();
                /// Cleans everything before plugin can be unloaded. 
//----------------------------------------------------------------------

protected:


private:

        Setting*                                _plugin_settings;
        Logger*                                 _plugin_logger;
        std::string                             _plugin_name;

	typedef struct {
		MemoryPoolBase* pool;
		ClassManagerBase* manager;
	} Base;

        std::map<std::string, Base>  _pools;
};

inline Plugin::Plugin()
{
}

inline Plugin::~Plugin()
{
        //LOG((*_plugin_logger), "Cleaning up the memory.")
        for(auto x: _pools)
                delete x.second.pool;
}

inline Setting& Plugin::settings()
{
        return *_plugin_settings;
}

inline Logger& Plugin::logger()
{
      return *_plugin_logger;  
}

inline bool Plugin::define_plugin(Setting* settings, Logger* logger, const std::string& name)
{
        _plugin_settings = settings;
        _plugin_name = name;
        _plugin_logger = logger->get_sublogger("PL_" + name);

        if(!_plugin_settings->has_element("classes"))
                _plugin_settings->add_element(new Setting("classes"));

        return true;
}

inline void Plugin::define_new_memory_pool(const std::string& name, MemoryPoolBase* pool, ClassManagerBase* manager)
{
	Base base;
	base.manager = manager;
	base.pool = pool;
        _pools.insert(std::pair<std::string, Base>(name, base));
        
        LOG((*_plugin_logger), "Object type '" << name << "' registered.")
}

inline MemoryPoolBase* Plugin::get_memory_pool(const std::string& name)
{
        auto it = _pools.find(name);
        if(it == _pools.end())
                return nullptr;
        else
                return it->second.pool;
}

inline ClassManagerBase* Plugin::get_class_manager(const std::string& name)
{
        auto it = _pools.find(name);
        if(it == _pools.end())
                return nullptr;
        else
                return it->second.manager;
}

inline bool Plugin::fill_available_types(std::set<std::string>& types)
{
        for(auto x: _pools)
                types.insert(x.first);

        return true;
}

inline bool Plugin::init()
{
        LOG((*_plugin_logger), "Initializing plugin.")
        return true;
}

inline bool Plugin::deinit()
{
        LOG((*_plugin_logger), "Uninitializing plugin.")
        return true;
}

inline bool Plugin::enable()
{
        LOG((*_plugin_logger), "Enabling the plugin.")        
        for(auto x: _pools)
		if(x.second.pool)
                	x.second.pool->enable();
        return true;
}

inline bool Plugin::disable()
{
        LOG((*_plugin_logger), "Disabling the plugin.")        
        for(auto x: _pools)
		if(x.second.pool)
			x.second.pool->disable();
        return true;
}

//
// Macros for declaring the plug-in
//
#define EXPORT_PLUGIN(CLASS, NAME) \
        bool global_memory_initialization(); \
        CLASS NAME; \
        extern "C" bool global_plugin_config(CatShell::Setting* settings, CatShell::Logger* logger, const std::string& name) \
        { \
                return NAME.define_plugin(settings, logger, name); \
        } \
        extern "C" bool global_plugin_init() \
        { \
                global_memory_initialization(); \
                return NAME.init(); \
        } \
        extern "C" bool global_plugin_deinit() \
        { \
                return NAME.deinit(); \
        } \
        extern "C" bool global_plugin_get_data_types(std::set<std::string>& typeList) \
        { \
                return NAME.fill_available_types(typeList); \
        } \
        extern "C" CatShell::MemoryPoolBase* global_plugin_get_memory_pool(const std::string& type) \
        { \
                return NAME.get_memory_pool(type); \
        } \
        extern "C" CatShell::ClassManagerBase* global_plugin_get_class_manager(const std::string& type) \
        { \
                return NAME.get_class_manager(type); \
        } \
        extern "C" bool global_plugin_enable() \
        { \
                return NAME.enable(); \
        } \
        extern "C" bool global_plugin_disable() \
        { \
                return NAME.disable(); \
        }

#define START_EXPORT_CLASSES(NAME) \
        bool global_memory_initialization() \
        {
               
#define EXPORT_CLASS(CLASS, CLASS_NAME, NAME) \
                if(!NAME.settings()["classes"].has_element(CLASS_NAME)) \
                        NAME.settings()["classes"].add_element(new CatShell::Setting(CLASS_NAME)); \
                NAME.define_new_memory_pool(CLASS_NAME, new CatShell::MemoryPool<CLASS>((CatShell::Setting*)&(NAME.settings()["classes"][CLASS_NAME])), new CatShell::ClassManager<CLASS>() );

#define EXPORT_VIRTUAL_CLASS(CLASS, CLASS_NAME, NAME) \
                if(!NAME.settings()["classes"].has_element(CLASS_NAME)) \
                        NAME.settings()["classes"].add_element(new CatShell::Setting(CLASS_NAME)); \
                NAME.define_new_memory_pool(CLASS_NAME, nullptr, new CatShell::ClassManager<CLASS>() );
               
#define END_EXPORT_CLASSES(NAME) \
                return true; \
        }

} // namespace CatShell

#endif
