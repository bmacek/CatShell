#ifndef INCLUDE_CATFILE_INCLUDED
#define INCLUDE_CATFILE_INCLUDED

#include <fstream>
#include <iostream>

#include "CatShell/core/CatObject.h"

namespace CatShell {

class CatFile
///
/// General purpose functions.
///
{
public:

        CatFile();
                /// Constructor: default.

        virtual ~CatFile();
                /// Destructor.

        bool open_file(const std::string& name, bool read, bool write);
                /// Opens a file.

        void close_file();
                /// Closes the file.

        CatPointer<CatObject> get_next_object() const;
                /// Returns the next object in the file.

        bool eof() const;
                /// Returns 'true' if end of file has been reached.


protected:
private:

        std::fstream*   _file;
        mutable bool    _eof;
};

inline bool CatFile::eof() const { return _eof; }

} // namespace CatShell

#endif

