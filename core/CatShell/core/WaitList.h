#ifndef INCLUDE_WAITLIST
#define INCLUDE_WAITLIST

#include <mutex>
#include <thread>
#include <condition_variable>
#include <queue>

namespace CatShell {

template<class T>
class WaitList
        ///
        /// Queue for interfacing multiple threads.
        ///
{
public:
        WaitList(bool on = true);
                /// Constructor: default.

        virtual ~WaitList();
                /// Destructor.

        bool insert(T* object);
                /// Inserts the object in the queue.
                /// Returns 'true' if object has been inserted.

        void turn_on();
                /// Enables the list to accept new elements.

        void turn_off(bool wake = false);
                /// Disables the list to accept new elements.
                /// If 'wake' is 'true' than all the threads that are waiting for the elements
                /// are waken up.

        T* wait();
                /// Non-blocking wait. If no object has been found it returns 'nullptr'.

        bool wait(T*& object);
                /// Wait for the objects. If object arrives, the pointer reference is updated with
                /// the object pointer. Returns 'true'.
                /// 'false' is returned only if the list has been turned off in which case the pointer
                /// reference is not modified.

        bool wait(T*& object, std::uint32_t timeout_ms);
                /// Wait for the objects. If object arrives, the pointer reference is updated with
                /// the object pointer. Returns 'true'.
                /// 'false' is returned if:
                /// 1) the list has been turned off in which case the pointer reference is not modified.
                /// 2) timeout has been reached.

protected:
private:

        std::queue<T*>          _data;
        std::mutex              _data_lock;
        std::condition_variable _data_cv;
        bool                    _data_stop;
};

template<class T>
WaitList<T>::WaitList(bool on)
: _data_stop(!on)
{
}

template<class T>
WaitList<T>::~WaitList()
{
        std::unique_lock<std::mutex> lock(_data_lock);
        _data_stop = true;


        while(_data.size())
        {
                delete _data.front();
                _data.pop();                
        }
}

template<class T>
bool WaitList<T>::insert(T* object)
{
        std::unique_lock<std::mutex> lock(_data_lock);
        if(_data_stop)
                return false;
        _data.push(object);
        _data_cv.notify_one();
        return true;
}

template<class T>
void WaitList<T>::turn_on()
{
        std::unique_lock<std::mutex> lock(_data_lock);
        _data_stop = false;
}

template<class T>
void WaitList<T>::turn_off(bool wake)
{
        std::unique_lock<std::mutex> lock(_data_lock);
        _data_stop = true;

        if(wake)
                _data_cv.notify_all();
}

template<class T>
T* WaitList<T>::wait()
{
        std::unique_lock<std::mutex> lock(_data_lock);        
        if(_data.size() == 0 || _data_stop == true)
                return nullptr;

        T* tmp = _data.front();
        _data.pop();
        return tmp;
}


template<class T>
bool WaitList<T>::wait(T*& object)
{
        std::unique_lock<std::mutex> lock(_data_lock);

        // if something there already
        if(_data.size())
        {
                // get the first element
                object = _data.front();
                _data.pop();

                return true;
        }

        //wait for some data to appear
        while(_data.size() == 0 && _data_stop == false) _data_cv.wait(lock);

        // need to end?
        if(_data_stop)
                return false;

        // get the first element
        object = _data.front();
        _data.pop();

        return true;
}

template<class T>
bool WaitList<T>::wait(T*& object, std::uint32_t timeout_ms)
{
        std::unique_lock<std::mutex> lock(_data_lock);

        // if something there already
        if(_data.size())
        {
                // get the first element
                object = _data.front();
                _data.pop();

                return true;
        }

        //wait for some data to appear or for the timeout
        std::cv_status st = _data_cv.wait_for(lock, std::chrono::milliseconds(timeout_ms));
        if (st == std::cv_status::timeout || _data.size() == 0 || _data_stop == true)
                return false;

        // get the first element
        object = _data.front();
        _data.pop();

        return true;
}

} // namespace CatShell

#endif
