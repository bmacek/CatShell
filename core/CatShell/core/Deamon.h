#ifndef INCLUDE_DEAMON
#define INCLUDE_DEAMON

#include "CatShell/core/Setting.h"
#include "CatShell/core/Logger.h"
#include "CatShell/core/WorkThread.h"

namespace CatShell {

class Deamon : protected WorkThread
        /// Base class for all apps that run as deamon
        /// processes. It has build in logging, settings,
        /// and shutdown procedures.
       
        /// The settings file can be specified with parameter -s <file>
        /// If not, it searches for the <programName>.xml. If nothing is found
        /// error occures.
        ///
        /// Setting file, shoud be a well formatted XML file with <settings> root tag,
        /// and the program checks for the settings under <settings>/<programName> tag.
        ///
        /// Minimal settings for a file should be the logging settings, since the logging
        /// infrastructure is automatically setup.
{
CAT_OBJECT_DECLARE(Deamon, WorkThread, "CatDeamon", CAT_TYPE_DEAMON)
public:

        virtual ~Deamon();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main() = 0;
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init() = 0;
                /// Called before the thread starts executing.

        virtual void work_deinit() = 0;
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatPointer<WorkThread> child) = 0;
                /// Called when a subprocess finished.
//----------------------------------------------------------------------

        static void sysShutdown(int signum);
                /// Callback function for system shutdown message.

        int run(int argc, char** argv, std::string programName, std::string programVersion);
                /// DO NOT CALL THIS FUNCTION! USE MACRO INSTEAD!
                /// Starts the program.

protected:

        Deamon();
                /// Constructor: default.
        
        bool has_user_flag(std::string flag);
                /// Check if the flag exist.

        std::string get_user_parameter(std::string key);
                /// Returns the parameter or raises SettingException if it
                /// does not exist.

private:

        void set_user_parameter(std::string key, std::string value);
                /// Set the parameter.

        void set_user_flag(std::string flag);
                /// Set the flag.


private:

        std::map<std::string, std::string>      _params;
        std::vector<std::string>                _flags;

        static Deamon* _stat_app;
};

inline StreamSize Deamon::cat_stream_get_size() const
{
        return 0;
}

inline void Deamon::cat_stream_out(std::ostream& output)
{
}

inline void Deamon::cat_stream_in(std::istream& input)
{
}

inline void Deamon::cat_print(std::ostream& output, const std::string& padding)
{
        WorkThread::cat_print(output, padding);
}

inline void Deamon::cat_free() {WorkThread::cat_free();}

// macro to implement main
#define CAT_DEAMON_APP_MAIN(APP_CLASS, APP_NAME, APP_VERSION)       \
int main(int argc, char** argv)                                 \
{                                                               \
        APP_CLASS app;                                          \
        return app.run(argc, argv, APP_NAME, APP_VERSION);      \
}        

} // namespace CatShell

#endif
