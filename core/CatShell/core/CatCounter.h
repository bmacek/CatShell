#ifndef INCLUDE_CATCOUNTER
#define INCLUDE_CATCOUNTER

#include "CatShell/core/CatObject.h"
#include "CatShell/core/Algorithm.h"
#include "CatShell/core/CatUint32.h"

namespace CatShell {

class CatCounter : public Algorithm
{
        CAT_OBJECT_DECLARE(CatCounter, Algorithm, CAT_NAME_CATCOUNTER, CAT_TYPE_CATCOUNTER);
public:

        CatCounter();
                /// Constructor: default.

        virtual ~CatCounter();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.
//----------------------------------------------------------------------

protected:
private:

private:

  Algorithm::DataPort    _in_port_object;
  Algorithm::DataPort    _out_port_count;

  CatShell::CatPointer<CatUint32> _count;
};

inline StreamSize CatCounter::cat_stream_get_size() const
{
        return Algorithm::cat_stream_get_size();
}

inline void CatCounter::cat_stream_out(std::ostream& output)
{
        return Algorithm::cat_stream_out(output);
}

inline void CatCounter::cat_stream_in(std::istream& input)
{
        return Algorithm::cat_stream_in(input);
}

inline void CatCounter::cat_print(std::ostream& output, const std::string& padding)
{
        Algorithm::cat_print(output, padding);
}

inline void CatCounter::cat_free()
{
  _count.make_nullptr();
}

} // namespace CatShell

#endif
