#ifndef INCLUDE_CATGENERATOR
#define INCLUDE_CATGENERATOR

#include "CatShell/core/CatObject.h"
#include "CatShell/core/defines.h"
#include "CatShell/core/Process.h"
#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/CatDouble.h"

#include <stack>
#include <fstream>
#include <iostream>

namespace CatShell {

class CatGenerator : public Process
{
        CAT_OBJECT_DECLARE(CatGenerator, Process, CAT_NAME_CATGENERATOR, CAT_TYPE_CATGENERATOR);
public:

        CatGenerator();
                /// Constructor: default.

        virtual ~CatGenerator();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

        virtual CatShell::CatPointer<CatShell::CatObject> produce_data(DataPort port, bool& flush, CatShell::Logger& logger);
                /// Produces data on request. Secont parameter that is returned if a flush flag.
                /// Object is taken form the first one that producess a result.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatPointer<WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------

protected:
private:
        typedef struct
        {
                bool is_value;
                union{
                        double value;
                        void* unit;
                };
        } tPattElement;
        typedef struct
        {
                std::uint32_t repetition;
                std::vector<tPattElement> units;
        } tPattUnit;


        typedef struct
        {
                tPattUnit* unit;
                std::vector<tPattElement>::iterator element;
                std::uint32_t count;
        } tStackEntry;

        void load_pattern(tPattUnit* unit, CatShell::Setting& s);
                /// Creates a sequence tree from the settings.

        void free_pattern(tPattUnit* unit);
                /// Free the internal memory structures.

        bool next_step();
                /// Publishes the next step and returns if process should continue.

private:

        Algorithm::DataPort    _in_port_trigger;
        Algorithm::DataPort    _out_port_object;

        CatShell::MemoryPool<CatShell::CatDouble>* _pool;

        tPattUnit _pattern;
        std::stack<tStackEntry> _stack;
};

inline StreamSize CatGenerator::cat_stream_get_size() const { return Process::cat_stream_get_size(); }
inline void CatGenerator::cat_stream_out(std::ostream& output) { return Process::cat_stream_out(output); }
inline void CatGenerator::cat_stream_in(std::istream& input) { return Process::cat_stream_in(input); }
inline void CatGenerator::cat_print(std::ostream& output, const std::string& padding) { Process::cat_print(output, padding); }
inline void CatGenerator::algorithm_deinit() { free_pattern(&_pattern);  Process::algorithm_deinit(); }
inline void CatGenerator::work_init() { Process::work_init(); }
inline void CatGenerator::work_deinit() { free_pattern(&_pattern);  Process::work_deinit(); }
inline void CatGenerator::work_subthread_finished(CatPointer<WorkThread> child) { }


} // namespace CatShell

#endif

