#ifndef INCLUDE_MEMORYPOOL
#define INCLUDE_MEMORYPOOL

#include "CatShell/core/defines.h"
#include "CatShell/core/Setting.h"
#include "CatShell/core/CatPointer.h"

#include <mutex>
#include <stack>
#include <new>

namespace CatShell {

class MemoryPoolBase
        /// Base class for all memory pools.
{
        template <class T> friend class CatPointer;
        friend class CatObject;
        
public:
        MemoryPoolBase() {};
                /// Constructor: default.

        virtual ~MemoryPoolBase() {};
                /// Destructor.

        virtual void enable() = 0;
                /// Locks the memory pool, which prevents from further object from being initialized.

        virtual void disable() = 0;
                /// Unlocks the memory pool, enabling object initialization.

        virtual std::uint32_t get_elements_now() const = 0;
                /// Returns the number of currently allocated elements.

        virtual CatPointer<CatObject> get_base_element() = 0;
                /// Returns the pointer to the element.
                /// If memory is unavailable throws: std::bad_alloc.

        virtual CatType memory_cat_type() const = 0;
                /// Return the type number which this pool serves.

        virtual const std::string& memory_cat_name() const = 0;
                /// Return the type name which this pool serves.

        virtual void init() = 0;
                /// Initialize the memory pool.

        virtual void drop_unused() = 0;
                /// Releases all unused elements.

private: 
        virtual void return_element(void* obj) = 0;
                /// Returns the element to the pool.

protected:
private:
};

template<class T>
class MemoryPool : public MemoryPoolBase
        ///
        /// Simple memory management.
        ///
        /// Settings:
        /// <memory_pool>
        ///    <max_elements type='std::uint32_t'>10</max_elements>
        ///    <resize type='std::uint32_t'>0</resize>
        /// </memory_pool>
{
        friend class CatPointer<T>;
public:
        MemoryPool(Setting* settings);
                /// Constructor: default.

        virtual ~MemoryPool();
                /// Destructor.

        virtual CatPointer<CatObject> get_base_element();
                /// Returns the pointer to the element.
                /// If memory is unavailable throws: std::bad_alloc.

        CatPointer<T> get_element();
                /// returns the element of this pool.

        void enable();
                /// Locks the memory pool, which prevents from further object from being initialized.

        void disable();
                /// Unlocks the memory pool, enabling object initialization.

        std::uint32_t get_elements_now() const;
                /// Returns the number of currently allocated elements.

        std::uint32_t get_elements_free() const;
                /// Returns number of free elements.

        virtual CatType memory_cat_type() const;
                /// Return the type number which this pool serves.

        virtual const std::string& memory_cat_name() const;
                /// Return the type name which this pool serves.

        virtual void init();
                /// Initialize the memory pool.

        void set_init_deinit(std::function<bool (T*)> init, std::function<void (T*)> deinit);
                /// Sets the init and deinit functions of the objects.

        virtual void drop_unused();
                /// Releases all unused elements.

protected:

        bool sett_change_max(Setting* setting);
                /// Settings change.

        bool sett_change_resize(Setting* setting);
                /// Settings change.

private:

        virtual void return_element(void* obj);
                /// Returns the element to the pool.

private:

        Setting*   _settings;

        std::uint32_t   _cur_count; // total count of all the object in this pool
        std::uint32_t   _max_count; // maximal number of allowed elements

        std::stack<T*>   _storage;

        std::mutex      _lock;

        std::atomic<bool>       _enabled;

        std::function<bool (T*)> _init;
        std::function<void (T*)> _deinit;
};

template<class T>
void MemoryPool<T>::set_init_deinit(std::function<bool (T*)> init, std::function<void (T*)> deinit)
{
        _init = init;
        _deinit = deinit;
}

template<class T>
MemoryPool<T>::MemoryPool(Setting* settings)
: _settings(settings), _cur_count(0), _enabled(true), _init(nullptr), _deinit(nullptr)
{
        // check if there are settings available
        if(!_settings->has_element("max_elements"))
                _settings->add_element(new ValueSetting<std::uint32_t>("max_elements", DEFAULT_MEMORY_POOL_MAX_SIZE));
        if(!_settings->has_element("resize"))
                _settings->add_element(new ValueSetting<std::uint32_t>("resize", 0));

        _max_count = ((ValueSetting<std::uint32_t>&)(*_settings)["max_elements"]).value();

        // register callbacks
        (*_settings)["max_elements"].register_call_back(std::bind(&MemoryPool<T>::sett_change_max, this, std::placeholders::_1));
        (*_settings)["resize"].register_call_back(std::bind(&MemoryPool<T>::sett_change_resize, this, std::placeholders::_1));
}

template<class T>
MemoryPool<T>::~MemoryPool()
{
        std::unique_lock<std::mutex> lock(_lock);

        // delete all instances
        while(_storage.size())
        {
                if(_deinit)
                        _deinit(_storage.top());
                delete _storage.top();
                _storage.pop();
        }
}

template<class T>
void MemoryPool<T>::drop_unused()
{
        std::unique_lock<std::mutex> lock(_lock);
 
        // delete all instances
        while(_storage.size())
        {
                if(_deinit)
                        _deinit(_storage.top());
                delete _storage.top();
                _storage.pop();
                _cur_count--;
        }        
}

template<class T>
CatType MemoryPool<T>::memory_cat_type() const
{
        return T::s_cat_type();
}

template<class T>
const std::string& MemoryPool<T>::memory_cat_name() const
{
        return T::s_cat_name();
}

template<class T>
inline std::uint32_t MemoryPool<T>::get_elements_free() const
{
        return _storage.size();
}

template<class T>
inline std::uint32_t MemoryPool<T>::get_elements_now() const
{
        return _cur_count;
}

template<class T>
inline void MemoryPool<T>::enable()
{
        _enabled = true;
}

template<class T>
inline void MemoryPool<T>::disable()
{
        _enabled = false;
}

template<class T>
CatPointer<CatObject> MemoryPool<T>::get_base_element()
{
        std::unique_lock<std::mutex> lock(_lock);

        if(_enabled)
        {
                // from reserve?
                if(_storage.size())
                {
                        CatPointer<CatObject> obj;
                        obj = _storage.top();
                        _storage.pop();
                        obj->reference_plus();
                       
                        return obj;
                }
                else
                {
                        // check if still available
                        if(_cur_count >= _max_count)
                                throw std::bad_alloc();

                        T* tmp = new T;
                        if(_init)
                                if(!_init(tmp))
                                {
                                        // failed to initilaize
                                        delete tmp;
                                        throw std::bad_alloc();
                                }

                        CatPointer<CatObject> obj(tmp);
                        //obj->reference_plus();
                        _cur_count++;

                        return obj;
                }                        
        }
        else
                throw std::bad_alloc();
}

template<class T>
CatPointer<T> MemoryPool<T>::get_element()
{
        std::unique_lock<std::mutex> lock(_lock);

        if(_enabled)
        {
                // from reserve?
                if(_storage.size())
                {
                        CatPointer<T> obj;
                        obj = _storage.top();
                        _storage.pop();
                        obj->reference_plus();
                       
                        return obj;
                }
                else
                {
                        // check if still available
                        if(_cur_count >= _max_count)
                                throw std::bad_alloc();

 
                        T* tmp = new T;
                        if(_init)
                                if(!_init(tmp))
                                {
                                        // failed to initilaize
                                        delete tmp;
                                        throw std::bad_alloc();
                                }

                        CatPointer<T> obj(tmp);
                        //obj->reference_plus();
                        _cur_count++;

                        return obj;
                }                        
        }
        else
                throw std::bad_alloc();

}

template<class T>
void MemoryPool<T>::return_element(void* obj)
{
        std::unique_lock<std::mutex> lock(_lock);

        T* tmp = static_cast<T*>(obj);
        tmp->cat_free();
        if(_cur_count > _max_count)
        {
                if(_deinit)
                        _deinit(tmp);
                delete tmp;
                _cur_count--;
        }
        else
                _storage.push(tmp);
}

template<class T>
bool MemoryPool<T>::sett_change_max(Setting* setting)
{
        std::unique_lock<std::mutex> lock(_lock);

        _max_count = ((ValueSetting<std::uint32_t>*)setting)->value();

        // should reduce the number of elements ?
        if(_cur_count > _max_count)
        {
                while(_cur_count > _max_count && _storage.size() > 0)
                {
                        if(_deinit)
                                _deinit(_storage.top());
                        delete _storage.top();
                        _storage.pop();
                        _cur_count--;
                }
        }

        return true;
}

template<class T>
bool MemoryPool<T>::sett_change_resize(Setting* setting)
{
        std::unique_lock<std::mutex> lock(_lock);

        std::uint32_t new_size = ((ValueSetting<std::uint32_t>*)setting)->value();

        // should reduce the number of elements ?
        while(_cur_count > new_size && _storage.size() > 0)
        {
                if(_deinit)
                        _deinit(_storage.top());
                delete _storage.top();
                _storage.pop();
                _cur_count--;
        }

        return true;
}

template<class T>
void MemoryPool<T>::init()
{
        T::s_init_memory_pool(this);
}

} // namespace CatShell

#endif
