#ifndef INCLUDE_CATUINT32
#define INCLUDE_CATUINT32

#include "CatShell/core/defines.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include <iostream>
#include <vector>

namespace CatShell {

class CatUint32 : public CatObject
        ///
        /// Unsigned integer for CatShell.
        ///
{
        CAT_OBJECT_DECLARE(CatUint32, CatObject, CAT_NAME_CATUINT32, CAT_TYPE_CATUINT32);

public:
        CatUint32();
                /// Constructor: default.

        virtual ~CatUint32();
                /// Destructor.

        void operator=(const std::uint32_t rhs);
                /// Assigment.

        std::uint32_t value() const;
                /// Returns the value.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.

        virtual void* cat_get_part(CatAddress& addr);
                /// Returns the pointer to the desired part.
//----------------------------------------------------------------------

        CatUint32& operator++();
        CatUint32& operator--();
                /// Prefix operators.

protected:

private:

        std::uint32_t _data;
};

inline CatUint32::CatUint32() { }
inline CatUint32::~CatUint32() { }
inline StreamSize CatUint32::cat_stream_get_size() const { return sizeof(std::uint32_t); }
inline void CatUint32::cat_stream_out(std::ostream& output) { output.write((char*)&_data, sizeof(std::uint32_t)); }
inline void CatUint32::cat_stream_in(std::istream& input) { input.read((char*)&_data, sizeof(std::uint32_t)); }
inline void CatUint32::cat_print(std::ostream& output, const std::string& padding) { CatObject::cat_print(output, padding); output << _data; }
inline void CatUint32::cat_free() { }
inline void CatUint32::operator=(const std::uint32_t rhs) { _data = rhs; }
inline std::uint32_t CatUint32::value() const { return _data; }

} // namespace CatShell

#endif
