#ifndef INCLUDE_WORKTHREAD
#define INCLUDE_WORKTHREAD

#include "CatShell/core/Setting.h"
#include "CatShell/core/Logger.h"
#include "CatShell/core/WaitList.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include <thread>

namespace CatShell {

class WorkThread : public virtual CatObject
{
        friend class Deamon;
public:

        typedef struct
        {
                std::uint32_t   pid;
                std::string     message;
        } message;

        WorkThread();
                /// Constructor: default.

        virtual ~WorkThread();
                /// Destructor.

        void run_subthread(CatPointer<WorkThread> th, const std::string& name, Setting* settings);
                /// Runs a new thread as a child of this thread.

        void shutdown();
                /// Requests the shutdown of the thread.

        void die_with_no_children();
                /// Threads finishes if all the children have ended.

        bool should_continue(bool blocking, std::uint32_t timeout_ms = 0);
                /// User work function should be calling this function 
                /// on regular basis. If 'true' is returned the process
                /// should finish.
                /// 1) if 'blocking' is false, the function will return 
                /// 2) if 'blocking' is true, it looks at hte 'timeout_ms' parameter
                ///    a) =0 the function will sleep until someone requests the shutdown.
                ///    b) >0 the function will sleep for at least given number of milliseconds or arbitrary more

        const std::string& name();
                /// Returns the name of the thread.

        std::mutex& mutex();
                /// Returns the reference to the mutex of the thread.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const = 0;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output) = 0;
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input) = 0;
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main() = 0;
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init() = 0;
                /// Called before the thread starts executing.

        virtual void work_deinit() = 0;
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatPointer<WorkThread> child) = 0;
                /// Called when a subprocess finished
                /// The thread mutex is locked during the execution of this
                /// thread, so should not be locked again.
//----------------------------------------------------------------------

protected:

        Logger& logger();
                /// Returns the logger of the thread.

        Setting& settings();
                /// Returns settings.

        void self_shutdown();
                /// Requests the shutdown of the thread.

private:


        void run();
                /// Starts the thread execution. Does nothing if the thread is already executing.

        void wait_end();
                /// Wait for the end of thread execution. 

        void work_init_private(std::uint32_t pid, std::function<bool (message* object)> parenth, Logger& parenth_logger, const std::string& name, Setting* settings);
                /// Initializes the thread.

        void main_private();
                /// Start function of the thread.

        void process_message(message* msg);
                /// Processes given message.

private:

        std::function<bool (message* object)>     _parenth;
        std::string     _name;
        std::uint32_t   _pid;

        Logger*         _logger;
        bool            _logger_own;

        Setting*        _settings;

        bool            _waiting_end;
        bool            _end_with_children;

        std::thread*    _thread;
        std::mutex      _thread_mutex;  

        WaitList<message>       _messages;

        std::map<std::uint32_t, CatPointer<WorkThread>>    _children;
};

inline void WorkThread::die_with_no_children()
{
        _end_with_children = true;     
}

inline std::mutex& WorkThread::mutex()
{
        return _thread_mutex;
}

inline Logger& WorkThread::logger()
{
        return *_logger;
}

inline const std::string& WorkThread::name()
{
        return _name;
}

inline Setting& WorkThread::settings()
{
        return *_settings;
}

inline void WorkThread::wait_end()
{
        if(_thread)
        {
                _thread->join();
                delete _thread;
                _thread = nullptr;
        }
}

inline void WorkThread::cat_print(std::ostream& output, const std::string& padding)
{
        CatObject::cat_print(output, padding);
}

inline void WorkThread::cat_free() {}

} // namespace CatShell

#endif
