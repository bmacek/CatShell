#include "CatShell/core/CatCondition.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatCondition, CAT_NAME_CATCONDITION, CAT_TYPE_CATCONDITION);


CatCondition::CatCondition()
{

}

CatCondition::~CatCondition()
{

}


bool CatCondition::test(CatPointer<CatObject>& object, tTest& condition, CatShell::Logger& logger)
{
  if(condition.a_type && condition.b_type)
  {
    // A element, B element
    if(condition.type.substr(0,13) == "std::uint16_t")
      return test_cc<std::uint16_t>(object, condition.test, condition.a_element, condition.b_element);
    else if(condition.type.substr(0,13) == "std::uint32_t")
      return test_cc<std::uint32_t>(object, condition.test, condition.a_element, condition.b_element);
    else if(condition.type.substr(0,11) == "std::string")
      return test_cc<std::string>(object, condition.test, condition.a_element, condition.b_element);
    else
      return false;
  }
  else if(!condition.a_type && condition.b_type)
  {
    // A value, B element
    if(condition.type.substr(0,13) == "std::uint16_t")
      return test_vc<std::uint16_t>(object, condition.test, condition.a_value, condition.b_element);
    else if(condition.type.substr(0,13) == "std::uint32_t")
      return test_vc<std::uint32_t>(object, condition.test, condition.a_value, condition.b_element);
    else if(condition.type.substr(0,11) == "std::string")
      return test_vc<std::string>(object, condition.test, condition.a_value, condition.b_element);
    else
      return false;
  }
  else if(condition.a_type && !condition.b_type)
  {
    // A element, B value
    if(condition.type.substr(0,13) == "std::uint16_t")
      return test_cv<std::uint16_t>(object, condition.test, condition.a_element, condition.b_value);
    else if(condition.type.substr(0,13) == "std::uint32_t")
      return test_cv<std::uint32_t>(object, condition.test, condition.a_element, condition.b_value);
    else if(condition.type.substr(0,11) == "std::string")
      return test_cv<std::string>(object, condition.test, condition.a_element, condition.b_value);
    else
      return false;
  }
  else
  {
    // A value, B value
    if(condition.type.substr(0,13) == "std::uint16_t")
      return test_vv<std::uint16_t>(condition.test, (std::uint16_t*)condition.a_value, (std::uint16_t*)condition.b_value);
    else if(condition.type.substr(0,13) == "std::uint32_t")
      return test_vv<std::uint32_t>(condition.test, (std::uint32_t*)condition.a_value, (std::uint32_t*)condition.b_value);
    else if(condition.type.substr(0,11) == "std::string")
      return test_vv<std::string>(condition.test, (std::string*)condition.a_value, (std::string*)condition.b_value);
    else
      return false;
  }
}

void CatCondition::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
  if(port == _in_port_object && object.isSomething())
  {
    if(test(object, _test, logger))
    {
      publish_data(_out_port_yes, object, logger);
    }
    else
    {
      publish_data(_out_port_no, object, logger);
    }
  }

  Algorithm::process_data(port, object, flush, logger);
}

void CatCondition::delete_condition(tTest* condition)
{
  if(!condition->a_type)
  {
    if(condition->type.substr(0,13) == "std::uint16_t")
    {
      delete ((std::uint16_t*)condition->a_value);
    }
    else if(condition->type.substr(0,13) == "std::uint32_t")
    {
      delete ((std::uint32_t*)condition->a_value);
    }
    else if(condition->type.substr(0,11) == "std::string")
    {
      delete ((std::string*)condition->a_value);
    }
  }
  if(!condition->b_type)
  {
    if(condition->type.substr(0,13) == "std::uint16_t")
    {
      delete ((std::uint16_t*)condition->b_value);
    }
    else if(condition->type.substr(0,13) == "std::uint32_t")
    {
      delete ((std::uint32_t*)condition->b_value);
    }
    else if(condition->type.substr(0,11) == "std::string")
    {
      delete ((std::string*)condition->b_value);
    }
  }
}

void CatCondition::read_condition(tTest* condition, Setting& setting)
{
  condition->test = setting.get_setting<std::string>("test", "");
  condition->type = setting.get_setting<std::string>("type", "");

  cout << condition->test << endl;
  cout << condition->type << endl;

  if(setting["A"].has_element("name"))
  {
    // A is element
    condition->a_element.address = setting.get_setting<std::string>("A/name", "");
    condition->a_element.def = setting.get_setting<std::string>("A/default", "");
    condition->a_element.type = setting.get_setting<std::string>("A/type", "");
    condition->a_type = true;
  }
  else
  {
    // A is value
    if(condition->type.substr(0,13) == "std::uint16_t")
    {
      condition->a_value = new std::uint16_t;
      *((std::uint16_t*)condition->a_value) = setting.get_setting<std::uint16_t>("A/value", 0);
    }
    else if(condition->type.substr(0,13) == "std::uint32_t")
    {
      condition->a_value = new std::uint32_t;
      *((std::uint32_t*)condition->a_value) = setting.get_setting<std::uint32_t>("A/value", 0);
    }
    else if(condition->type.substr(0,11) == "std::string")
    {
      condition->a_value = new std::string;
      *((std::string*)condition->a_value) = setting.get_setting<std::string>("A/value", 0);
    }
    else
    {
      //ERROR(logger, "Could not read conditions A for CatCondition.");
    }
    condition->a_type = false;
  }

  if(setting["B"].has_element("name"))
  {
    // B is element
    condition->b_element.address = setting.get_setting<std::string>("B/name", "");
    condition->b_element.def = setting.get_setting<std::string>("B/default", "");
    condition->b_element.type = setting.get_setting<std::string>("B/type", "");
    condition->b_type = true;
  }
  else
  {
    // B is value
    if(condition->type.substr(0,13) == "std::uint16_t")
    {
      condition->b_value = new std::uint16_t;
      (*((std::uint16_t*)condition->b_value)) = setting.get_setting<std::uint16_t>("B/value", 0);
    }
    else if(condition->type.substr(0,13) == "std::uint32_t")
    {
      condition->b_value = new std::uint32_t;
      (*((std::uint32_t*)condition->b_value)) = setting.get_setting<std::uint32_t>("B/value", 0);
    }
    else if(condition->type.substr(0,11) == "std::string")
    {
      condition->b_value = new std::string;
      (*((std::string*)condition->b_value)) = setting.get_setting<std::string>("B/value", "");
    }
    else
    {
      //ERROR(logger, "Could not read conditions B for CatCondition.");
    }
    condition->b_type = false;
  }
}

void CatCondition::algorithm_init()
{
        Algorithm::algorithm_init();

        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);
        _out_port_yes = declare_output_port("out_yes", CAT_NAME_OBJECT);
        _out_port_no = declare_output_port("out_no", CAT_NAME_OBJECT);

        read_condition(&_test, algo_settings());
}

void CatCondition::algorithm_deinit()
{
        delete_condition(&_test);

        Algorithm::algorithm_deinit();
}

} // namespace CatShell
