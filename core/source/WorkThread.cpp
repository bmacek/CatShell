#include "CatShell/core/WorkThread.h"

using namespace std;

namespace CatShell {

WorkThread::WorkThread()
: _pid(0), _name(""), _thread(nullptr), _waiting_end(false), _logger(nullptr), _logger_own(false)
{
}

WorkThread::~WorkThread()
{
        if(_logger && _logger_own)
                delete _logger;

        if(_thread)
                delete _thread;
}

void WorkThread::work_init_private(std::uint32_t pid, std::function<bool (message* object)> parenth, Logger& parenth_logger, const std::string& name, Setting* settings)
{
        _pid = pid;
        _parenth = parenth;   
        _name = name;    
        _settings = settings;

        // define the logger
        if(_parenth)
        {
                _logger = parenth_logger.get_sublogger(name);
                _logger_own = true;
        }
        else
        {
                _logger = &parenth_logger;
                _logger_own = false;
        }
}

void WorkThread::run_subthread(CatPointer<WorkThread> th, const std::string& name, Setting* settings)
{
        {
                std::unique_lock<std::mutex> lock(_thread_mutex);

                std::uint32_t pid;

                pid = _children.size()+1;

                // initialise the thread
                th->work_init_private(pid, std::bind(&WaitList<message>::insert, &_messages, std::placeholders::_1), logger(), name, settings);

                // insert into children
                _children.insert(std::pair<std::uint32_t, WorkThread*>(pid, th));
        }

        th->run();

}

void WorkThread::run()
{
        std::unique_lock<std::mutex> lock(_thread_mutex);

        if(!_thread)
        {
                _thread = new std::thread(&WorkThread::main_private, this);
        }
        else
        {
        	WARNING(logger(), "Thread tried to be started, but running already.")
        }
}

void WorkThread::shutdown()
{
        std::unique_lock<std::mutex> lock(_thread_mutex);

        // flag the end
        _waiting_end = true;

        // notify all the sub-threads
        for(auto x: _children)
                x.second->shutdown();

        // wake up current thread if in infinite wait
        _messages.insert(nullptr);
}

void WorkThread::self_shutdown()
{
        // flag the end
        _waiting_end = true;

        // wake up current thread if in infinite wait
        //_messages.insert(nullptr);
}

void WorkThread::process_message(message* msg)
{
        if(!msg)
                return;

        auto child = _children.find(msg->pid);
        if(child == _children.end())
        {
                // message arrived but not from the children
                ERROR(logger(), "Message received from pid (" << msg->pid << ") which is not a child of this thread. Message: " << msg->message)
        }
        else
        {
                if(msg->message == "FINISHED")
                {
                        // sub-thread has finished

                        // join the thread
                        child->second->wait_end();
                        // notify that the child finished
                        this->work_subthread_finished(child->second);
                        // delete the thread
                        _children.erase(child);

                        LOG(logger(), "Sub-thread pid (" << msg->pid << ") finished and was unregistered.")

                        if(_end_with_children && _children.size() == 0)
                        {
                                self_shutdown();
                        }
                }
                else
                {
                        // unknown message
                        WARNING(logger(), "Message received from pid (" << msg->pid << ") but not understood. Message: " << msg->message)
                }
        }        

        delete msg;
}

bool WorkThread::should_continue(bool blocking, std::uint32_t timeout_ms)
{
        message* msg;
        while(true)
        {
                msg = nullptr;
                if(blocking)
                {
                        if(timeout_ms == 0)
                        {
                                // wait for eternity
                                _messages.wait(msg);

                                {
                                        // there were messages
                                        std::unique_lock<std::mutex> lock(_thread_mutex);

                                        process_message(msg);

                                        // check if exiting
                                        if(_waiting_end && _children.size() == 0)
                                        {
                                                return false;
                                        }

                                        continue;
                                }
                        }
                        else
                        {
                                // wait for the timeout
                                while(_messages.wait(msg, timeout_ms))
                                {
                                        // there was a message
                                        std::unique_lock<std::mutex> lock(_thread_mutex);

                                        process_message(msg);

                                        if(_waiting_end && _children.size() == 0)
                                        {
                                                return false;
                                        }                                        
                                }

                                // time-out
                                if(_waiting_end)
                                {
                                        LOG(logger(), "Waiting for " << _children.size() << " sub-threads to finish...")
                                        if(_children.size() == 0)
                                                return false;
                                }

                                return true;
                        }
                }
                else
                {
                        // check if there are any messages
                        msg = _messages.wait();

                        if(msg)
                        {
                                // there were messages
                                std::unique_lock<std::mutex> lock(_thread_mutex);

                                process_message(msg);
                                continue;
                        }
                        else
                        {
                                // there were no messages
                                std::unique_lock<std::mutex> lock(_thread_mutex);

                                if(_waiting_end)
                                {
                                        LOG(logger(), "Waiting for " << _children.size() << " sub-threads to finish...")
                                        if(_children.size() == 0)
                                                return false;
                                }

                                return true;
                        }
                }
        }
}

void WorkThread::main_private()
{
        // init the thread
        INFO(logger(), "Initializing ...")
        try
        {
                this->work_init();
        }
        catch(CatException& ex)
        {
                ERROR(logger(), "Initialization of thread '" << _name << "' failed with CatException: " << ex.explain())
                return;
        }
        catch(std::exception& ex)
        {
                ERROR(logger(), "Initialization of thread '" << _name << "' failed with std::exception: " << ex.what())
                return;
        }
        catch(...)
        {
                ERROR(logger(), "Initialization of thread '" << _name << "' failed with unknown exception.")
                return;
        }

        // run the thread
        INFO(logger(), "Running ...")
        try
        {
                this->work_main();
        }
        catch(CatException& ex)
        {
                ERROR(logger(), "Running of thread '" << _name << "' failed with CatException: " << ex.explain())
        }
        catch(std::exception& ex)
        {
                ERROR(logger(), "Running of thread '" << _name << "' failed with std::exception: " << ex.what())
        }
        catch(...)
        {
                ERROR(logger(), "Running of thread '" << _name << "' failed with unknown exception.")
        }

        // deinit the thread
        INFO(logger(), "Deinitialization ...")
        try
        {
                this->work_deinit();
        }
        catch(CatException& ex)
        {
                ERROR(logger(), "Deinitialization of thread '" << _name << "' failed with CatException: " << ex.explain())
        }
        catch(std::exception& ex)
        {
                ERROR(logger(), "Deinitialization of thread '" << _name << "' failed with std::exception: " << ex.what())
        }
        catch(...)
        {
                ERROR(logger(), "Deinitialization of thread '" << _name << "' failed with unknown exception.")
        }

        // the thread has finished, notify the parenth
        if(_parenth)
        {
                message* msg = new message;
                msg->pid = _pid;
                msg->message = "FINISHED";
                if(!_parenth(msg))
                //if(!_parenth->_messages.insert(msg))
                        ERROR(logger(), "'FINISHED' message not send, because parenth is not listening any more.")
        }
}

} // namespace CatShell
