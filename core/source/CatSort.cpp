#include "CatShell/core/CatSort.h"
#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatSort, CAT_NAME_CATSORT, CAT_TYPE_CATSORT);


CatSort::CatSort()
{

}

CatSort::~CatSort()
{

}

void CatSort::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_object && object.isSomething())
        {

		_type_in = object->cat_type();

		uint32_t c = 0;
		for(uint32_t i=0; i<_counter; i++)
		{

			if(_out_port_object[i]._strict == true && _type_in == _out_port_object[i]._type)
			{
				publish_data(_out_port_object[i]._out_port, object, logger);
				break;
			}
			else if(_out_port_object[i]._strict == false && object->s_is_derived(_out_port_object[i]._type) == true)
			{
				publish_data(_out_port_object[i]._out_port, object, logger);
				break;
			}
			c++;
		}
		if(c == _counter)
		{
			publish_data(_out_port_default, object, logger);
		}
        }

	Algorithm::process_data(port, object, flush, logger);
}

void CatSort::algorithm_init()
{
        Algorithm::algorithm_init();

        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);
	_counter=0;
	_out_port_object.reserve(1);
	for(auto& setting: algo_settings())
	{
		if(setting.name() == "port")
		{
			std::string name = setting.get_setting<std::string>("name", "");

			std::string type_set = setting.get_setting<std::string>("type","");

			if(type_set != "CatObject")
			{
				MemoryPoolBase* pool = PluginDeamon::get_memory_pool(type_set);

				_out_port_object[_counter]._type = pool->memory_cat_type();

				_out_port_object[_counter]._strict = setting.get_setting<bool>("strict", false);

				_out_port_object[_counter]._out_port = declare_output_port(name, type_set);

				_counter++;
			}
		}
	}

	_out_port_default = declare_output_port("out_default", CAT_NAME_OBJECT);


}

void CatSort::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
}


} // namespace CatShell
