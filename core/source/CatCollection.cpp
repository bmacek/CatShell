#include "CatShell/core/CatCollection.h"
#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatCollection, CAT_NAME_CATCOLLECTION, CAT_TYPE_CATCOLLECTION);

CatCollection::CatCollection()
{

}

CatCollection::~CatCollection()
{

}

StreamSize CatCollection::cat_stream_get_size() const
{
	StreamSize size = sizeof(std::uint32_t);
	for(auto it = this->begin(); it != this->end(); ++it)
		size += CatShell::PluginDeamon::stream_object_size(*it);
	return size;
}

void CatCollection::cat_stream_out(std::ostream& output)
{
	std::uint32_t x = this->size();

	// stream the size
	output.write((char*)&x, sizeof(std::uint32_t));

	// stream the objects
	StreamSize size = sizeof(std::uint32_t);
	for(auto it = this->begin(); it != this->end(); ++it)
		CatShell::PluginDeamon::stream_object_out(output, *it);
}

void CatCollection::cat_stream_in(std::istream& input)
{
	std::uint32_t x;
	this->clear();

	// read the size
	input.read((char*)&x, sizeof(std::uint32_t));

	// read the objects
	CatPointer<CatObject> obj;
	for(std::uint32_t i=0; i<x; ++i)
	{
		obj = CatShell::PluginDeamon::stream_object_in(input);
		this->push_back(obj);
	}
}

void CatCollection::cat_print(std::ostream& output, const std::string& padding)
{
	CatObject::cat_print(output, padding); output << "Collection of " << this->size() << " objects: " << endl;
	for(auto it = this->begin(); it != this->end(); ++it)
	{
		(*it)->cat_print(output, padding + " |  ");
		if((--this->end()) != it)
			output << endl; 
	}
}

void CatCollection::cat_free()
{
	this->clear();
}

void* CatCollection::cat_get_part(CatShell::CatAddress& addr)
{
	if(addr == "element")
	{
        if(addr.has_index())
        {
                if(addr.get_index() >= this->size())
                        return nullptr;
                else
                		return this->operator[](addr.get_index())->cat_get_part(++addr);
        }
        else
                return nullptr;
	}
	else if(addr == "size")
	{
		_size = this->size();
        return &_size;
	}
	else
		return nullptr;
}

} // namespace CatShell
