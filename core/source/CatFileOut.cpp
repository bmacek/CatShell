#include "CatShell/core/CatFileOut.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatFileOut, CAT_NAME_CATFILEOUT, CAT_TYPE_CATFILEOUT);


CatFileOut::CatFileOut()
: _file(nullptr)
{
}

CatFileOut::~CatFileOut()
{
}

void CatFileOut::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
  std::unique_lock<std::mutex> lock(_data_lock);

  if((port == _in_port_object) && (_file != nullptr))
  {
    if(_limit)
    {
      StreamSize size = object->cat_stream_get_size();
      CatType type = object->cat_type();
      _pile += size;

      if(_pile > _max_size)
      {
        _count++;
        _file->close();
        char text[100];
        std::string name;
        sprintf(text, "%s.%u.%s", _name.c_str(), (int)_count, _ext.c_str());
        name = text;
        _file = new fstream(name, std::ifstream::out | std::ifstream::binary);
        if(!_file->is_open())
        {
          delete _file;
          _file = nullptr;
          ERROR(logger, "File failed to open: " << name);
          return;
        }
        _pile = size;
      }

      // write the size of the object
      _file->write((char*)&size, sizeof(StreamSize));
      // write the type
      _file->write((char*)&type, sizeof(CatType));
      // write the object
      object->cat_stream_out(*_file);
    }
    else
    {
      StreamSize size = object->cat_stream_get_size();
      CatType type = object->cat_type();

      // write the size of the object
      _file->write((char*)&size, sizeof(StreamSize));
      // write the type
      _file->write((char*)&type, sizeof(CatType));
      // write the object
      object->cat_stream_out(*_file);
    }
  }
}

void CatFileOut::algorithm_init()
{
  Algorithm::algorithm_init();

  _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);

	_limit = algo_settings().get_setting<bool>("limit", false);

	if(_limit)
	{
		_max_size = algo_settings().get_setting<std::uint32_t>("max_size", 1000);
		_name = algo_settings().get_setting<std::string>("name", "out");
		_ext = algo_settings().get_setting<std::string>("extension", "cat");
		_count = 0;
    _pile = 0;
		char text[256];
		std::string name;
		sprintf(text, "%s.%u.%s", _name.c_str(), (int)_count, _ext.c_str());
		name = text;
		_file = new fstream(name, std::ifstream::out | std::ifstream::binary);
		if(!_file->is_open())
		{
			delete _file;
			_file = nullptr;
		}

	}

	else
        {
		_file = new fstream(algo_settings().get_setting<std::string>("name", "out.cat"), std::ifstream::out | std::ifstream::binary);

		if(!_file->is_open())
		{
		        delete _file;
		        _file = nullptr;
		}
	}
}

void CatFileOut::algorithm_deinit()
{
        if(_file)
        {
                _file->close();
                delete _file;
                _file = nullptr;
        }

        Algorithm::algorithm_deinit();
}

} // namespace CatShell
