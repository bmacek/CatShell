#include "CatShell/core/CatStdIn.h"
#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatStdIn, CAT_NAME_CATSTDIN, CAT_TYPE_CATSTDIN);


CatStdIn::CatStdIn()
{

}

CatStdIn::~CatStdIn()
{
}

void CatStdIn::work_main()
{
        char text[100];
        StreamSize            size;
        StreamSize            location = 0;
        CatType               type;
        MemoryPoolBase*       pool;
        CatPointer<CatObject> object;

        // prepare the exception
        std::cin.exceptions( std::ios::failbit | std::ios::badbit  | std::ios::eofbit );

        LOG(logger(), "Started reading the stream.")

        bool finish = false;
        while(!finish)
        {
                try
                {
                        while(true)
                        {                
                                // read the length
                                std::cin.read((char*)&size, sizeof(StreamSize));
                                // read the type
                                std::cin.read((char*)&type, sizeof(CatType));

                                _stat_objects++;
                                _stat_bytes += size;

                                // don't continue too long
                                if(_object_limit_n != 0 && _stat_objects > (_object_limit_min + _object_limit_n))
                                {
                                        finish = true;
                                        break;
                                }

                                // don't start too early
                                if(_stat_objects <= _object_limit_min)
                                {
                                        // before desired start

                                        // skip data
                                        std::cin.ignore(size);
                                }
                                else if(_stream_map_mode)
                                {
                                        // print 
                                        sprintf(text, "%10llu location: 0x%08x type: 0x%08x, lenght: 0x%08x", _stat_objects, location, type, size);
                                        cout << text << endl;
 
                                        // skip data
                                        std::cin.ignore(size);
                                }
                                else
                                {
                                        // get the instance
                                        pool = PluginDeamon::get_memory_pool(type);
                                        if(pool)
                                        {
                                                object = pool->get_base_element();
                                        }
                                        else
                                        {
                                                object = CatPointer<CatObject>(nullptr);
                                                _stat_type_err++;
                                        }

                                        if(object.isNull())
                                        {
                                                // object could not be instantiated
                                                std::cin.ignore(size);
                                        }
                                        else
                                        {
                                                // object exists, so read it from the stream
                                                object->cat_stream_in(std::cin);
                                                publish_data(_out_port_object, object, logger());
                                        }
                                }

                                location += size + sizeof(CatType) + sizeof(StreamSize);
                        }

                }
                catch(std::bad_alloc e)
                {
                        _stat_memory_err++;

                        // not enough space in teh memory pool
                        ERROR(logger(), "Not enough memory for objects of type: " << (int)type << ".")
                        finish = false;
                }
                catch(std::ios::failure e)
                {
                        // end of file
                        LOG(logger(), "End of file received.")
                        finish = true;
                }
        }

        // flush
        flush_data_clients(_out_port_object, logger());

        std::cin.clear();
        LOG(logger(), "Finished reading the stream.")

        START_LOG_LOG(logger())
        LINE_LOG_LOG(logger(), "End statistics:")
        logger().stream() << logger().padding() << "Total objects    : " << (_stat_objects) << endl;
        logger().stream() << logger().padding() << "Type err objects : " << (_stat_type_err) << endl;
        logger().stream() << logger().padding() << "Mem. err objects : " << (_stat_memory_err) << endl;
        logger().stream() << logger().padding() << "Total bytes read : " << (_stat_objects)*(sizeof(StreamSize)+sizeof(CatType)) + _stat_bytes << endl;
        END_LOG(logger())
}

void CatStdIn::work_init()
{
        Process::work_init();

        _stat_objects = 0;
        _stat_type_err = 0;
        _stat_memory_err = 0;
        _stat_bytes = 0;
}

void CatStdIn::work_deinit()
{
        Process::work_deinit();
}

void CatStdIn::algorithm_init()
{
        Process::algorithm_init();

        _out_port_object = declare_output_port("out_object", CAT_NAME_OBJECT);

        // check if the mode is 'map mode'
        _stream_map_mode = algo_settings().get_setting<bool>("map_mode", false);

        // check if there is a limit on the object count
        _object_limit_min = algo_settings().get_setting<std::uint32_t>("object_interval/start", 0);
        _object_limit_n = algo_settings().get_setting<std::uint32_t>("object_interval/number", 0);
}

void CatStdIn::algorithm_deinit()
{
        Process::algorithm_deinit();
}

} // namespace CatShell
