#include "CatShell/core/Algorithm.h"

using namespace std;

namespace CatShell {

const Algorithm::DataPort Algorithm::AllPorts = 0;

Algorithm::Algorithm()
{
}

Algorithm::~Algorithm()
{
}

void Algorithm::algorithm_init()
{
}

void Algorithm::algorithm_deinit()
{
        _ports_output.clear();
        _ports_input.clear();
        _map_ports_input.clear();
}

void Algorithm::untie()
{
        _ports_output.clear();
        _ports_input.clear();
        _map_ports_input.clear();
}

void Algorithm::flush_data_clients(DataPort port, CatShell::Logger& logger)
{
        CatPointer<CatObject> fake_object(nullptr);
        if(port == 0)
        {
                for(auto& p: _ports_output)
                {
                        for(auto& x: p.links)
                        {
                                x.process->process_data(x.link, fake_object, true, logger);
                        }                
                }
        }
        else
        {
                for(auto& x: _ports_output[port-1].links)
                {
                        x.process->process_data(x.link, fake_object, true, logger);
                }                
        }
}

void Algorithm::publish_data(DataPort port, CatPointer<CatObject> object, CatShell::Logger& logger)
{
        for(auto& x: _ports_output[port-1].links)
        {
                x.process->process_data(x.link, object, false, logger);
        }
}

CatPointer<CatObject> Algorithm::request_data(DataPort port, bool& flush, CatShell::Logger& logger)
{
        CatPointer<CatObject> result(nullptr);

        // finds the specified port and requests data from whoever is connected there
        for(auto& x: _ports_input[port-1].links)
        {
                flush = false;
                result = x.process->produce_data(x.link, flush, logger);
                if(result.isSomething())
                        return result;
        }
        return result;
}

bool Algorithm::register_data_client(DataPort output_port, CatPointer<Algorithm>& client, DataPort client_port)
{
        for(auto& x: _ports_output)
        {
                if(x.port == output_port)
                {
                        // list it for this ouput port
                        x.links.emplace_back(data_link_t{client, client_port});
                        // list it for the destination input port
                        for(auto&y : client->_ports_input)
                        {
                                if(y.port == client_port)
                                {
                                        y.links.emplace_back(data_link_t{CatPointer<Algorithm>(this), output_port});
                                        return true;
                                }
                        }
                }
        }
        return false;
}

Algorithm::DataPort Algorithm::get_input_port(const std::string& name) const
{
        auto x =  _map_ports_input.find(name);
        if(x == _map_ports_input.end())
                return 0;
        else
                return x->second.port;
}

Algorithm::DataPort Algorithm::get_output_port(const std::string& name) const
{
        for(auto& x: _ports_output)
        {
                if(x.name == name)
                {
                        return x.port;
                }
        }
        return 0;
}

std::string Algorithm::get_input_port_type(const DataPort port) const
{
        for(auto& x: _ports_input)
        {
                if(x.port == port)
                {
                        return x.type;
                }
        }
        return "";
}

std::string Algorithm::get_output_port_type(const DataPort port) const
{
        for(auto& x: _ports_output)
        {
                if(x.port == port)
                {
                        return x.type;
                }
        }
        return "";
}



Algorithm::DataPort Algorithm::declare_output_port(const std::string& name, const std::string& type, std::uint8_t number_of_ports)
{
        char text[100];

        Algorithm::DataPort port = _ports_output.size()+1;

        for(std::uint8_t i=0; i<number_of_ports; ++i)
        {
                output_port_t tmp;

                if(number_of_ports == 1)
                {
                        tmp.name = name;
                }
                else
                {
                        sprintf(text, "%s_%03u", name.c_str(), (int)i);
                        tmp.name = text;
                }
                tmp.type = type;
                tmp.port = _ports_output.size()+1; // this way the port is never assigned 0!!
                _ports_output.push_back(tmp);
        }
        return port;
}

Algorithm::DataPort Algorithm::declare_input_port(const std::string& name, const std::string& type, std::uint8_t number_of_ports)
{
        char text[100];

        Algorithm::DataPort port = _ports_input.size()+1;

        for(std::uint8_t i=0; i<number_of_ports; ++i)
        {
                input_port_t tmp;

                if(number_of_ports == 1)
                {
                        tmp.name = name;
                }
                else
                {
                        sprintf(text, "%s_%03u", name.c_str(), (int)i);
                        tmp.name = text;
                }
                tmp.type = type;
                tmp.port = _ports_input.size()+1; // this way the port is never assigned 0!!
                _ports_input.push_back(tmp);
                _map_ports_input.insert(std::pair<std::string, input_port_t>(tmp.name, tmp));
        }
        return port;
}

} // namespace CatShell
