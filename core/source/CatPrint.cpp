#include "CatShell/core/CatPrint.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatPrint, CAT_NAME_CATPRINT, CAT_TYPE_CATPRINT);


CatPrint::CatPrint()
{

}

CatPrint::~CatPrint()
{

}

void CatPrint::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_object && object.isSomething())
        {
                if(flush)
                {
                        cout << flush;
                }
                else
                {
                        cout << "--> Object no. " << _counter << endl; 
                        object->cat_print(std::cout, "");
                        cout << endl;
                        _counter++;
                }
        }
}

void CatPrint::algorithm_init()
{
        Algorithm::algorithm_init();

        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);

        _counter = 0;
}

void CatPrint::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
}

} // namespace CatShell
