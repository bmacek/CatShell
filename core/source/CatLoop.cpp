#include "CatShell/core/CatLoop.h"
#include "CatShell/core/PluginDeamon.h"

#include <fstream>
#include <iostream>

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatLoop, CAT_NAME_CATLOOP, CAT_TYPE_CATLOOP);


CatLoop::CatLoop()
{

}

CatLoop::~CatLoop()
{
}

void CatLoop::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(!flush)
        {
                if(_counter >= _filter_start)
                {
                        if(_counter < _filter_end)
                        {
                                publish_data(_out_port_object, object, logger);
                                ++_counter;
                        }
                        else
                                flush = true;
                }
                else
                        ++_counter;
        }

        Algorithm::process_data(port, object, flush, logger);
}

CatShell::CatPointer<CatShell::CatObject> CatLoop::produce_data(DataPort port, bool& flush, CatShell::Logger& logger)
{
        while(true)
        {
                if(_counter >= _filter_start)
                {
                        if(_counter < _filter_end)
                        {
                                ++_counter;
                                return request_data(_in_port_object, flush, logger);
                        }
                        else
                        {
                                flush = true;
                                return CatPointer<CatObject>(nullptr);
                        }
                }
                else
                {
                        request_data(_in_port_object, flush, logger);
                        ++_counter;
                        if(flush)
                                return CatPointer<CatObject>(nullptr);
                }
        }

        return CatPointer<CatObject>(nullptr);
}

void CatLoop::work_main()
{
        CatPointer<CatObject> obj;
        bool flush;

        LOG(logger(), "Starting the loop through interval  [" << _filter_start << ", " << _filter_end << ").")

        while(true)
        {
                if(!should_continue(false))
                        break;
                if(_counter >= _filter_start)
                {
                        if(_counter < _filter_end)
                        {
                                ++_counter;
                                obj = request_data(_in_port_object, flush, logger());
                                if(flush)
                                        break;
                                publish_data(_out_port_object, obj, logger());
                        }
                        else
                        {
                                break;
                        }
                }
                else
                {
                        request_data(_in_port_object, flush, logger());
                        ++_counter;
                        if(flush)
                                break;
                }
        }

        // flush
        flush_data_clients(_out_port_object, logger());

        LOG(logger(), "Finished the loop at index " << _counter)
}

void CatLoop::algorithm_init()
{
        Process::algorithm_init();

        // declare the input port
        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);
        _out_port_object = declare_output_port("out_object", CAT_NAME_OBJECT);

        _counter = 0;
        _filter_start = algo_settings().get_setting<std::uint32_t>("loop_start", 0);
        _filter_end = algo_settings().get_setting<std::uint32_t>("loop_end", std::numeric_limits<std::uint32_t>::max());
}


} // namespace CatShell
