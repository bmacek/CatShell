#include "CatShell/core/CatPartPrint.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatPartPrint, CAT_NAME_CATPARTPRINT, CAT_TYPE_CATPARTPRINT);


CatPartPrint::CatPartPrint()
{

}

CatPartPrint::~CatPartPrint()
{

}

void CatPartPrint::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        void* pointer;
        if(port == _in_port_object && object.isSomething())
        {
                size_t length = _variables.size();
                for(size_t i=0; i<length; ++i)
                {
                        _variables[i].address.begin();
                        pointer = object->cat_get_part(_variables[i].address);
                        if(pointer)
                        {
                                if(_variables[i].type == "std::uint16_t")
                                        cout << (*(std::uint16_t*)pointer);
                                else if(_variables[i].type == "std::uint32_t")
                                        cout << (*(std::uint32_t*)pointer);
                                else if(_variables[i].type == "std::uint64_t")
                                        cout << (*(std::uint64_t*)pointer);
                        }
                        else
                                cout << _variables[i].def;

                        if(i != (length-1))
                                cout << _separator;
                }
                cout << endl;
                _counter++;
        }

        if(flush)
        {
                cout << flush;
        }
}

void CatPartPrint::algorithm_init()
{
        Algorithm::algorithm_init();

        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);

        _counter = 0;

        _separator = algo_settings().get_setting<std::string>("separator", " ");
        // make a list of everything we want to print

        for(auto& setting: algo_settings()["variables"])
        {
                if(setting.name() == "variable")
                {
                        tVariable tmp;
                        tmp.address = setting.get_setting<std::string>("name", "");
                        tmp.def = setting.get_setting<std::string>("default", " - ");
                        tmp.type = setting.get_setting<std::string>("type", "std::uint8_t");

                        _variables.push_back(tmp);
                }
        }
}

void CatPartPrint::algorithm_deinit()
{
        Algorithm::algorithm_deinit();

        _variables.clear();
}

} // namespace CatShell
