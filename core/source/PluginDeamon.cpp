#include "CatShell/core/PluginDeamon.h"
#include "CatShell/core/CatException.h"
#include "CatShell/core/defines.h"

#include <signal.h>
#include <dlfcn.h>
#include <iostream>
#include <cstdlib>
#include <execinfo.h>
#include <cxxabi.h>

// in case OS does not define it
#ifndef RTLD_DEEPBIND
	#define RTLD_DEEPBIND (0)
#endif

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(PluginDeamon, "CatPluginDeamon", CAT_TYPE_PLUGINDEAMON)

PluginDeamon* PluginDeamon::s_instance = nullptr;

//

void PluginDeamon::Stack()
{
	char text[500];
	size_t addrlen;
	void* stack[50];

	addrlen = backtrace(stack, 50);
	
	char** symbollist = backtrace_symbols(stack, addrlen);

    // allocate string which will be filled with the demangled function name
    size_t funcnamesize = 256;
    char* funcname = (char*)malloc(funcnamesize);

    // iterate over the returned symbol lines. skip the first, it is the
    // address of this function.
    for (int i = 1; i < addrlen; i++)
    {
		char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

		// find parentheses and +address offset surrounding the mangled name:
		// ./module(function+0x15c) [0x8048a6d]
		for (char *p = symbollist[i]; *p; ++p)
		{
		    if (*p == '(')
			begin_name = p;
		    else if (*p == '+')
			begin_offset = p;
		    else if (*p == ')' && begin_offset) {
			end_offset = p;
			break;
		    }
		}

		if (begin_name && begin_offset && end_offset && begin_name < begin_offset)
		{
		    *begin_name++ = '\0';
		    *begin_offset++ = '\0';
		    *end_offset = '\0';

		    // mangled name is now in [begin_name, begin_offset) and caller
		    // offset in [begin_offset, end_offset). now apply
		    // __cxa_demangle():

		    int status;
		    char* ret = abi::__cxa_demangle(begin_name, funcname, &funcnamesize, &status);
		    if (status == 0) {
			funcname = ret; // use possibly realloc()-ed string
			sprintf(text, "  %s : %s+%s",
				symbollist[i], funcname, begin_offset);
		    }
		    else {
			// demangling failed. Output function name as a C function with
			// no arguments.
			sprintf(text, "  %s : %s()+%s",
				symbollist[i], begin_name, begin_offset);
		    }

			ERROR(s_instance->logger(), text)   
		}
		else
		{
		    // couldn't parse the line? print the whole line.
		    sprintf(text, "  %s", symbollist[i]);

			ERROR(s_instance->logger(), text)   
		}
    }

    free(funcname);
    free(symbollist);
//  for (int i = 0; i < size; ++i)
//	{
//		ERROR(s_instance->logger(), strs[i])
//	}
}

PluginDeamon::PluginDeamon()
{
	s_instance = this;
}

PluginDeamon::~PluginDeamon()
{
}

void PluginDeamon::work_main()
{
	bool success = true;
	{
		std::unique_lock<std::mutex> lock(mutex());

		//
		// load all the processes
		//
		for(auto& setting: settings()["algorithms"])
		{
			if(setting.name() == "algorithm")
			{
				bool n_bool = setting.has_element("number");
				std::uint32_t number = setting.get_setting<std::uint32_t>("number",1);

				// multiple ports
				if(n_bool == true)
				{
					char text[100];
					std::string name;

					for(std::uint32_t i=0; i<number; i++)
					{
						// get name
						sprintf(text,"%s_%03u",setting.get_setting<std::string>("name","").c_str(),(int)i);
						name = text;
						if(name.size() == 0)
						{
							ERROR(logger(), "Name for the process not found! Process will not be created.")
							success = false;
							break;
						}
						// get settings
						std::string set_name = setting.get_setting<std::string>("settings","");
						if(set_name.size() == 0)
						{
							ERROR(logger(), "Settings path for the process '" << name << "' not found! Process will not be created.")
							success = false;
							break;
						}
						Setting* set_set = settings().get_setting_node(set_name, nullptr);
						if(!set_set)
						{
							ERROR(logger(), "Settings for the process '" << name << "' on path '" << set_name << "' not found! Process will not be created.")
							success = false;
							break;
						}
						MemoryPoolBase* mp = get_memory_pool_private(setting.get_setting<std::string>("type", ""));
						if(!mp)
						{
							ERROR(logger(), "Factory for the process '" << name << "' not found! Process will not be created.")
							success = false;
							break;
						}

						CatPointer<Algorithm> algo(mp->get_base_element().cast<Algorithm>());
						if(!algo)
						{
							// could not be initialised
							ERROR(logger(), "Process of type'" << setting.get_setting<std::string>("type", "") << "' could not be instantiated! Process will not be created.")
							success = false;
							break;
						}
			   
						// set the settings
						algo->set_settings(set_set);

						// add to the list of processes
						auto it = _algorithms.insert(std::pair<std::string, CatPointer<Algorithm>>(name, algo));
						if(!it.second)
						{
							ERROR(logger(), "Process named '" << name << "' already exist! Process will not be created.")
							success = false;
							break;
						}

						INFO(logger(), "Algorithm '" << name << "' instantiated.")
					}
					if(success == false) break;
				}

				// one port
				else if(n_bool == false)
				{

					// get name
					std::string name = setting.get_setting<std::string>("name", "");
					if(name.size() == 0)
					{
						ERROR(logger(), "Name for the process not found! Process will not be created.")
						success = false;
						break;
					}

					// get settings
					std::string set_name = setting.get_setting<std::string>("settings", "");
					if(set_name.size() == 0)
					{
						ERROR(logger(), "Settings path for the process '" << name << "' not found! Process will not be created.")
						success = false;
						break;
					}
					Setting* set_set = settings().get_setting_node(set_name, nullptr);
					if(!set_set)
					{
						ERROR(logger(), "Settings for the process '" << name << "' on path '" << set_name << "' not found! Process will not be created.")
						success = false;
						break;
					}

					// get the process instance
					MemoryPoolBase* mp = get_memory_pool_private(setting.get_setting<std::string>("type", ""));
					if(!mp)
					{
						ERROR(logger(), "Factory for the process '" << name << "' not found! Process will not be created.")
						success = false;
						break;
					}

					CatPointer<Algorithm> algo(mp->get_base_element().cast<Algorithm>());
					if(!algo)
					{
						// could not be initialised
						ERROR(logger(), "Process of type'" << setting.get_setting<std::string>("type", "") << "' could not be instantiated! Process will not be created.")
						success = false;
						break;
					}
		   
					// set the settings
					algo->set_settings(set_set);

					// add to the list of processes
					auto it = _algorithms.insert(std::pair<std::string, CatPointer<Algorithm>>(name, algo));
					if(!it.second)
					{
						ERROR(logger(), "Process named '" << name << "' already exist! Process will not be created.")
						success = false;
						break;
					}

					INFO(logger(), "Algorithm '" << name << "' instantiated.")
				}
			}
		}

		// if process loading failed finish the program
		if(!success)
		{
			finish();
			return;
		}

	}
		// initialize the processes
		INFO(logger(), "Initializing algorithms.")
		for(auto& x: _algorithms)
		{
			x.second->algorithm_init();
		}

	{
		std::unique_lock<std::mutex> lock(mutex());
		
		//
		// create all the links
		//
		for(auto& setting: settings()["links"])
		{
			if(setting.name() == "link")
			{
				bool                saf_b = setting.has_element("source_algo_from");
				std::uint32_t       saf = setting.get_setting<std::uint32_t>("source_algo_from",0);
				bool                spf_b = setting.has_element("source_port_from");
				std::uint32_t       spf = setting.get_setting<std::uint32_t>("source_port_from",0);
				bool                daf_b = setting.has_element("destination_algo_from");
				std::uint32_t       daf = setting.get_setting<std::uint32_t>("destination_algo_from",0);
				bool                dpf_b = setting.has_element("destination_port_from");
				std::uint32_t       dpf = setting.get_setting("destination_port_from",0);
				std::uint32_t       number = setting.get_setting<std::uint32_t>("number",1);

				// 1 algo/1 port to 1a/1p
				if(saf_b == false && spf_b == false && daf_b == false && dpf_b == false)
				{
					// algorithms
					CatPointer<Algorithm> algo_src = get_algorithm(setting.get_setting<std::string>("source_algo", ""));
					CatPointer<Algorithm> algo_dst = get_algorithm(setting.get_setting<std::string>("destination_algo", ""));
					if(algo_src.isNull() || algo_dst.isNull())
					{
						ERROR(logger(), "Processes '" << setting.get_setting<std::string>("destination_algo", "") << "' or '" << setting.get_setting<std::string>("source_algo", "") << "' not found, so link will not be established.")
						success = false;
						break;
					}
					// ports
					Algorithm::DataPort    port_src = algo_src->get_output_port(setting.get_setting<std::string>("source_port", ""));
					if(port_src == 0)
					{
						ERROR(logger(), "Port '" + setting.get_setting<std::string>("source_port", "") + "' not found in source algorithm: '" + setting.get_setting<std::string>("source_algo", "") + "'. Link will not be established.")
						success = false;
						break;
					}
					Algorithm::DataPort    port_dst = algo_dst->get_input_port(setting.get_setting<std::string>("destination_port", ""));
					if(port_dst == 0)
					{
						ERROR(logger(), "Port '" + setting.get_setting<std::string>("destination_port", "") + "' not found in destination algorithm: '" + setting.get_setting<std::string>("destination_algo", "") + "'. Link will not be established.")
						success = false;
						break;
					}

					// check port types
					std::string            port_src_type = algo_src->get_output_port_type(port_src);
					std::string            port_dst_type = algo_dst->get_input_port_type(port_dst);
					if(is_derived(port_dst_type, port_src_type) == false)
					{
						ERROR(logger(), "Port '" + setting.get_setting<std::string>("source_port", "") + "' of type '" + port_src_type + "' in source algorithm: '" + setting.get_setting<std::string>("source_algo", "") + "' is not type-compatible with port '" + setting.get_setting<std::string>("destination_port", "") + "' of type '" + port_dst_type + "' in destination algorithm: '" + setting.get_setting<std::string>("destination_algo", "") + "'. Link will not be established.")
						success = false;
						break;
					}

					// create links
					algo_src->register_data_client(port_src, algo_dst, port_dst);

					LOG(logger(), "Link '" << setting.get_setting<std::string>("source_algo", "") << "[" << setting.get_setting<std::string>("source_port", "") << "]->" << setting.get_setting<std::string>("destination_algo", "") << "[" << setting.get_setting<std::string>("destination_port", "") << "]' created.")
				}

				// 1a/1p to 1a/np + 1a/np to 1a/1p
				if((dpf_b == true && saf_b == false && spf_b == false && daf_b == false) || (spf_b == true && saf_b == false && daf_b == false && dpf_b == false))
				{
					// algorithms
					CatPointer<Algorithm> algo_src = get_algorithm(setting.get_setting<std::string>("source_algo", ""));
					CatPointer<Algorithm> algo_dst = get_algorithm(setting.get_setting<std::string>("destination_algo", ""));
					if(algo_src.isNull() || algo_dst.isNull())
					{
						ERROR(logger(), "Processes '" << setting.get_setting<std::string>("destination_algo", "") << "' or '" << setting.get_setting<std::string>("source_algo", "") << "' not found, so link will not be established.")
						success = false;
						break;
					}

					Algorithm::DataPort port_src;
					Algorithm::DataPort port_dst;

					char text[100];
					std::string port_src_name;
					std::string port_dst_name;

					if(dpf_b == true)
					{
						// ports
						port_src_name = setting.get_setting<std::string>("source_port","");
						port_src = algo_src->get_output_port(port_src_name);
						if(port_src == 0)
						{
							ERROR(logger(), "Port '" + port_src_name + "' not found in source algorithm: '" + setting.get_setting<std::string>("source_algo", "") + "'. Link will not be established.")
							success = false;
							break;
						}
						for(uint32_t i = dpf; i<dpf+number; i++)
						{
							sprintf(text,"%s_%03u",setting.get_setting<std::string>("destination_port","").c_str(),(int)i);
							port_dst_name = text;
							port_dst = algo_dst->get_input_port(port_dst_name);
							
							if(port_dst == 0)
							{
								ERROR(logger(), "Port '" + port_dst_name + "' not found in destination algorithm: '" + setting.get_setting<std::string>("destination_algo", "") + "'. Link will not be established.")
								success = false;
								break;
							}
							
							// check port types
							std::string port_src_type = algo_src->get_output_port_type(port_src);
							std::string port_dst_type = algo_dst->get_input_port_type(port_dst);

							if(is_derived(port_dst_type, port_src_type) == false)
							{
								ERROR(logger(), "Port '" + port_src_name + "' of type '" + port_src_type + "' in source algorithm: '" + setting.get_setting<std::string>("source_algo", "") + "' is not type-compatible with port '" + port_dst_name + "' of type '" + port_dst_type + "' in destination algorithm: '" + setting.get_setting<std::string>("destination_algo", "") + "'. Link will not be established.")
								success = false;
								break;
							}

							// create links
							algo_src->register_data_client(port_src, algo_dst, port_dst);

							LOG(logger(), "Link '" << setting.get_setting<std::string>("source_algo", "") << "[" << port_src_name << "]->" << setting.get_setting<std::string>("destination_algo", "") << "[" << port_dst_name << "]' created.")
						}
						if(success == false) break;
					}

					if(spf_b == true)
					{
						// ports
						port_dst_name = setting.get_setting<std::string>("destination_port","");
						port_dst = algo_dst->get_input_port(port_dst_name);
						if(port_dst == 0)
						{
							ERROR(logger(), "Port '" + port_dst_name + "' not found in destination algorithm: '" + setting.get_setting<std::string>("destination_algo", "") + "'. Link will not be established.")
							success = false;
							break;
						}
						for(uint32_t i = spf; i<spf+number; i++)
						{
							sprintf(text,"%s_%03u",setting.get_setting<std::string>("source_port","").c_str(),(int)i);
							port_src_name = text;
							port_src = algo_src->get_output_port(port_src_name);
							
							if(port_src == 0)
							{
								ERROR(logger(), "Port '" + port_src_name + "' not found in source algorithm: '" + setting.get_setting<std::string>("source_algo", "") + "'. Link will not be established.")
								success = false;
								break;
							}
							
							// check port types
							std::string port_src_type = algo_src->get_output_port_type(port_src);
							std::string port_dst_type = algo_dst->get_input_port_type(port_dst);

							if(is_derived(port_dst_type, port_src_type) == false)
							{
								ERROR(logger(), "Port '" + port_src_name + "' of type '" + port_src_type + "' in source algorithm: '" + setting.get_setting<std::string>("source_algo", "") + "' is not type-compatible with port '" + port_dst_name + "' of type '" + port_dst_type + "' in destination algorithm: '" + setting.get_setting<std::string>("destination_algo", "") + "'. Link will not be established.")
								success = false;
								break;
							}

							// create links
							algo_src->register_data_client(port_src, algo_dst, port_dst);

							LOG(logger(), "Link '" << setting.get_setting<std::string>("source_algo", "") << "[" << port_src_name << "]->" << setting.get_setting<std::string>("destination_algo", "") << "[" << port_dst_name << "]' created.")
						}
						if(success == false) break;
					}
				}

				//1a/np to 1a/np
				if(saf_b == false && daf_b == false && spf_b == true && dpf_b == true)
				{
					// algorithms
					CatPointer<Algorithm> algo_src = get_algorithm(setting.get_setting<std::string>("source_algo", ""));
					CatPointer<Algorithm> algo_dst = get_algorithm(setting.get_setting<std::string>("destination_algo", ""));
					if(algo_src.isNull() || algo_dst.isNull())
					{
						ERROR(logger(), "Processes '" << setting.get_setting<std::string>("destination_algo", "") << "' or '" << setting.get_setting<std::string>("source_algo", "") << "' not found, so link will not be established.")
						success = false;
						break;
					}

					Algorithm::DataPort port_src;
					Algorithm::DataPort port_dst;

					char text[100];
					std::string port_src_name;
					std::string port_dst_name;

					for(uint32_t i = spf; i<spf+number; i++)
					{
						// ports
						sprintf(text,"%s_%03u",setting.get_setting<std::string>("source_port","").c_str(),(int)i);
						port_src_name = text;
						port_src = algo_src->get_output_port(port_src_name);
						if(port_src == 0)
						{
							ERROR(logger(), "Port '" + port_src_name + "' not found in source algorithm: '" + setting.get_setting<std::string>("source_algo", "") + "'. Link will not be established.")
							success = false;
							break;
						}
						sprintf(text,"%s_%03u",setting.get_setting<std::string>("destination_port","").c_str(),(int)(i-spf+dpf));
						port_dst_name =text;
						port_dst = algo_dst->get_input_port(port_dst_name);
						if(port_dst == 0)
						{
							ERROR(logger(), "Port '" + port_dst_name + "' not found in destination algorithm: '" + setting.get_setting<std::string>("destination_algo","") + "'. Link will not be established.")
							success = false;
							break;
						}

						// check port types
						std::string port_src_type = algo_src->get_output_port_type(port_src);
						std::string port_dst_type = algo_dst->get_input_port_type(port_dst);

						if(is_derived(port_dst_type, port_src_type) == false)
						{
							ERROR(logger(), "Port '" + port_src_name + "' of type '" + port_src_type + "' in source algorithm: '" + setting.get_setting<std::string>("source_algo", "") + "' is not type-compatible with port '" + port_dst_name + "' of type '" + port_dst_type + "' in destination algorithm: '" + setting.get_setting<std::string>("destination_algo", "") + "'. Link will not be established.")
							success = false;
							break;
						}

						// create links
						algo_src->register_data_client(port_src, algo_dst, port_dst);

						LOG(logger(), "Link '" << setting.get_setting<std::string>("source_algo", "") << "[" << port_src_name << "]->" << setting.get_setting<std::string>("destination_algo", "") << "[" << port_dst_name << "]' created.")
					}
					if(success == false) break;
				}

				// 1a/1p to na/1p + na/1p to 1a/1p
				if((daf_b == true && saf_b == false && spf_b == false && dpf_b == false) || (saf_b == true && spf_b == false && daf_b == false && dpf == false))
				{
					char text [100];
					std::string algo_src_name;
					std::string algo_dst_name;

					CatPointer<Algorithm> algo_src;
					CatPointer<Algorithm> algo_dst;
					
					if(daf_b == true)
					{
						// algorithms
						algo_src_name = setting.get_setting<std::string>("source_algo","");
						algo_src = get_algorithm(algo_src_name);
						if(algo_src.isNull())
						{
							ERROR(logger(), "Process '" << algo_src_name << "' not found, so link will not be established.")
							success = false;
							break;
						}
						for(uint32_t i = daf; i<daf+number; i++)
						{
							sprintf(text,"%s_%03u",setting.get_setting<std::string>("destination_algo","").c_str(),(int)i);
							algo_dst_name = text;
							algo_dst = get_algorithm(algo_dst_name);
							if(algo_dst.isNull())
							{
								ERROR(logger(), "Process '" << algo_dst_name << "' not found, so link will not be established.")
								success = false;
								break;
							}
							
							// ports
							Algorithm::DataPort    port_src = algo_src->get_output_port(setting.get_setting<std::string>("source_port", ""));
							if(port_src == 0)
							{
								ERROR(logger(), "Port '" + setting.get_setting<std::string>("source_port", "") + "' not found in source algorithm: '" + algo_src_name + "'. Link will not be established.")
								success = false;
								break;
							}
							Algorithm::DataPort    port_dst = algo_dst->get_input_port(setting.get_setting<std::string>("destination_port", ""));
							if(port_dst == 0)
							{
								ERROR(logger(), "Port '" + setting.get_setting<std::string>("destination_port", "") + "' not found in destination algorithm: '" + algo_dst_name + "'. Link will not be established.")
								success = false;
								break;
							}

							// check port types
							std::string            port_src_type = algo_src->get_output_port_type(port_src);
							std::string            port_dst_type = algo_dst->get_input_port_type(port_dst);
							if(is_derived(port_dst_type, port_src_type) == false)
							{
								ERROR(logger(), "Port '" + setting.get_setting<std::string>("source_port", "") + "' of type '" + port_src_type + "' in source algorithm: '" + algo_src_name + "' is not type-compatible with port '" + setting.get_setting<std::string>("destination_port", "") + "' of type '" + port_dst_type + "' in destination algorithm: '" + algo_dst_name + "'. Link will not be established.")
								success = false;
								break;
							}

							// create links
							algo_src->register_data_client(port_src, algo_dst, port_dst);

							LOG(logger(), "Link '" << algo_src_name << "[" << setting.get_setting<std::string>("source_port", "") << "]->" << algo_dst_name << "[" << setting.get_setting<std::string>("destination_port", "") << "]' created.")
						}
						if(success == false) break;
					}
					
					if(saf_b == true)
					{
						// algorithms
						algo_dst_name = setting.get_setting<std::string>("destination_algo","");
						algo_dst = get_algorithm(algo_dst_name);
						if(algo_dst.isNull())
						{
							ERROR(logger(), "Process '" << algo_dst_name << "' not found, so link will not be established.")
							success = false;
							break;
						}
						for(uint32_t i = saf; i<saf+number; i++)
						{
							sprintf(text,"%s_%03u",setting.get_setting<std::string>("source_algo","").c_str(),(int)i);
							algo_src_name = text;
							algo_src = get_algorithm(algo_src_name);
							if(algo_src.isNull())
							{
								ERROR(logger(), "Process '" << algo_src_name << "' not found, so link will not be established.")
								success = false;
								break;
							}
							
							// ports
							Algorithm::DataPort    port_src = algo_src->get_output_port(setting.get_setting<std::string>("source_port", ""));
							if(port_src == 0)
							{
								ERROR(logger(), "Port '" + setting.get_setting<std::string>("source_port", "") + "' not found in source algorithm: '" + algo_src_name + "'. Link will not be established.")
								success = false;
								break;
							}
							Algorithm::DataPort    port_dst = algo_dst->get_input_port(setting.get_setting<std::string>("destination_port", ""));
							if(port_dst == 0)
							{
								ERROR(logger(), "Port '" + setting.get_setting<std::string>("destination_port", "") + "' not found in destination algorithm: '" + algo_dst_name + "'. Link will not be established.")
								success = false;
								break;
							}

							// check port types
							std::string            port_src_type = algo_src->get_output_port_type(port_src);
							std::string            port_dst_type = algo_dst->get_input_port_type(port_dst);
							if(is_derived(port_dst_type, port_src_type) == false)
							{
								ERROR(logger(), "Port '" + setting.get_setting<std::string>("source_port", "") + "' of type '" + port_src_type + "' in source algorithm: '" + algo_src_name + "' is not type-compatible with port '" + setting.get_setting<std::string>("destination_port", "") + "' of type '" + port_dst_type + "' in destination algorithm: '" + algo_dst_name + "'. Link will not be established.")
								success = false;
								break;
							}

							// create links
							algo_src->register_data_client(port_src, algo_dst, port_dst);

							LOG(logger(), "Link '" << algo_src_name << "[" << setting.get_setting<std::string>("source_port", "") << "]->" << algo_dst_name << "[" << setting.get_setting<std::string>("destination_port", "") << "]' created.")
						}
						if(success == false) break;
					}
				}

				// na/1p to na/1p
				if(saf_b == true && daf_b == true && spf_b == false && dpf_b == true)
				{
					char text[100];
					std::string algo_src_name;
					std::string algo_dst_name;

					CatPointer<Algorithm> algo_src;
					CatPointer<Algorithm> algo_dst;

					for(uint32_t i=saf; i<saf+number; i++)
					{
						// algorithms
						sprintf(text,"%s_%03u",setting.get_setting<std::string>("source_algo","").c_str(),(int)i);
						algo_src_name = text;

						sprintf(text,"%s_%03u",setting.get_setting<std::string>("destination_algo","").c_str(),(int)(i-saf+daf));
						algo_dst_name = text;

						algo_src = get_algorithm(algo_src_name);
						algo_dst = get_algorithm(algo_dst_name);
						if(algo_src.isNull() || algo_dst.isNull())
						{
							ERROR(logger(), "Processes '" << algo_dst_name << "' or '" << algo_src_name << "' not found, so link will not be established.")
							success = false;
							break;
						}

						// ports
						Algorithm::DataPort    port_src = algo_src->get_output_port(setting.get_setting<std::string>("source_port", ""));
						if(port_src == 0)
						{
							ERROR(logger(), "Port '" + setting.get_setting<std::string>("source_port", "") + "' not found in source algorithm: '" + algo_src_name + "'. Link will not be established.")
							success = false;
							break;
						}
						Algorithm::DataPort    port_dst = algo_dst->get_input_port(setting.get_setting<std::string>("destination_port", ""));
						if(port_dst == 0)
						{
							ERROR(logger(), "Port '" + setting.get_setting<std::string>("destination_port", "") + "' not found in destination algorithm: '" + algo_dst_name + "'. Link will not be established.")
							success = false;
							break;
						}

						// check port types
						std::string            port_src_type = algo_src->get_output_port_type(port_src);
						std::string            port_dst_type = algo_dst->get_input_port_type(port_dst);
						if(is_derived(port_dst_type, port_src_type) == false)
						{
							ERROR(logger(), "Port '" + setting.get_setting<std::string>("source_port", "") + "' of type '" + port_src_type + "' in source algorithm: '" + algo_src_name + "' is not type-compatible with port '" + setting.get_setting<std::string>("destination_port", "") + "' of type '" + port_dst_type + "' in destination algorithm: '" + algo_dst_name + "'. Link will not be established.")
							success = false;
							break;
						}

						// create links
						algo_src->register_data_client(port_src, algo_dst, port_dst);

						LOG(logger(), "Link '" << algo_src_name << "[" << setting.get_setting<std::string>("source_port", "") << "]->" << algo_dst_name << "[" << setting.get_setting<std::string>("destination_port", "") << "]' created.")
					}
					if(success == false) break;
				}
			}
		}

		// if link loading failed finish the program
		if(!success)
		{
			finish();
			return;
		}

		//
		// run the processes that are runnable
		//
		uint8_t sequence = 0;

		for(auto& setting: settings()["processes"])
		{
			if(setting.name() == "process")
			{
				// this is a new list of processes
				process_list_t* p_list = new process_list_t;
				_processes.push_back(p_list);
				++sequence;

				// find all within this list
				for(auto& list_setting: setting)
				{
					if(list_setting.name() == "name")
					{
						// get process name and process
						std::string name = list_setting.get_setting<std::string>("", "");
						CatPointer<Process> process = get_algorithm(name).cast<Process>();

						if(process.isNull())
						{
							ERROR(logger(), "Processes '" << name << "' not found.")
							success = false;
							break;
						}

						if(!p_list->insert(std::pair<std::string, CatPointer<Process>>(name, process)).second)
						{
							ERROR(logger(), "Processes with name '" << name << "' already in the list.")
							success = false;
							break;
						}

						LOG(logger(), "Process '" << name << "' added to the list #" << (int)sequence << ".")
					}
				}

			}
		}

		// if processes
		if(!success)
		{
			finish();
			return;
		}
	}

	for(auto& process_batch: _processes)
	{
		//
		// check if any processes at all
		//
		if(has_user_flag("-f") && process_batch->size() == 0)
		{
			WARNING(logger(), "No processes configured, so nothing to do. Exiting the batch.")
		}
		else
		{
			//
			// remember which batch is currently ongoing
			//
			_current_batch = process_batch;

			//
			// run the processes
			//
			for(auto& x: *process_batch)
			{
				LOG(logger(), "Process '" << x.first << "' being started.")
				this->run_subthread(x.second, x.first, &x.second->algo_settings());
			}

			//
			// be a watchdog
			//
		    std::uint32_t counter = 0;
		    std::uint32_t sub_counter = 0;

		    while(should_continue(true, 2000))
		    {
		    	sub_counter++;
		    	if(sub_counter == 30)
		    	{
		    		sub_counter = 0;
		    		counter++;
		            LOG(logger(), "Heartbeat " << counter)
		    	}
		    }
		}
	}



    // 
    // finishing
    //

   	// at this stage all the subprocesses should have ended
   	{
		std::unique_lock<std::mutex> lock(mutex());
		finish();
   	}
}

void PluginDeamon::finish()
{
	// deinitialize the processes
	for(auto& x: _processes)
	{
		for(auto&y: *x)
		{
			y.second->work_deinit();
		}
	}

	for(auto& x: _algorithms)
	{
		x.second->algorithm_deinit();
	}

	for(auto& x: _algorithms)
	{
		x.second->untie();
	}

	for(auto& x: _processes)
	{
		x->clear();
	}
	_algorithms.clear();

    //
    // the program is ending, so mark all plugins for unloading
    //
	for(auto& x: _plugins)
	{
		// plugin found
		x.f_disable(); // disable new allocation from this plugin
		x.waiting_shutdown = true;
	}
}

void PluginDeamon::work_subthread_finished(CatPointer<WorkThread> child)
{
//	std::unique_lock<std::mutex> lock(mutex());

	CatPointer<Process> pr_child(child.cast<Process>());

	for(auto it = _current_batch->begin(); it != _current_batch->end(); ++it)
	{
		if(it->second == pr_child)
		{
			it->second->work_deinit();
			_current_batch->erase(it);
			break;
		}
	}

	if(_current_batch->size() == 0)
		this->self_shutdown();
}

void PluginDeamon::work_init()
{
	// make sure that all the setting branches are there
	if(!settings().has_element("plugins"))
		settings().add_element(new Setting("plugins"));
	if(!settings().has_element("links"))
		settings().add_element(new Setting("links"));
	if(!settings().has_element("algorithms"))
		settings().add_element(new Setting("algorithms"));
	if(!settings().has_element("processes"))
		settings().add_element(new Setting("processes"));

	// loop through the settings and load the plugins
	for(auto& setting: settings()["plugins"])
	{
		if(setting.name() == "plugin")
		{
			std::string setting_path = setting.get_setting<std::string>("settings", "settings/floating");
			try
			{
				// check the settings
				Setting* plug_setting = settings().get_setting_node(setting_path, nullptr);
				if(!plug_setting)
				{
					plug_setting = &settings().create_element(setting_path);
					WARNING(logger(), "Setting '" << setting_path << "' not found. Created now.")	
				}
				load_plugin(setting.get_setting<std::string>("path", ""), setting.get_setting<std::string>("name", ""), plug_setting);
			}
			catch(PluginCatException& ex)
			{
				ERROR(logger(), "Failed loading plugin: " << ex.explain())
			}
		}
	}
}

void PluginDeamon::work_deinit()
{
	// wait for all the plugins to finish
	std::uint32_t counter = 0;

	while(true)
	{
		if(remove_free_plugins() == 0)
		{
			return;
		}
		//
		WARNING(logger(), "Deinit heartbeat " << counter)
		counter++;
		//
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

CatPointer<Algorithm> PluginDeamon::get_algorithm(const std::string& name)
{
	auto it=_algorithms.find(name);
	if(it == _algorithms.end())
	{
		ERROR(logger(), "Algorithm '" << name << "' not found.")
		return CatPointer<Algorithm>(nullptr);
	}
	else
		return it->second;
}

std::uint32_t PluginDeamon::remove_free_plugins()
{
	std::unique_lock<std::mutex> lock(mutex());

	std::uint32_t counter = 0;

	for(auto it = _plugins.begin(); it != _plugins.end();)
	{
		// check if marked for shutdown
		if(!it->waiting_shutdown)
		{
			counter++;
			++it;
			continue;
		}

		// check if all memory pools are empty
		bool empty = true;
		for(auto x: it->m_pools)
		{
			if(x.second->get_elements_now() != 0)
			{
				empty = false;
				break;
			}
		}

		if(!empty)
		{
			counter++;
			++it;
			continue;
		}

		// empty so unload
		INFO(logger(), "Plugin '" << it->name << "' empty.")

		// init the plugin
		if(!it->f_deinit())
		{
			ERROR(logger(), "Failed plugin deinitialization.")
		}
		INFO(logger(), "Plugin '" << it->name << "' deinitialized.")

		// close the handle
		if(dlclose(it->handle))
		{
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		}
		else
		{
			INFO(logger(), "Plugin '" << it->name << "' removed.")
		}

		it = _plugins.erase(it);

	}

	return counter;
}

void PluginDeamon::unload_plugin(const std::string& name)
{
	std::unique_lock<std::mutex> lock(mutex());

	// find the plugin
	for(auto& x: _plugins)
	{
		if(x.name == name)
		{
			// plugin found
			x.f_disable(); // disable new allocation from this plugin
			x.waiting_shutdown = true;
			return;
		}
	}
	throw PluginCatException(__FILE__, __LINE__, "Plugin named '" + name + "' not found.");
}

void PluginDeamon::load_plugin(const std::string& path, const std::string& name, Setting* setting)
{
	std::unique_lock<std::mutex> lock(mutex());

	std::string err_message;
	// check for names and paths
	for(auto& x: _plugins)
	{
		if(x.name == name)
			throw PluginCatException(__FILE__, __LINE__, "Plugin named '" + name + "' already exist.");
		if(x.path == path)
			throw PluginCatException(__FILE__, __LINE__, "Plugin '" + path + "' already loaded.");
	}

	// create a structure
	plugin_t plg;
	plg.name = name;
	plg.path = path+PLUGIN_EXTENTION;
	plg.waiting_shutdown = false;

	// load the plugin
	// try loading it from local path
	plg.handle = dlopen(plg.path.c_str(), RTLD_NOW | RTLD_GLOBAL | RTLD_DEEPBIND);// | RTLD_GLOBAL);
	if(!plg.handle)
	{
		if(path[0] == '/')
		{
			// user specified absoluth path
			err_message = "Plugin failed loading: ";
			err_message += dlerror();
			return throw PluginCatException(__FILE__, __LINE__, err_message);
		}

		// read the system variable
		char* pPath;
		pPath = getenv(SW_SYS_VARIABLE);
		if(pPath)
		{
			std::string syspath(pPath);

			// merge paths
			if(syspath.back() == '/')
				syspath += std::string("lib/") + plg.path;
			else
				syspath += std::string("/lib/") + plg.path;

			plg.handle = dlopen(syspath.c_str(), RTLD_NOW);// | RTLD_GLOBAL);
			if(!plg.handle)
			{
				err_message = "Plugin failed loading: ";
				err_message += dlerror();
				return throw PluginCatException(__FILE__, __LINE__, err_message);
			}
			else
				INFO(logger(), "Plugin '" << plg.name << "' loaded from: " << syspath)
		}
		else
		{
			// user specified absoluth path
			err_message = "Plugin failed loading: ";
			err_message += dlerror();
			return throw PluginCatException(__FILE__, __LINE__, err_message);			
		}
	}
	else
	{
		INFO(logger(), "Plugin '" << plg.name << "' loaded from: " << path)
	}

	// bind the symbols
	plg.f_config = (bool (*)(Setting*, Logger*, const std::string&))dlsym(plg.handle, "global_plugin_config");
	if(!plg.f_config)
	{
		err_message = "Failed loading symbol 'global_plugin_config': ";
		err_message += dlerror();
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}
	plg.f_init = (bool (*)(void))dlsym(plg.handle, "global_plugin_init");
	if(!plg.f_init)
	{
		err_message = "Failed loading symbol 'global_plugin_init': ";
		err_message += dlerror();
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}
	plg.f_deinit = (bool (*)(void))dlsym(plg.handle, "global_plugin_deinit");
	if(!plg.f_deinit)
	{
		err_message = "Failed loading symbol 'global_plugin_deinit': ";
		err_message += dlerror();
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}
	plg.f_get_data_types = (bool (*)(std::set<std::string>&))dlsym(plg.handle, "global_plugin_get_data_types");
	if(!plg.f_get_data_types)
	{
		err_message = "Failed loading symbol 'global_plugin_get_data_types': ";
		err_message += dlerror();
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}
	plg.f_get_memory_pool = (MemoryPoolBase* (*)(const std::string&))dlsym(plg.handle, "global_plugin_get_memory_pool");
	if(!plg.f_get_memory_pool)
	{
		err_message = "Failed loading symbol 'global_plugin_get_memory_pool': ";
		err_message += dlerror();
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}
	plg.f_get_class_manager = (ClassManagerBase* (*)(const std::string&))dlsym(plg.handle, "global_plugin_get_class_manager");
	if(!plg.f_get_class_manager)
	{
		err_message = "Failed loading symbol 'global_plugin_get_class_manager': ";
		err_message += dlerror();
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}
	plg.f_enable = (bool (*)(void))dlsym(plg.handle, "global_plugin_enable");
	if(!plg.f_enable)
	{
		err_message = "Failed loading symbol 'global_plugin_enable': ";
		err_message += dlerror();
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}
	plg.f_disable = (bool (*)(void))dlsym(plg.handle, "global_plugin_disable");
	if(!plg.f_disable)
	{
		err_message = "Failed loading symbol 'global_plugin_disable': ";
		err_message += dlerror();
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}
	INFO(logger(), "Plugin '" << plg.name << "' symbols imported.")

	// config the plugin
	if(!plg.f_config(setting, &logger(), plg.name))
	{
		err_message = "Failed configuring the plugin.";
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}
	INFO(logger(), "Plugin '" << plg.name << "' configured.")

	// init the plugin
	if(!plg.f_init())
	{
		err_message = "Failed plugin initialization.";
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}
	INFO(logger(), "Plugin '" << plg.name << "' initialized.")

	// get the available data types
	std::set<std::string> mem_set;
	if(!plg.f_get_data_types(mem_set))
	{
		err_message = "Failed retrieving list of types.";
		if(dlclose(plg.handle))
			ERROR(logger(), "Failed closing the plugin: " << dlerror())
		return throw PluginCatException(__FILE__, __LINE__, err_message);
	}

	// get all memory pools
	std::vector<MemoryPoolBase*> pools;
	for(auto& x: mem_set)
	{
		MemoryPoolBase* tmp_mem;
		ClassManagerBase* tmp_class;
		tmp_mem = plg.f_get_memory_pool(x);
		tmp_class = plg.f_get_class_manager(x);
		if(!tmp_class)
		{
			err_message = "Failed retrieving class manager for type '" + x + "'. Unregistering those already registered.";
			// unload all the previously loaded memory pools
			for(auto y: pools)
				unregister_memory_pool(y);
			//
			if(dlclose(plg.handle))
				ERROR(logger(), "Failed closing the plugin: " << dlerror())
			return throw PluginCatException(__FILE__, __LINE__, err_message);
		}
		else
		{
			if(register_memory_pool(tmp_mem, tmp_class))
			{
				pools.push_back(tmp_mem);
				INFO(logger(), "Plugin '" << plg.name << "' registered class manager for type '" << tmp_class->memory_cat_name() << "'.")
			}
			else
			{
				INFO(logger(), "Plugin '" << plg.name << "' class manager for type '" << tmp_class->memory_cat_name() << "' will not be used because same type is alredy available.")
			}
		}
	}

	INFO(logger(), "Plugin '" << plg.name << "' memory pools registered.")

	// insert it in the list of memory pools
	_plugins.push_back(plg);
}

bool PluginDeamon::register_memory_pool(MemoryPoolBase* mp, ClassManagerBase* cm)
{
	Base base;
	base.pool = mp;
	base.manager = cm;
	if(!_pool_name.insert(std::pair<std::string, Base>(cm->memory_cat_name(), base)).second)
		return false;
	if(!_pool_type.insert(std::pair<CatType, Base>(cm->memory_cat_type(), base)).second)
		return false;

	// pool obvously inserted
	if(mp)
		mp->init();

	return true;
}

void PluginDeamon::unregister_memory_pool(MemoryPoolBase* mp)
{
	auto it_n = _pool_name.find(mp->memory_cat_name());
	if(it_n != _pool_name.end())
		_pool_name.erase(it_n);

	auto it_t = _pool_type.find(mp->memory_cat_type());
	if(it_t != _pool_type.end())
		_pool_type.erase(it_t);
}

MemoryPoolBase* PluginDeamon::get_memory_pool_private(const std::string& type)
{
	auto it = _pool_name.find(type);
//	if(type == "CatObject")
//	{
//		
//	}
	if(it != _pool_name.end())
		return it->second.pool;
	else
	{
        ERROR(logger(), "Factory for '" << type << "' objects not found.")
		return nullptr;
	}
}

ClassManagerBase* PluginDeamon::get_class_private(const std::string& type)
{
	auto it = _pool_name.find(type);
//	if(type == "CatObject")
//	{
//		
//	}
	if(it != _pool_name.end())
		return it->second.manager;
	else
	{
        ERROR(logger(), "Factory for '" << type << "' objects not found.")
		return nullptr;
	}
}

MemoryPoolBase* PluginDeamon::get_memory_pool(const std::string& type)
{
	std::unique_lock<std::mutex> lock(s_instance->mutex());

	auto it = s_instance->_pool_name.find(type);
	if(it != s_instance->_pool_name.end())
		return it->second.pool;
	else
	{
        ERROR(s_instance->logger(), "Factory for '" << type << "' objects not found.")
		return nullptr;
	}
}

MemoryPoolBase* PluginDeamon::get_memory_pool(CatType type)
{
	std::unique_lock<std::mutex> lock(s_instance->mutex());

	auto it = s_instance->_pool_type.find(type);
	if(it != s_instance->_pool_type.end())
		return it->second.pool;
	else
	{
        ERROR(s_instance->logger(), "Factory for '" << hex << type << dec << "' objects not found.")
		return nullptr;
	}
}

void PluginDeamon::stream_object_out(std::ostream& output, CatPointer<CatObject> object)
{
	StreamSize size;
	CatType type;

	if(object.isSomething())
	{
		size = object->cat_stream_get_size();
		type = object->cat_type();

		// write the size of the object
		output.write((char*)&size, sizeof(StreamSize));
		// write the type
		output.write((char*)&type, sizeof(CatType));
		// write the object
		object->cat_stream_out(output);
	}
	else
	{
		size = 0;
		type = CAT_TYPE_OBJECT;

		// write the size of the object
		output.write((char*)&size, sizeof(StreamSize));
		// write the type
		output.write((char*)&type, sizeof(CatType));
	}
}

CatPointer<CatObject> PluginDeamon::stream_object_in(std::istream& input)
{
	CatPointer<CatObject> object(nullptr);

	StreamSize size;
	CatType type;

    // read the length
    input.read((char*)&size, sizeof(StreamSize));
    // read the type
    input.read((char*)&type, sizeof(CatType));

    if(type == CAT_TYPE_OBJECT)
    {
    	// null pointer
		input.ignore(size);
    	return object;
    }
    else
    {
		// get the instance
		MemoryPoolBase*       pool;

		pool = PluginDeamon::get_memory_pool(type);
		if(pool)
		{
		        object = pool->get_base_element();
		}
		else
		{
		        object = CatPointer<CatObject>(nullptr);
		}

		if(object.isNull())
		{
		        // object could not be instantiated
		        input.ignore(size);
		        return object;
		}
		else
		{
		        // object exists, so read it from the stream
		        object->cat_stream_in(input);
		        return object;
		}

    }
}

bool PluginDeamon::is_derived(const std::string& base, const std:: string& derived)
{
	if(derived == "CatObject" && base != "CatObject")
	{
		return false;
	}
	else if(derived != "CatObject" && base != "CatObject")
	{
		ClassManagerBase*        class_der = get_class_private(derived);
		ClassManagerBase*        class_base = get_class_private(base);
		CatType type_base = class_base->memory_cat_type();

		if(class_der->is_derived(type_base) == false)
		{
			return false;
		}
		else if(class_der->is_derived(type_base) == true)
		{
			return true;
		}
	}
	else if(base == "CatObject"){return true;}
}
} // namespace CatShell
