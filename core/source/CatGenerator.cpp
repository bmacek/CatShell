#include "CatShell/core/CatGenerator.h"
#include "CatShell/core/PluginDeamon.h"

#include <fstream>
#include <iostream>

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatGenerator, CAT_NAME_CATGENERATOR, CAT_TYPE_CATGENERATOR);

CatGenerator::CatGenerator()
{

}

CatGenerator::~CatGenerator()
{
}

void CatGenerator::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_trigger && object.isSomething())
        {
                if(!next_step())
                {
                        flush_data_clients(_out_port_object, logger);
                }
        }

        Algorithm::process_data(port, object, flush, logger);
}

CatShell::CatPointer<CatShell::CatObject> CatGenerator::produce_data(DataPort port, bool& flush, CatShell::Logger& logger)
{
        return CatPointer<CatObject>(nullptr);
}

bool CatGenerator::next_step()
{
        while(_stack.top().element == _stack.top().unit->units.end())
        {
                // reached the end of this unit
                _stack.top().count++;
                if(_stack.top().count == _stack.top().unit->repetition)
                {
                        // reached the end of the repetitions for this element
                        if(_stack.top().unit == &_pattern)
                        {
                                // reached the end of the top node
                                return false;
                        }
                        // remove the level of stack
                        _stack.pop();
                        ++(_stack.top().element);
                        continue;
                }
                _stack.top().element = _stack.top().unit->units.begin();
        }

        // do this element
        while(true)
        {
                if(_stack.top().element->is_value)
                {
                        CatPointer<CatDouble> number;

                        // publish the value
                        number = _pool->get_element();
                        *number = _stack.top().element->value;

                        // publish data
                        publish_data(_out_port_object, number, logger());

                        // go to the next one
                        ++(_stack.top().element);

                        // check if this is the last element
                        while(_stack.top().element == _stack.top().unit->units.end())
                        {
                                // reached the end of this unit
                                _stack.top().count++;
                                if(_stack.top().count == _stack.top().unit->repetition)
                                {
                                        // reached the end of the repetitions for this element
                                        if(_stack.top().unit == &_pattern)
                                        {
                                                // reached the end of the top node
                                                return false;
                                        }
                                        // remove the level of stack
                                        _stack.pop();
                                        ++(_stack.top().element);
                                        continue;
                                }
                                _stack.top().element = _stack.top().unit->units.begin();
                        }
                        return true;
                }
                else
                {
                        // step into it
                        tStackEntry tmp;
                        tmp.unit = (tPattUnit*)_stack.top().element->unit;
                        tmp.element = tmp.unit->units.begin();
                        tmp.count = 0;

                        _stack.push(tmp);
                }                        
        }
}

void CatGenerator::work_main()
{
        bool finish = false;
        while(true)
        {
                if(!should_continue(false))
                        break;

                if(!next_step())
                {
                        flush_data_clients(_out_port_object, logger());
                        break;
                }
        }
}

void CatGenerator::algorithm_init()
{
        Process::algorithm_init();

        _in_port_trigger = declare_input_port("in_trigger", CAT_NAME_OBJECT);
        _out_port_object = declare_output_port("out_number", CAT_NAME_CATDOUBLE);

        // load the settings
        load_pattern(&_pattern, algo_settings()["pattern"]);

        // init the stack
        tStackEntry tmp;
        tmp.unit = &_pattern;
        tmp.element = _pattern.units.begin();
        tmp.count = 0;

        _stack.push(tmp);

        // prepare the memory pool
        _pool = PluginDeamon::get_pool<CatDouble>();
}

void CatGenerator::load_pattern(tPattUnit* unit, CatShell::Setting& s)
{
        // read repetitions
        unit->repetition = s.get_setting<std::uint32_t>("repetition", 1);
        // loop through all the settings
        Setting::iterator it;

        for(it=s.begin(); it != s.end(); ++it)
        {
                if(it->name() == "value")
                {
                        tPattElement tmp;// = new tPattElement;
                        tmp.is_value = true;
                        tmp.value = it->get_value<double>();
                        unit->units.push_back(tmp);
                }
                else if(it->name() == "unit")
                {
                        tPattElement tmp;
                        tmp.is_value = false;
                        tmp.unit = new tPattUnit;
                        load_pattern((tPattUnit*)tmp.unit, *it);
                        unit->units.push_back(tmp);
                }
                //else
                //       cout << it->name() << endl;
        }
}

void CatGenerator::free_pattern(tPattUnit* unit)
{
        for(auto& x: unit->units)
        {
                if(!x.is_value)
                {
                        free_pattern((tPattUnit*)x.unit);
                        delete (tPattUnit*)x.unit;
                        x.unit = nullptr;
                }
        }
        unit->units.clear();
}

} // namespace CatShell
