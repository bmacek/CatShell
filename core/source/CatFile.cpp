#include "CatShell/core/CatFile.h"
#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CatFile::CatFile()
: _file(nullptr)
{
}

CatFile::~CatFile()
{
}

 
bool CatFile::open_file(const std::string& name, bool read, bool write)
{
	std::ios_base::openmode flags = std::fstream::binary;
	if(read)
		flags |= std::fstream::in;
	if(write)
		flags |= std::fstream::out;
        _file = new fstream(name, flags);
        if(!_file->is_open())
        {
                delete _file;
                _file = nullptr;
                return false;
        }
        else
        {
                _file->exceptions( std::ios::failbit | std::ios::badbit  | std::ios::eofbit );
                _eof = false;
                return true;
        }
}

void CatFile::close_file()
{
        if(_file)
        {
                _file->close();
                delete _file;
                _file = nullptr;
        }
}

CatPointer<CatObject> CatFile::get_next_object() const
{
        StreamSize            size;
        CatType               type;
        MemoryPoolBase*       pool;
        CatPointer<CatObject> object;

        // read the length
        _file->read((char*)&size, sizeof(StreamSize));
        // read the type
        _file->read((char*)&type, sizeof(CatType));

       try
        {

                // get the instance
                pool = PluginDeamon::get_memory_pool(type);
                if(pool)
                {
                        object = pool->get_base_element();
                }
                else
                {
                        object = CatPointer<CatObject>(nullptr);
                }

                if(object.isNull())
                {
                        // object could not be instantiated
                        _file->ignore(size);
                }
                else
                {
                        // object exists, so read it from the stream
                        object->cat_stream_in(*_file);
                }                                                

                return object;

        }
        catch(std::bad_alloc e)
        {
                object.make_nullptr();
                return object;
        }
        catch(std::ios::failure e)
        {
                // return object
                _eof = true;

                object.make_nullptr();
                return object;
        }
}

} // namespace CatShell
