#include "CatShell/core/CatBool.h"
#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatBool, CAT_NAME_CATBOOL, CAT_TYPE_CATBOOL);

void CatBool::cat_stream_out(std::ostream& output) 
{ 
	std::uint8_t tmp;
	tmp = (_data ? 0xFF : 0x00);
	output.write((char*)&tmp, sizeof(std::uint8_t));
}

void CatBool::cat_stream_in(std::istream& input)
{ 
	std::uint8_t tmp;
	input.read((char*)&tmp, sizeof(std::uint8_t));
	_data = (tmp == 0xFF);
}

void* CatBool::cat_get_part(CatShell::CatAddress& addr)
{
	if(addr == "value")
	{
		return &_data;
	}
	else
		return nullptr;
}

} // namespace CatShell
