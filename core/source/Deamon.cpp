#include "CatShell/core/Deamon.h"
#include "CatShell/core/CatException.h"

#include <signal.h>

using namespace std;

namespace CatShell {

// static

Deamon* Deamon::_stat_app = nullptr;

CAT_OBJECT_IMPLEMENT(Deamon, "CatDeamon", CAT_TYPE_DEAMON)

void Deamon::sysShutdown(int signum)
{
        Deamon::_stat_app->shutdown();
}

//

Deamon::Deamon()
{
        Deamon::_stat_app = this;
}

Deamon::~Deamon()
{
}

void Deamon::work_main()
{
        ERROR(logger(), "Empty App!")
}

void Deamon::work_init()
{
}

void Deamon::work_deinit()
{
}

void Deamon::set_user_parameter(std::string key, std::string value)
{
        map<std::string, std::string>::iterator it;
        it=_params.find(key);
       
        if(it != _params.end())
                it->second = value;
        else
                _params.insert( pair<std::string, std::string>(key, value));
}

void Deamon::set_user_flag(std::string flag)
{
        if(!has_user_flag(flag))
                _flags.push_back(flag);
}

bool Deamon::has_user_flag(std::string flag)
{
        std::vector<std::string>::iterator it;
        for(it = _flags.begin(); it!=_flags.end(); it++)
        {
                if(*it == flag)
                        return true;
        }
        return false;
}

std::string Deamon::get_user_parameter(std::string key)
{
        map<std::string, std::string>::iterator it;
        it=_params.find(key);
       
        if(it != _params.end())
                return it->second;
        else
                throw SettingCatException(__FILE__, __LINE__, "Key not found!");
}

int Deamon::run(int argc, char** argv, std::string programName, std::string programVersion)
{
        //
        // parse the input parameters
        //
        std::vector<std::string> params;

        bool is_setting = false;
        std::string setting_flag;
        _params.clear();
        _flags.clear();
        SettingParameters setting_params;
        std::uint32_t setting_parameter_count = 0;

        for(int i=1; i<argc; i++)
        {
                std::string tmp(argv[i]);
                if(tmp[0] == '-')
                {
                        if(is_setting)
                        {
                                // the previous one was actually a flag
                                _flags.push_back(setting_flag);
                        }
                        else
                        {
                                // this is the value of the setting
                                _params.insert( pair<std::string, std::string>(setting_flag, tmp));
                        }

                        is_setting = true;
                        setting_flag = tmp;
                }
                else
                {
                        if(is_setting)
                        {
                                _params.insert( pair<std::string, std::string>(setting_flag, tmp));
                        }
                        else
                        {
                                setting_params.insert_substitution(setting_parameter_count, tmp);
                                ++setting_parameter_count;
                        }

                        is_setting = false;
                }
        }
        if(is_setting)
        {
                // the previous one was actually a flag
                _flags.push_back(setting_flag);
        }

        //
        // check if help was reqested
        //

        if(has_user_flag("-h"))
        {
                // help should be displayed
                cout    << "Usage: " << argv[0] << endl
                        << "  -h         display thi help," << endl
                        << "  -s <file>  use <file> instead of default setting file (" << argv[0] << ".xml)" << endl;
                return 0;
        }

        //
        // load the settings
        //

        // check if there was setting file specified
        std::string setting_fileName;
        std::string setting_extention;

        Setting* settings;

        try
        {
                setting_fileName = get_user_parameter("-s");

                size_t n = setting_fileName.find_last_of('.');
                if(n == string::npos)
                {
                        cout << "Setting file without extention!" << endl;
                        return 0;
                }
                setting_extention = setting_fileName.substr(n);
                setting_fileName = setting_fileName.substr(0, n);

        }
        catch(SettingCatException& ex)
        {
                setting_fileName = programName;
                setting_extention = ".xml";
        }

        try
        {
                settings = SettingParser::settings_from_xml(setting_fileName, setting_extention, setting_params);
        }
        catch(SettingCatException& ex)
        {
                if(setting_fileName[0] == '/')
                {
                        cout << endl << endl << "Fatal error while initial load of settings: " << endl << ex.explain() << endl;
                        cout << "Do you need to set system variable '" << SW_SYS_VARIABLE << "' ?" << endl;
                        return 0;
                }

                // read the system variable
                char* pPath;
                pPath = getenv(SW_SYS_VARIABLE);
                if(pPath)
                {
                        std::string syspath(pPath);

                        // merge paths
                        if(syspath.back() == '/')
                                syspath += std::string("config/") + setting_fileName;
                        else
                                syspath += std::string("/config/") + setting_fileName;

                        try
                        {
                                settings = SettingParser::settings_from_xml(syspath, setting_extention, setting_params);
                        }
                        catch(SettingCatException& ex)
                        {
                                cout << endl << endl << "Fatal error while initial load of settings: " << endl << ex.explain() << endl;
                                return 0;
                        }
                }
                else
                {
                        cout << endl << endl << "Fatal error while initial load of settings: " << endl << ex.explain() << endl;
                        cout << "Do you need to set system variable '" << SW_SYS_VARIABLE << "' ?" << endl;
                        return 0;
                }
        }

        //
        // logging infrastructure
        //

        std::uint32_t logging_n_files = settings->get_setting<std::uint32_t>("logging/file_number", 10);
        std::uint32_t logging_file_volume = settings->get_setting<std::uint32_t>("logging/file_size", 10*1024*1024);
        std::string   logging_file_name = settings->get_setting<std::string>("logging/path", programName);

        FileLogger logger(programName, logging_file_name, logging_n_files, logging_file_volume);

        START_LOG_LOG(logger)
        LINE_LOG_LOG(logger, "******")
        logger.stream() << logger.padding() << "***********" << endl
                        << logger.padding() <<  "****************" << endl
                        << logger.padding() <<  "*********************" << endl
                        << logger.padding() <<  "**************************" << endl
                        << logger.padding() <<  "*  Starting application  *" << endl
                        << logger.padding() <<  "**************************" << endl;
        END_LOG(logger)

        // print settngs
        START_LOG_INFO(logger)
        LINE_LOG_LOG(logger, "Initial setting:")
        settings->print(logger.stream(), logger.padding());
        END_LOG(logger)

        //
        // initialize
        //

        die_with_no_children();
        work_init_private(0, nullptr, logger, programName, settings);

        signal(SIGTERM, Deamon::sysShutdown);
        signal(SIGINT, Deamon::sysShutdown);

        // 
        // work
        // 
        
        WorkThread::run(); // starts the working process

        // wait for the end of the process or the kill signal
        this->wait_end();


        START_LOG_LOG(logger)
        LINE_LOG_LOG(logger, "**************************")
        logger.stream() << logger.padding() <<  "*   Ending application   *" << endl
                        << logger.padding() <<  "**************************" << endl
                        << logger.padding() <<  "*********************" << endl
                        << logger.padding() <<  "****************" << endl
                        << logger.padding() <<  "***********" << endl
                        << logger.padding() <<  "******" << endl;
        END_LOG(logger)

        //
        // cleanup
        //

        delete settings;
        settings = nullptr;

        //
        // end
        //
        return 0;
}

void Deamon::work_subthread_finished(CatPointer<WorkThread> child)
{

}

} // namespace CatShell
