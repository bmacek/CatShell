#include "CatShell/core/CatCounter.h"
#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatCounter, CAT_NAME_CATCOUNTER, CAT_TYPE_CATCOUNTER);


CatCounter::CatCounter()
{

}

CatCounter::~CatCounter()
{

}

void CatCounter::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
  if(port == _in_port_object && object.isSomething())
  {
    publish_data(_out_port_count, _count, logger);
    ++(*_count);
  }

  Algorithm::process_data(port, object, flush, logger);
}

void CatCounter::algorithm_init()
{
        Algorithm::algorithm_init();

        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);
        _out_port_count = declare_output_port("out_count", CAT_NAME_CATUINT32);

        _count = PluginDeamon::get_pool<CatUint32>()->get_element();
        (*_count) = 0;
}

void CatCounter::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
}

} // namespace CatShell
