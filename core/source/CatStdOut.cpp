#include "CatShell/core/CatStdOut.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatStdOut, CAT_NAME_CATSTDOUT, CAT_TYPE_CATSTDOUT);


CatStdOut::CatStdOut()
{

}

CatStdOut::~CatStdOut()
{

}

void CatStdOut::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_object)
        {
                StreamSize size = object->cat_stream_get_size();
                CatType type = object->cat_type();
                
                // write the size of the object
                std::cout.write((char*)&size, sizeof(StreamSize));
                // write the type
                std::cout.write((char*)&type, sizeof(CatType));
                // write the object
                object->cat_stream_out(std::cout);
        }
}

void CatStdOut::algorithm_init()
{
        Algorithm::algorithm_init();

        _in_port_object = declare_input_port("in_object", CAT_NAME_OBJECT);
}

void CatStdOut::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
}

} // namespace CatShell
