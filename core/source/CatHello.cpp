#include "CatShell/core/CatHello.h"
#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatHello, CAT_NAME_CATHELLO, CAT_TYPE_CATHELLO);


CatHello::CatHello()
{

}

CatHello::~CatHello()
{
}

void CatHello::work_main()
{
        cout    << "Welcome to ---->   " << endl
                << endl
                << " ####      ##   ########    ####  ##  ##  #####  ##     ##" << endl
                << "##  ##    ####     ##      ##     ##  ##  ##     ##     ##" << endl
                << "##       ##  ##    ##      ####   ######  ####   ##     ##" << endl
                << "##  ##  ########   ##         ##  ##  ##  ##     ##     ##" << endl
                << " ####  ##      ##  ##      ####   ##  ##  #####  #####  ########" << endl
                << "                                                               ##" << endl
                << " ###############################################################" << endl
                << "##" << endl
                << " #######>>>>>>>>>>> by BOSTJAN MACEK"
                << endl
                << endl
                << "Version: " << SW_BASE_VERSION << endl;

        // read the system variable
        char* pPath;
        pPath = getenv(SW_SYS_VARIABLE);
        if(pPath)
                cout    << "Instalation path: " << pPath << endl;
        else
                cout    << "WARNING: envaromental variable '" << SW_SYS_VARIABLE << "' not set." << endl;

        pPath = getenv(SW_LOG_VARIABLE);
        if(pPath)
                cout    << "Log file path: " << pPath << endl;
        else
                cout    << "WARNING: envaromental variable '" << SW_LOG_VARIABLE << "' not set." << endl;

        cout    << endl
                << endl
                << "Hope you'll have fun ..." << endl;

        LOG(logger(), "CatHello is saying 'Hi'.")
}

void CatHello::work_init()
{
        Process::work_init();
}

void CatHello::work_deinit()
{
        Process::work_deinit();
}

void CatHello::algorithm_init()
{
        Process::algorithm_init();
}

void CatHello::algorithm_deinit()
{
        Process::algorithm_deinit();
}

} // namespace CatShell
