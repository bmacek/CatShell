#include "CatShell/core/Plugin.h"
#include "CatShell/core/CatPrint.h"
#include "CatShell/core/CatStdIn.h"
#include "CatShell/core/CatStdOut.h"
#include "CatShell/core/CatFileIn.h"
#include "CatShell/core/CatFileOut.h"
#include "CatShell/core/CatHello.h"
#include "CatShell/core/CatCollection.h"
#include "CatShell/core/CatPartPrint.h"
#include "CatShell/core/CatLoop.h"
#include "CatShell/core/CatDouble.h"
#include "CatShell/core/CatUint32.h"
#include "CatShell/core/CatGenerator.h"
#include "CatShell/core/CatPoisson.h"
#include "CatShell/core/CatBool.h"
#include "CatShell/core/CatThreshold.h"
#include "CatShell/core/CatSort.h"
#include "CatShell/core/CatCondition.h"
#include "CatShell/core/CatCounter.h"

using namespace std;

namespace CatShell {

EXPORT_PLUGIN(Plugin, CatCorePlugin)
START_EXPORT_CLASSES(CatCorePlugin)
EXPORT_CLASS(CatHello, CAT_NAME_CATHELLO, CatCorePlugin)
EXPORT_CLASS(CatPrint, CAT_NAME_CATPRINT, CatCorePlugin)
EXPORT_CLASS(CatStdIn, CAT_NAME_CATSTDIN, CatCorePlugin)
EXPORT_CLASS(CatStdOut, CAT_NAME_CATSTDOUT, CatCorePlugin)
EXPORT_CLASS(CatFileIn, CAT_NAME_CATFILEIN, CatCorePlugin)
EXPORT_CLASS(CatCollection, CAT_NAME_CATCOLLECTION, CatCorePlugin)
EXPORT_CLASS(CatPartPrint, CAT_NAME_CATPARTPRINT, CatCorePlugin)
EXPORT_CLASS(CatLoop, CAT_NAME_CATLOOP, CatCorePlugin)
EXPORT_CLASS(CatDouble, CAT_NAME_CATDOUBLE, CatCorePlugin)
EXPORT_CLASS(CatUint32, CAT_NAME_CATUINT32, CatCorePlugin)
EXPORT_CLASS(CatGenerator, CAT_NAME_CATGENERATOR, CatCorePlugin)
EXPORT_CLASS(CatPoisson, CAT_NAME_CATPOISSON, CatCorePlugin)
EXPORT_CLASS(CatBool, CAT_NAME_CATBOOL, CatCorePlugin)
EXPORT_CLASS(CatThreshold, CAT_NAME_CATTHRESHOLD, CatCorePlugin)
EXPORT_CLASS(CatFileOut, CAT_NAME_CATFILEOUT, CatCorePlugin)
EXPORT_CLASS(CatSort, CAT_NAME_CATSORT, CatCorePlugin)
EXPORT_CLASS(CatCondition, CAT_NAME_CATCONDITION, CatCorePlugin)
EXPORT_CLASS(CatCounter, CAT_NAME_CATCOUNTER, CatCorePlugin)
END_EXPORT_CLASSES(CatCorePlugin)

} // namespace CatShell
