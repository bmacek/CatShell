#include "CatShell/core/CatAddress.h"

#include <sstream>
#include <cstdlib>

using namespace std;

#define FLAG_NO_INDEX (-1)

namespace CatShell {

CatAddress::CatAddress()
{
	_it = _levels.end();
}

CatAddress::~CatAddress()
{

}

void CatAddress::operator=(const std::string& text)
{
	// split the string into tokens
	std::istringstream iss(text);
	std::string s;    

	while (getline(iss, s, '/'))
	{
		// have the substring
		// check if index is present
		size_t p0 = s.find("[");
		if(p0 == string::npos)
		{
			// no index is present
			_levels.emplace_back(s, FLAG_NO_INDEX);
		}
		else
		{
			size_t p1 = s.find("]", p0);
			if(p1 == string::npos)
			{
				// error in index parsing
				_levels.emplace_back(s, FLAG_NO_INDEX);
			}
			else
			{
				// insert it with the index
				_levels.emplace_back(s.substr(0, p0), atoi(s.substr(p0+1, p1-p0-1).c_str()));
			}
		}
	}

	_it = _levels.begin();
}

void CatAddress::print(std::ostream& output)
{
	for(auto& x: _levels)
	{
		output << "/" << x.name;
		if(x.index != FLAG_NO_INDEX)
			output << "[" << x.index << "]";
	}
}

bool CatAddress::operator==(const std::string& name) const
{
	if(_it != _levels.end())
	{
		return _it->name == name;
	}
	else
		return false;
}

bool CatAddress::has_index() const
{
	if(_it != _levels.end())
		return _it->index != FLAG_NO_INDEX;
	else
		return false;
}

std::int32_t CatAddress::get_index() const
{
	if(_it != _levels.end())
		return _it->index;
	else
		return false;
}

} // namespace CatShell
