#include "CatShell/core/defines.h"
#include "CatShell/core/CatException.h"

namespace CatShell {

CatException::CatException()
{
}

CatException::CatException(const std::string& file, unsigned int line, const std::string& what_arg)
: _file(file), _line(line), _message(what_arg)
{
}

CatException::CatException(const std::string& file, unsigned int line, const char* what_arg)
: _file(file), _line(line), _message(what_arg)
{
}

CatException::~CatException()
{
}

const char* CatException::what() const noexcept
{
        return "CatException";
}

const std::string& CatException::message() const
{
        return _message;
}

std::string CatException::explain() const
{
        char text[256];
        sprintf(text, "%s [%s, %s:%u]: %s", this->what(), SW_BASE_VERSION, _file.c_str(), _line, _message.c_str());
        return text;
}

BASE_IMPLEMENT_EXCEPTION(SettingCatException, CatException, "SettingCatException")
BASE_IMPLEMENT_EXCEPTION(NullCatException, CatException, "NullCatException")
BASE_IMPLEMENT_EXCEPTION(OutOfMemoryCatException, CatException, "OutOfMemoryCatException")
BASE_IMPLEMENT_EXCEPTION(PluginCatException, CatException, "PluginCatException")

} // namespace CatShell
