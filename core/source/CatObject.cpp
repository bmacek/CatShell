#include "CatShell/core/CatObject.h"
#include "CatShell/core/MemoryPool.h"

using namespace std;

namespace CatShell {

void CatObject::reference_minus()
{
        if(_ref_count.fetch_sub(1) == 1)
        {
        	this->cat_memory_pool()->return_element(this);
        }
}

} // namespace CatShell
