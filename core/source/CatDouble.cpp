#include "CatShell/core/CatDouble.h"
#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatDouble, CAT_NAME_CATDOUBLE, CAT_TYPE_CATDOUBLE);

void* CatDouble::cat_get_part(CatShell::CatAddress& addr)
{
	if(addr == "value")
	{
		return &_data;
	}
	else
		return nullptr;
}

} // namespace CatShell
