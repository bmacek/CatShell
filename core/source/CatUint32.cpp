#include "CatShell/core/CatUint32.h"
#include "CatShell/core/PluginDeamon.h"

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatUint32, CAT_NAME_CATUINT32, CAT_TYPE_CATUINT32);

void* CatUint32::cat_get_part(CatShell::CatAddress& addr)
{
	if(addr == "value")
	{
		return &_data;
	}
	else
		return nullptr;
}

CatUint32& CatUint32::operator++()
{
  ++_data;
  return *this;
}
CatUint32& CatUint32::operator--()
{
  --_data;
  return *this;
}

} // namespace CatShell
