#include "CatShell/core/PluginDeamon.h"
#include "CatShell/core/BaseException.h"

#include <signal.h>

using namespace std;

namespace CatShell {

//

PluginDeamon::PluginDeamon()
{
}

PluginDeamon::~PluginDeamon()
{
}

void PluginDeamon::work_main()
{
        ERROR(logger(), "Empty Deamon App!")
}

void PluginDeamon::work_init()
{
}

void PluginDeamon::work_deinit()
{
}

} // namespace CatShell
