#include "CatShell/core/CatElement.h"

using namespace std;

namespace CatShell {

CatVirtualElement* CatVirtualElement::create_element(Setting& setting)
{
    std::string type = setting.get_setting<std::string>("type", "?");

    if(type.substr(0,12) == "std::uint8_t")// test if it starts with ...
        return new CatElement<std::uint8_t>(setting);
    else if(type.substr(0,13) == "std::uint16_t")
        return new CatElement<std::uint16_t>(setting);
    else if(type.substr(0,13) == "std::uint32_t")
        return new CatElement<std::uint32_t>(setting);
    else if(type.substr(0,6) == "double")
        return new CatElement<double>(setting);
    else if(type.substr(0,5) == "float")
        return new CatElement<float>(setting);
   else if(type.substr(0,4) == "bool")
        return new CatElement<bool>(setting);
    else
        return nullptr;
}

} // namespace CatShell
