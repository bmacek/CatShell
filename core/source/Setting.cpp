#include "CatShell/core/Setting.h"

#include <iostream>

using namespace std;
using namespace tinyxml2;

namespace CatShell {

//
// Setting
//

Setting::Setting(const std::string& name, Setting* parenth)
: _name(name), _parent(parenth)
{

}

Setting::~Setting()
{
}

void Setting::register_call_back(std::function<bool (Setting*)> cb)
{
	_callbacks.push_back(cb);
}

void Setting::update()
{
	auto it = _callbacks.begin();
	while(it != _callbacks.end())
	{
		if(!((*it)(this)))
		{
			// callback should be removed
			it = _callbacks.erase(it);
		}
		else
			++it;
	}

	if(_parent)
		_parent->update();
}

std::string Setting::name_full() const
{
	if(_parent)
		return _parent->name_full() + "/" + _name;
	else
		return std::string("/") + _name;
}

Setting& Setting::operator[](const std::string& key)
{
	for(auto& x: _children)
		if(x.name == key)
		{
			return *(x.setting);
		}

	// setting was not found
	throw SettingCatException(__FILE__, __LINE__, std::string("Setting not found: ") + this->name_full() + "/" + key);
}

bool Setting::add_element(Setting* setting)
{
	_children.emplace_back(setting_entry_t{setting->name(), setting});
	setting->_parent = this;
	return true;
}

bool Setting::has_element(const std::string& key) const
{
	for(auto& x: _children)
		if(x.name == key)
			return true;
	return false;
}

void Setting::print(std::ostream& output, const std::string& padding)
{
	output << padding << name() << ":" << endl;
	for(auto& x: _children)
		x.setting->print(output, padding + "   ");
}

Setting* Setting::get_setting_if(const std::string& name, std::function<bool (Setting*)> condition)
{
	Setting* setting(this);

	// split name into path and name
	std::string s_path("");
	std::string s_name(name);

	std::size_t found = name.find_last_of("/\\");
	if(found != string::npos)
	{
		s_path = name.substr(0,found);
		s_name = name.substr(found+1);
		setting = get_setting<Setting*>(s_path, nullptr);
		if(!setting)
			return nullptr;
	}

	for(auto& x: setting->_children)
	{
		if(x.name == s_name)
		{
			// the name is correct
			if(condition(x.setting))
				return x.setting;
		}
	}

	// non met the condition
	return nullptr;
}

Setting* Setting::get_setting_node(const std::string& name, Setting* def)
{
	// split the string into tokens
	std::istringstream iss(name);
	std::string s;

	Setting* sett = this;
	Setting* sett_next = nullptr;

	while (getline(iss, s, '/'))
	{
		// find the sub-setting
		sett_next = nullptr;
		for(auto& x: sett->_children)
		{
			if(x.name == s)
			{
				sett_next = x.setting;
				break;
			}
		}

		if(nullptr)
		{
			// sub-setting not found
			return def;
		}
		else
			sett = sett_next;
	}

	return sett;
}

Setting& Setting::create_element(const std::string& name)
{
	// split the string into tokens
	std::istringstream iss(name);
	std::string s;

	Setting* sett = this;
	Setting* sett_next = nullptr;

	while (getline(iss, s, '/'))
	{
		// find the sub-setting
		sett_next = nullptr;
		for(auto& x: sett->_children)
		{
			if(x.name == s)
			{
				sett_next = x.setting;
				break;
			}
		}

		if(nullptr)
		{
			// sub-setting not found
			sett = new Setting(s);
			add_element(sett);
		}
		else
		{
			sett = sett_next;
		}
	}

	return *sett;
}


//
// SettingParser
//

Setting* SettingParser::settings_from_xml(const std::string& name, const std::string& extension, SettingParameters& parameters)
{
    XMLDocument doc;
    if(doc.LoadFile((name+extension).c_str()) != XML_NO_ERROR)
    {
    	// error loading the file
    	throw SettingCatException(__FILE__, __LINE__, std::string("No setting file found: ") + name + extension);
    }

	XMLElement* root = doc.FirstChildElement("configuration");//name.c_str());

	return xml_2_settings(root, parameters);
}

Setting* SettingParser::xml_2_settings(XMLElement* element, SettingParameters& parameters)
{
	if(element->Attribute("type") && (strcmp(element->Attribute("type"), "double") == 0))
	{
		// double
		return new ValueSetting<double>(element->Name(), (double)atof(parameters.transform_setting(element->GetText()).c_str()));
	}
	if(element->Attribute("type") && (strcmp(element->Attribute("type"), "std::uint8_t") == 0))
	{
		// std::uint8_t
		return new ValueSetting<std::uint8_t>(element->Name(), (std::uint8_t)atoi(parameters.transform_setting(element->GetText()).c_str()));
	}
	if(element->Attribute("type") && (strcmp(element->Attribute("type"), "std::uint16_t") == 0))
	{
		// std::uint16_t
		return new ValueSetting<std::uint16_t>(element->Name(), (std::uint16_t)atoi(parameters.transform_setting(element->GetText()).c_str()));
	}
	if(element->Attribute("type") && (strcmp(element->Attribute("type"), "std::uint32_t") == 0))
	{
		// std::uint32_t
		return new ValueSetting<std::uint32_t>(element->Name(), (std::uint32_t)atoi(parameters.transform_setting(element->GetText()).c_str()));
	}
	if(element->Attribute("type") && (strcmp(element->Attribute("type"), "std::uint64_t") == 0))
	{
		// std::uint64_t
		return new ValueSetting<std::uint64_t>(element->Name(), (std::uint64_t)atoi(parameters.transform_setting(element->GetText()).c_str()));
	}
	if(element->Attribute("type") && (strcmp(element->Attribute("type"), "bool") == 0))
	{
		// bool
		std::string val = parameters.transform_setting(element->GetText());
		if((val == "true") || (val == "True") || (val == "TRUE"))
			return new ValueSetting<bool>(element->Name(), true);
		else
			return new ValueSetting<bool>(element->Name(), false);
	}
	if(element->Attribute("type") && (strcmp(element->Attribute("type"), "std::string") == 0))
	{
		// std::string
		return new ValueSetting<std::string>(element->Name(), parameters.transform_setting(element->GetText()).c_str());
	}
	else if(!element->Attribute("type"))
	{
		// this is group
		Setting* s = new Setting(element->Name());

		for(XMLElement* e = element->FirstChildElement(); e != nullptr; e = e->NextSiblingElement())
		{
			s->add_element(xml_2_settings(e, parameters));
		}
		return s;
	}

	throw SettingCatException(__FILE__, __LINE__, std::string("Unrecognised setting type: ") + element->Attribute("type"));

	return nullptr;
}

//
// SettingParameters
//

SettingParameters::SettingParameters()
{

}

SettingParameters::~SettingParameters()
{

}

void SettingParameters::insert_substitution(std::uint32_t serial, std::string value)
{
	auto response = _substitutions.emplace(serial, value);
	if(response.second)
	{
		//cout << "Substitution rule inserted: " << serial << " -> " << value << endl;
	}
	else
	{
		// element not inserted
		//cout << "Substitution rule not inserted, this index already exists." << endl;
	}
}

std::string SettingParameters::transform_setting(const std::string& original)
{
	std::size_t start = 0;
	std::size_t location = 0;
	std::string result = "";
	while(true)
	{
		location = original.find("$", start);
		if(location == std::string::npos)
		{
			result += original.substr(start, location-start);
			break;
		}
		else
		{
			// found something to be substituted
			std::size_t loc_delimiter_1, loc_delimiter_2;
			std::size_t loc_end;
			loc_delimiter_1 = original.find(",", location+1);
			loc_delimiter_2 = original.find(",", loc_delimiter_1+1);
			loc_end = original.find("$", loc_delimiter_2+1);
			if(loc_delimiter_1 == std::string::npos || loc_delimiter_2 == std::string::npos || loc_end == std::string::npos)
			{
				// format error, do nothing
				return original;
			}

			result += original.substr(start, location-start);

			// find the substitutuin orders
			std::string in_parameter;
			std::string in_system;
			std::string in_default;

			in_parameter = original.substr(location+1, loc_delimiter_1-location-1);
			in_system = original.substr(loc_delimiter_1+1, loc_delimiter_2-loc_delimiter_1-1);
			in_default = original.substr(loc_delimiter_2+1, loc_end-loc_delimiter_2-1);

			bool donext = true;
			// try the input parameter first
			if(in_parameter != "")
			{
				// there is an input paramater - it has priority
				std::uint32_t serial = (std::uint32_t)atoi(in_parameter.c_str());
				auto r = _substitutions.find(serial);
				if(r == _substitutions.end())
				{
					// no such substitution found -> use the default
					donext = true;
				}
				else
				{
					// use the substitution
					result += r->second;
					donext = false;
				}
			}

			// try the system variable next
			if(donext && in_system != "")
			{
				// check if SYS VARIABLE or CONSOLE COMMAND
				if(in_system.front() == '`' && in_system.back() == '`')
				{
					// command
					in_system = in_system.substr(1, in_system.size()-2);

					std::array<char, 128> buffer;
					std::string ret;

					FILE* pipe = popen(in_system.c_str(), "r");
					if(pipe)
					{
						while (fgets(buffer.data(), 128, pipe) != NULL)
						{
							ret += buffer.data();
						}
						auto returnCode = pclose(pipe);

						if(returnCode == 0)
						{
							// do the substitution
							ret = ret.substr(0, ret.size()-1); // !! it assumes new-line at the end
							result += ret;

							donext = false;
						}
						else
							donext = true;
					}
					else
					{
						// could not start the command
						donext = true;
					}
				}
				else
				{
					// variable
					char* pVar;
					pVar = getenv(in_system.c_str());
					if(pVar)
					{
						// system variable present -> do the substitution
						result += pVar;
						donext = false;
					}
					else
					{
						// system variable not found
						donext = true;
					}
				}
			}

			// do the default
			if(donext)
			{
				result += in_default;
			}

			location = loc_end+1;
		}

		start = location;
	}

//	if(result != original)
//	{
//			cout << "Substituted: " << original << " -> " << result << endl;
//	}

	return result;
}

} // namespace CatShell
