#include "CatShell/core/CatService.h"

using namespace std;

namespace CatShell {

void CatService::write7bitEncoded(std::ostream& output, std::uint32_t value)
{
        do
        {
                std::uint8_t c = (std::uint8_t) (value & 0x7F);
                value >>= 7;
                if (value)
                        c |= 0x80;
                output.write((char*) &c, sizeof(std::uint8_t));
        }
        while (value);
}

std::uint32_t CatService::read7bitEncoded(std::istream& input)
{
        int s = 0;
        std::uint8_t c;
        std::uint32_t tmp, value = 0;
        do
        {
                c = 0;
                input.read((char*)&c, sizeof(std::uint8_t));
                tmp = (c & 0x7F);
                tmp <<= s;
                value += tmp;
                s += 7;
        }
        while (c & 0x80);
        return value;
}

std::uint32_t CatService::lengtOf7bitEncoded(std::uint32_t value)
{
        if(value <= 0x7f) return 1;
        else if(value <= 0x3fff) return 2;
        else if(value <= 0x1fffff) return 3;
        else if(value <= 0xfffffff) return 4;
        else return 5;
}

std::uint32_t CatService::reverse_bits(std::uint32_t x)
{
        std::uint32_t result = 0;

        for(std::uint8_t i=0; i<31; ++i)
        {
                if(x & 0x1)
                        result |= 0x1;
                x = x >> 1;
                result = result << 1;
        }
        if(x & 0x1)
                result |= 0x1;
        return result;
}



} // namespace CatShell
