#include "CatShell/core/LimitedFile.h"

#include <stdexcept>

using namespace std;

namespace CatShell {

LimitedFile::LimitedFile(const std::string& name, const std::string& extension, std::ios::openmode openMode, std::uint32_t sizeLimit, bool cyclic, std::uint32_t cyclic_top, bool time_stamp, std::uint32_t startFileNumber)
: _sizeLimit(sizeLimit), _fileNumber(startFileNumber), _name(name), _extension(extension), _currentName(""), _openMode(openMode), _cyclic(cyclic), _cycle_top(cyclic_top)
{
	// remember
	_openMode = openMode;

	// construct the prename
	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	if(time_stamp)
		{
		char buffer [80];
		strftime (buffer,80,"%Y%m%d%H%M%S",timeinfo);
		_name += ".";
		_name += buffer;
	}

	// get the current name
	setNewCurrentFileName(false);
	// _file.exceptions ( ifstream::failbit | ifstream::badbit );

	// open the file     
	_file = new std::ofstream;
	if(_cyclic)
		_file->open((_currentName + ".tmp").c_str(), _openMode | ios_base::trunc);
	else
		_file->open((_currentName + ".tmp").c_str(), _openMode);
	if(!_file->is_open())
	{
		char text[100];
		sprintf(text, "Failed to open file '%s'.", (_currentName + ".tmp").c_str());
		throw std::runtime_error(text);
	}
}

ostream& LimitedFile::flush()
{
	_file->flush();

	//check if over the limit
	if(_file->tellp() > _sizeLimit)
	{
		_file->close();

		// rename the file
		if(rename((_currentName + ".tmp").c_str(), _currentName.c_str()))
		{
			char text[500];
			sprintf(text, "Failed to rename (when migrating) the file '%s' -> '%s'.", (_currentName + ".tmp").c_str(), _currentName.c_str());
			throw std::runtime_error(text);
		}

		// create a new file
		setNewCurrentFileName();
		delete _file;
		_file = new std::ofstream;
		if(_cyclic)
		_file->open((_currentName + ".tmp").c_str(), _openMode | ios_base::trunc);
		else
		_file->open((_currentName + ".tmp").c_str(), _openMode);
		if(!_file->is_open())
		{
			char text[100];
			sprintf(text, "Failed to open file '%s'.", (_currentName + ".tmp").c_str());
			throw std::runtime_error(text);
		}
	}

	return *_file;
}

void LimitedFile::setNewCurrentFileName(bool increaseFileNumber)
{
	if(increaseFileNumber)
	{
		_fileNumber++;
		if(_cyclic && _fileNumber >= _cycle_top)
			_fileNumber = 0;
	}

	char buffer [80];
	sprintf(buffer, ".%u", _fileNumber);
	_currentName = _name + buffer;
	_currentName += _extension;            
}

LimitedFile::~LimitedFile()
{
	// close the file if needed
	if(_file->is_open())
	{
		_file->close();
		delete _file;
	}
	// rename the file
	if(rename((_currentName + ".tmp").c_str(), _currentName.c_str()))
	{
		char text[100];
		sprintf(text, "Failed to rename (when destructing) the file '%s' -> '%s'.", (_currentName + ".tmp").c_str(), _currentName.c_str());
		throw std::runtime_error(text);
	}
}

} // namespace CatShell

