#include "CatShell/core/TimePoint.h"

#include <iostream>
#include <sys/time.h>

using namespace std;

namespace CatShell {

TimePoint::TimePoint()
{
        get_now();
}

TimePoint::TimePoint(const TimePoint& other_time)
{
        _seconds = other_time._seconds;
        _micro_seconds = other_time._micro_seconds;
}

TimePoint::~TimePoint()
{
}

void TimePoint::get_now()
{
        timeval time_now;
        gettimeofday(&time_now, nullptr);
        _seconds = time_now.tv_sec;
        _micro_seconds = time_now.tv_usec;
}
       
double TimePoint::get_time_double() const
{
        double temp;
        temp = _micro_seconds;
        temp /= 1000000;
        temp += _seconds;
       
        return temp;
}

TimePoint& TimePoint::operator=(const TimePoint& other_time)
{
        _seconds = other_time._seconds;
        _micro_seconds = other_time._micro_seconds;
       
        return *this;
}

TimePoint TimePoint::operator-(const TimePoint& other_time) const
{
        TimePoint temp;
        temp._seconds = _seconds - other_time._seconds;
        if(_micro_seconds < other_time._micro_seconds)
        {
                temp._seconds--;
                temp._micro_seconds = 1000000 - (other_time._micro_seconds - _micro_seconds);
        }
        else
        {
                temp._micro_seconds = _micro_seconds - other_time._micro_seconds;
        }
        return temp;
}

void TimePoint::operator-=(const TimePoint& other_time)
{
        _seconds -= other_time._seconds;
        if(_micro_seconds < other_time._micro_seconds)
        {
                _seconds--;
                _micro_seconds = 1000000 - (other_time._micro_seconds - _micro_seconds);
        }
        else
        {
                _micro_seconds -= other_time._micro_seconds;
        }
}

void TimePoint::operator+=(const TimePoint& other_time)
{
        _seconds += other_time._seconds;
        _micro_seconds += other_time._micro_seconds;
        _seconds += _micro_seconds/1000000;
        _micro_seconds = _micro_seconds%1000000;
}


bool TimePoint::operator==(const TimePoint& other_time) const
{
        return (_seconds == other_time._seconds) && (_micro_seconds == other_time._micro_seconds);
}

bool TimePoint::operator>(const TimePoint& other_time) const
{
        if(_seconds > other_time._seconds)
                return true;
        else if(_seconds == other_time._seconds)
        {
                return _micro_seconds > other_time._micro_seconds;
        }
        else
                return false;
}

bool TimePoint::operator>=(const TimePoint& other_time) const
{
        if(_seconds > other_time._seconds)
                return true;
        else if(_seconds == other_time._seconds)
        {
                return _micro_seconds >= other_time._micro_seconds;
        }
        else
                return false;
}

bool TimePoint::operator<(const TimePoint& other_time) const
{
        if(_seconds < other_time._seconds)
                return true;
        else if(_seconds == other_time._seconds)
        {
                return _micro_seconds < other_time._micro_seconds;
        }
        else
                return false;
}

bool TimePoint::operator<=(const TimePoint& other_time) const
{
        if(_seconds < other_time._seconds)
                return true;
        else if(_seconds == other_time._seconds)
        {
                return _micro_seconds <= other_time._micro_seconds;
        }
        else
                return false;
}

void TimePoint::convert_to_UTC_asci(char* str) const
{
        time_t t = (time_t)_seconds;
        tm* pmt = gmtime(&t);

        sprintf(str, "%04u-%02u-%02u %02u:%02u:%02u.%06u", pmt->tm_year+1900, pmt->tm_mon+1, pmt->tm_mday, pmt->tm_hour, pmt->tm_min, pmt->tm_sec, _micro_seconds);
}

void TimePoint::convert_to_local_asci(char* str) const
{
        time_t t = (time_t)_seconds;
        tm* pmt = localtime(&t);

        sprintf(str, "%04u-%02u-%02u %02u:%02u:%02u.%06u", pmt->tm_year+1900, pmt->tm_mon+1, pmt->tm_mday, pmt->tm_hour, pmt->tm_min, pmt->tm_sec, _micro_seconds);
}

void TimePoint::set_value_as_UTC(std::uint32_t year, std::uint8_t month, std::uint8_t daym, std::uint8_t hour, std::uint8_t minute, std::uint8_t second, std::uint32_t microseconds)
{
        tm timeinfo;
        timeinfo.tm_year = year-1900;
        timeinfo.tm_mon = month-1;
        timeinfo.tm_mday = daym;
        timeinfo.tm_hour = hour;
        timeinfo.tm_min = minute;
        timeinfo.tm_sec = second;
        time_t rootTime = mktime ( &timeinfo );
        _seconds = rootTime;
        _micro_seconds = microseconds;
}

void TimePoint::set_time_double(double time)
{
        _seconds = (std::uint64_t)time;
        _micro_seconds = (std::uint32_t)((time - _seconds)*1000000);
}

void TimePoint::write_to_stream(std::ostream& output) const
{
	output.write((char*)&_seconds, sizeof(std::uint64_t));
	output.write((char*)&_micro_seconds, sizeof(std::uint32_t));
}

void TimePoint::read_from_stream(std::istream& input)
{
	input.read((char*)&_seconds, sizeof(std::uint64_t));
	input.read((char*)&_micro_seconds, sizeof(std::uint32_t));
}

} // namespace CatShell
