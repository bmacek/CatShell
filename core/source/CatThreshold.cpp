#include "CatShell/core/CatThreshold.h"
#include "CatShell/core/CatDouble.h"
#include "CatShell/core/PluginDeamon.h"
#include <math.h>

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatThreshold, CAT_NAME_CATTHRESHOLD, CAT_TYPE_CATTHRESHOLD);


CatThreshold::CatThreshold()
{

}

CatThreshold::~CatThreshold()
{

}

void CatThreshold::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_object && object.isSomething())
        {

		CatPointer<CatDouble>  number = object.cast<CatDouble>();

		bool b;
		
		if((*number).value() >= _threshold){
			b = true;
		}
		
		else b = false;

		CatPointer<CatBool> l = _pool->get_element();
		
		*l = b;

		publish_data(_out_port_object, l ,logger);

        }
	
	Algorithm::process_data(port, object, flush, logger);
}

void CatThreshold::algorithm_init()
{
        Algorithm::algorithm_init();

        _in_port_object = declare_input_port("in_double", CAT_NAME_CATDOUBLE);

	_out_port_object = declare_output_port("out_bool", CAT_NAME_CATBOOL);

        _pool = PluginDeamon::get_pool<CatBool>();

	_threshold = algo_settings().get_setting<double>("threshold", 1);
}

void CatThreshold::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
	
	
}


} // namespace CatShell
