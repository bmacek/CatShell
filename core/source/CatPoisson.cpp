#include "CatShell/core/CatPoisson.h"
#include "CatShell/core/CatDouble.h"
#include "CatShell/core/PluginDeamon.h"
#include <math.h>

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatPoisson, CAT_NAME_CATPOISSON, CAT_TYPE_CATPOISSON);


CatPoisson::CatPoisson()
{

}

CatPoisson::~CatPoisson()
{

}

void CatPoisson::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
    if(port == _in_port_object && object.isSomething())
    {
		CatPointer<CatDouble>  average = object.cast<CatDouble>();

		double L=exp(-(*average).value());
		double p=1;
		std::uint32_t counter=0;

		for(std::uint32_t i=0; i<_repetition;i++)
		{
			counter=0;
			p=1;
			do {
				counter++;
				p *= (double)rand()/(double)RAND_MAX;
			} while (p > L);

			CatPointer<CatUint32> number = _pool->get_element();

			*number = counter-1;

			publish_data(_out_port_object, number, logger);
		}
    }
	
	Algorithm::process_data(port, object, flush, logger);
}

void CatPoisson::algorithm_init()
{
        Algorithm::algorithm_init();

        _in_port_object = declare_input_port("in_double", CAT_NAME_CATDOUBLE);

	_out_port_object = declare_output_port("out_uint", CAT_NAME_CATUINT32);

        _pool = PluginDeamon::get_pool<CatUint32>();

	srand(time(NULL));

	_repetition = algo_settings().get_setting<std::uint32_t>("repetition", 1);
}

void CatPoisson::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
	
	
}


} // namespace CatShell
