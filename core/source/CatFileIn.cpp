#include "CatShell/core/CatFileIn.h"
#include "CatShell/core/PluginDeamon.h"

#include <fstream>
#include <iostream>

using namespace std;

namespace CatShell {

CAT_OBJECT_IMPLEMENT(CatFileIn, CAT_NAME_CATFILEIN, CAT_TYPE_CATFILEIN);


CatFileIn::CatFileIn()
: _file(nullptr)
{

}

CatFileIn::~CatFileIn()
{
}

bool CatFileIn::open_next_file()
{
        // close the current file
        if(_file)
        {
                delete _file;
                _file = nullptr;
        }

        while(true)
        {
                if(_files.size())
                {
                        // still some files

                        // check what kind of file it is
                        if(_files.front().stream)
                        {
                                if(_file_cnt > _files.front().to)
                                {
                                        // end of stream
                                        _files.pop();
                                        // prepare next is stream
                                        if(_files.size() && _files.front().stream)
                                        {
                                            _file_cnt = _files.front().from;
                                        }
                                        continue;
                                }

                                // should stay in this stream
                                char text[256];
                                sprintf(text, "%s.%u.%s", _files.front().path.c_str(), _file_cnt, _files.front().extension.c_str());
                                _file = new ifstream(std::string(text), std::ifstream::in | std::ifstream::binary);
                                if(_file->is_open())
                                {
                                        LOG(logger(), "File opened: " << text);
                                        // prepare the exception
                                        _file->exceptions( std::ios::failbit | std::ios::badbit  | std::ios::eofbit );
                                        _file_cnt++;
                                        return true;
                                }
                                else
                                {
                                        ERROR(logger(), "Could not opened stream-file: " << text);
                                        _file_cnt++;
                                        continue;
                                }
                        }
                        else
                        {
                                _file = new ifstream(_files.front().path.c_str(), std::ifstream::in | std::ifstream::binary);
                                if(_file->is_open())
                                {
                                        LOG(logger(), "File opened: " << _files.front().path);
                                        // prepare the exception
                                        _file->exceptions( std::ios::failbit | std::ios::badbit  | std::ios::eofbit );
                                        _files.pop();
                                        // prepare next is stream
                                        if(_files.size() && _files.front().stream)
                                        {
                                            _file_cnt = _files.front().from;
                                        }
                                        return true;
                                }
                                else
                                {
                                        ERROR(logger(), "Could not opened file: " << _files.front().path);
                                        _files.pop();
                                        // prepare next is stream
                                        if(_files.size() && _files.front().stream)
                                        {
                                            _file_cnt = _files.front().from;
                                        }
                                        continue;
                                }
                        }
                }
                else
                        return false;
        }
}

void CatFileIn::work_main()
{
        char text[100];
        StreamSize            size;
        StreamSize            location = 0;
        CatType               type;
        MemoryPoolBase*       pool;
        CatPointer<CatObject> object;

        LOG(logger(), "Started reading the stream.")

        if(open_next_file())
        {

                bool finish = false;
                while(!finish)
                {
                        try
                        {
                                location = 0;
                                if(_stream_map_mode)
                                {
                                        cout << "New file *******************************************" << endl;
                                }
                                while(true)
                                {
                                        // read the length
                                        _file->read((char*)&size, sizeof(StreamSize));
                                        // read the type
                                        _file->read((char*)&type, sizeof(CatType));

                                        _stat_objects++;
                                        _stat_bytes += size;

                                        // don't continue too long
                                        if(_object_limit_n != 0 && _stat_objects > (_object_limit_min + _object_limit_n))
                                        {
                                                finish = true;
                                                break;
                                        }

                                        // don't start too early
                                        if(_stat_objects <= _object_limit_min)
                                        {
                                                // before desired start

                                                // skip data
                                                _file->ignore(size);
                                        }
                                        else if(_stream_map_mode)
                                        {
                                                // print
                                                sprintf(text, "%10llu location: 0x%08x type: 0x%08x, lenght: 0x%08x", _stat_objects, location, type, size);
                                                cout << text << endl;

                                                // skip data
                                                _file->ignore(size);
                                        }
                                        else
                                        {
                                                // get the instance
                                                pool = PluginDeamon::get_memory_pool(type);
                                                if(pool)
                                                {
                                                        object = pool->get_base_element();
                                                }
                                                else
                                                {
                                                        object = CatPointer<CatObject>(nullptr);
                                                        _stat_type_err++;
                                                }

                                                if(object.isNull())
                                                {
                                                        // object could not be instantiated
                                                        _file->ignore(size);
                                                }
                                                else
                                                {
                                                        // object exists, so read it from the stream
                                                        object->cat_stream_in(*_file);
                                                        publish_data(_out_port_object, object, logger());
                                                }
                                        }

                                        location += size + sizeof(CatType) + sizeof(StreamSize);
                                }

                        }
                        catch(std::bad_alloc e)
                        {
                                _stat_memory_err++;

                                // not enough space in teh memory pool
                                ERROR(logger(), "Not enough memory for objects of type: " << (int)type << ".")
                                finish = false;
                        }
                        catch(std::ios::failure e)
                        {
                                // end of file
                                LOG(logger(), "End of file received.")
                                finish = !open_next_file();
                        }
                        catch(std::exception e)
                        {
                                // end of file
                                LOG(logger(), "End of file received.")
                                finish = !open_next_file();
                        }
                }
        }
        else
        {
                WARNING(logger(), "No files were available.")
        }

        // flush
        flush_data_clients(_out_port_object, logger());

        LOG(logger(), "Finished reading the stream.")

        START_LOG_LOG(logger())
        LINE_LOG_LOG(logger(), "End statistics:")
        logger().stream() << logger().padding() << "Total objects    : " << (_stat_objects) << endl;
        logger().stream() << logger().padding() << "Type err objects : " << (_stat_type_err) << endl;
        logger().stream() << logger().padding() << "Mem. err objects : " << (_stat_memory_err) << endl;
        logger().stream() << logger().padding() << "Total bytes read : " << (_stat_objects)*(sizeof(StreamSize)+sizeof(CatType)) + _stat_bytes << endl;
        END_LOG(logger())
}

void CatFileIn::work_init()
{
        Process::work_init();

        _stat_objects = 0;
        _stat_type_err = 0;
        _stat_memory_err = 0;
        _stat_bytes = 0;
}

void CatFileIn::work_deinit()
{
        Process::work_deinit();
}

void CatFileIn::algorithm_init()
{
        Process::algorithm_init();

        // declare the input port
        _out_port_object = declare_output_port("out_object", CAT_NAME_OBJECT);

        // load all the files from settings
        for(auto& setting: algo_settings()["files"])
        {
                if(setting.name() == "file")
                {
                        t_file_desc tmp;
                        // check if it is a stream
                        tmp.stream = setting.get_setting<bool>("stream", false);
                        if(tmp.stream)
                        {
                                // get the stream parameters
                                tmp.path = setting.get_setting<std::string>("path", "");
                                tmp.extension = setting.get_setting<std::string>("extension", "");
                                tmp.from = setting.get_setting<std::uint32_t>("from", 0);
                                tmp.to = setting.get_setting<std::uint32_t>("to", 0);
                                if(tmp.path.size() != 0)
                                {
                                        _files.push(tmp);
                                }

                                _file_cnt = tmp.from;
                        }
                        else
                        {
                                // get name
                                tmp.path = setting.get_setting<std::string>("path", "");
                                if(tmp.path.size() != 0)
                                {
                                        _files.push(tmp);
                                }

                                _file_cnt = 0;
                        }
                }
        }

        // check if the mode is 'map mode'
        _stream_map_mode = algo_settings().get_setting<bool>("map_mode", false);

        // check if there is a limit on the object count
        _object_limit_min = algo_settings().get_setting<std::uint32_t>("object_interval/start", 0);
        _object_limit_n = algo_settings().get_setting<std::uint32_t>("object_interval/number", 0);
}

void CatFileIn::algorithm_deinit()
{
        Process::algorithm_deinit();
}

} // namespace CatShell
