#include "CatShell/core/Logger.h"
#include <sstream>

using namespace std;

namespace CatShell {

//
// Logger
//

Logger::Logger(const std::string& name)
: _good(true), _name(name), _parenth(nullptr), _lock(new std::mutex)
{
}

Logger::Logger(const std::string& name, Logger* parenth)
: _good(true), _name(name), _parenth(parenth), _lock(parenth->_lock)
{
}

Logger::~Logger()
{
	if(!_parenth)
		delete _lock;
}

Logger* Logger::get_sublogger(const std::string& name)
{
	return new Logger(name, this);
}

void Logger::startNoteDEBUG(bool lock)
{
	if(lock)
		_lock->lock();
	time_t rawtime;
	time ( &rawtime );
	char buffer [80];
	strftime (buffer,80,"%Y_%m_%d at %H:%M:%S", localtime ( &rawtime ));   
	this->stream() << buffer << " {D} ";
	_state = "D";
}

void Logger::startNoteINFO(bool lock)
{
	if(lock)
		_lock->lock();
	time_t rawtime;
	time ( &rawtime );
	char buffer [80];
	strftime (buffer,80,"%Y_%m_%d at %H:%M:%S", localtime ( &rawtime ));   
	this->stream() << buffer << " {I} ";
	_state = "I";
}

void Logger::startNoteLOG(bool lock)
{
	if(lock)
		_lock->lock();
	time_t rawtime;
	time ( &rawtime );
	char buffer [80];
	strftime (buffer,80,"%Y_%m_%d at %H:%M:%S", localtime ( &rawtime ));   
	this->stream() << buffer << " {L} ";
	_state = "L";
}

void Logger::startNoteWARNING(bool lock)
{
	if(lock)
		_lock->lock();
	time_t rawtime;
	time ( &rawtime );
	char buffer [80];
	strftime (buffer,80,"%Y_%m_%d at %H:%M:%S", localtime ( &rawtime ));   
	this->stream() << buffer << " {W} ";
	_state = "W";
}

void Logger::startNoteERROR(bool lock)
{
	if(lock)
		_lock->lock();
	time_t rawtime;
	time ( &rawtime );
	char buffer [80];
	strftime (buffer,80,"%Y_%m_%d at %H:%M:%S", localtime ( &rawtime ));   
	this->stream() << buffer << " {E} ";
	_state = "E";
}

void Logger::startNoteFATAL(bool lock)
{
	if(lock)
		_lock->lock();
	time_t rawtime;
	time ( &rawtime );
	char buffer [80];
	strftime (buffer,80,"%Y_%m_%d at %H:%M:%S", localtime ( &rawtime ));   
	this->stream() << buffer << " {F} ";
	_state = "F";
}

std::string Logger::padding() const
{
	std::stringstream result;
	result << "                       {" << _state << "} ";
	this->full_name(result);
	result << " ";
	return result.str();
}

void Logger::endNote()
{
	//this->stream() << std::endl;
	this->flush();
	_lock->unlock();
}

void Logger::full_name(std::ostream& output) const
{
	if(_parenth)
	{
		_parenth->full_name(output);
	}
	//
	output << "[" << _name << "]";
}

std::ostream& Logger::stream()
{
	if(_parenth)
		return _parenth->stream();
	else
		return std::cout;
}

//
// FileLogger
//

FileLogger::FileLogger(const std::string& log_name, const std::string& file_name, std::uint16_t max_files, std::uint32_t max_length)
: Logger(log_name), _file(file_name, ".log", std::ios_base::out, max_length, true, max_files, false)
{
}

FileLogger::~FileLogger()
{
}

void FileLogger::flush()
{
	try
	{
		_file.flush();
	}
	catch ( std::runtime_error& ex )
	{
		_good = false;
		throw ex;
	}
}

std::ostream& FileLogger::stream()
{
	return _file.get_stream();
}

} // namespace CatShell
