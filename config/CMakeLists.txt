cmake_minimum_required(VERSION 2.8)

project(CatConfig CXX)

file(GLOB CONFIG_FILES "*.xml")
file(GLOB SCRIPT_FILES "*.sh")

install (FILES ${CONFIG_FILES} DESTINATION config)
install (FILES ${SCRIPT_FILES} DESTINATION scripts)
